package com.ecufroster.formulario.view.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.ecufroster.formulario.R
import com.ecufroster.formulario.core.BaseActivity
import com.ecufroster.formulario.core.ConstantPermisos
import com.ecufroster.formulario.core.TIPO_FORMULARIO_MEDICION_CONSUMO_KWH_EN_PDV
import com.ecufroster.formulario.db.controller.*
import com.ecufroster.formulario.db.model.DBFormulario
import com.ecufroster.formulario.db.model.DBFormularioInformacionCliente
import com.ecufroster.formulario.helper.PermisosHelper
import com.ecufroster.formulario.helper.UtilidadesHelper
import com.ecufroster.formulario.helper.ViewHelper
import com.ecufroster.formulario.model.PopupListSelected
import com.ecufroster.formulario.retrofit.model.response.ApiResponseCliente
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import kotlinx.android.synthetic.main.activity_informacion_cliente.*
import kotlinx.android.synthetic.main.layout_editext_busqueda.*

class InformacionClienteActivity : BaseActivity() {
    var idFormulario: Int = -1
    var informacionCliente = DBFormularioInformacionCliente()
    var formulario = DBFormulario()
    var cliente = ApiResponseCliente()
    var listPopupListSelected = ArrayList<PopupListSelected>()
    val listCiudad = arrayListOf("Guayaquil", "Durán", "Milagro", "Chongón en adelante")
    var descripcionCiudad : String = ""
    var positionCiudadSeleccionado : Int = -1
    val LISTA_CIUDAD : Int = 1
    var mensaje: String = ""
    var locationManager: LocationManager? = null
    var locationListener: LocationListener? = null
    var fusedLocationProviderClient: FusedLocationProviderClient? = null
    private val TAG_UBICACION = "ubicacion_informacion_cliente"

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState, R.layout.activity_informacion_cliente, this)
        configToolbar(R.id.toolbar, "Información del cliente/usuario")
        configProgressBar(R.id.layoutProgressBar)

        if(intent.hasExtra("id_formulario")){
            idFormulario = intent.getIntExtra("id_formulario", 0)
            formulario = ControllerFormularios.getDataByIdForm(idFormulario)
        }else{
            Toast.makeText(this, "No se puede iniciar formulario por falta de información", Toast.LENGTH_LONG).show()
            finish()
        }

        tvLabelBusqueda.text = getString(R.string.info_cliente_label_codigo_sap_busqueda)
        informacionCliente = ControllerInformacionCliente.getDataByIdForm(idFormulario)
        setData()

        if(ControllerFormularios.isFormEnviado(idFormulario)){
            deshabilitaCampos()
        }

        btnSearch.setOnClickListener { busquedaCliente() }
        edtInfoClienteLocalCiudad.setOnClickListener { selectedCiudad() }
        fabInformacionClienteGuardar.setOnClickListener { guardarForm() }
        btnGeolocalizacion.setOnClickListener { getUbication() }

        edtInfoClienteLocalNombre.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if(edtInfoClienteLocalNombre.text.toString().trim().isNotEmpty()){
                    edtInfoClienteLocalNombre.error = null
                }
            }
        })

        edtInfoClienteLocalDireccion.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if(edtInfoClienteLocalDireccion.text.toString().trim().isNotEmpty()){
                    edtInfoClienteLocalDireccion.error = null
                }
            }
        })

        edtInfoClienteLocalTelefono.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if(edtInfoClienteLocalTelefono.text.toString().trim().isNotEmpty()){
                    edtInfoClienteLocalTelefono.error = null
                }
            }
        })

        edtInfoClienteLocalCiudad.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if(edtInfoClienteLocalCiudad.text.toString().trim().isNotEmpty()){
                    edtInfoClienteLocalCiudad.error = null
                }
            }
        })

        edtInfoClienteLocalVendedor.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if(edtInfoClienteLocalVendedor.text.toString().trim().isNotEmpty()){
                    edtInfoClienteLocalVendedor.error = null
                }
            }
        })

        edtInfoClienteLocalVendedorTelefono.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if(edtInfoClienteLocalVendedorTelefono.text.toString().trim().isNotEmpty()){
                    edtInfoClienteLocalVendedorTelefono.error = null
                }
            }
        })
    }

    fun deshabilitaCampos(){
        edtBusqueda.isEnabled = false
        btnSearch.isEnabled = false
        edtInfoClienteNombreCliente.isEnabled = false
        edtInfoClienteLocalNombre.isEnabled = false
        edtInfoClienteLocalDireccion.isEnabled = false
        edtInfoClienteLocalTelefono.isEnabled = false
        edtInfoClienteLocalCiudad.isEnabled = false
        rbInfoClienteForaneoSi.isEnabled = false
        rbInfoClienteForaneoNo.isEnabled = false
        edtInfoClienteTerritorio.isEnabled = false
        edtInfoClienteLocalVendedor.isEnabled = false
        edtInfoClienteLocalVendedorTelefono.isEnabled = false
        fabInformacionClienteGuardar.visibility = View.GONE
    }

    @SuppressLint("MissingPermission", "LongLogTag", "SetTextI18n")
    fun getUbication(){
        Log.e(TAG_UBICACION, "obteniendo la ubicación...")

        if(PermisosHelper.solicitarPermiso(this, Manifest.permission.ACCESS_FINE_LOCATION, ConstantPermisos.Request.REQUEST_CODE_ACCESS_FINE_LOCATION)){
            Log.e(TAG_UBICACION, "si hay permisos de ubicación")

            if(ViewHelper.revisarServicioUbicacion(this)) {
                Log.e(TAG_UBICACION, "si tiene encendido el gps")
                fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

                // Obtener la ubicación actual
                val locationTask: Task<Location> = fusedLocationProviderClient!!.lastLocation
                locationTask.addOnSuccessListener(OnSuccessListener<Location?> { location ->
                    if (location != null) {
                        Log.e(TAG_UBICACION, "Ubicación capturada.")

                        // Aquí obtienes la ubicación actual
                        val latitude = location.latitude
                        val longitude = location.longitude

                        edtInfoClienteGeolocalizacion.setText("$latitude,$longitude")

                        Toast.makeText(
                            this@InformacionClienteActivity,
                            "Ubicación del cliente capturada.",
                            Toast.LENGTH_SHORT
                        ).show()

                        Log.e(TAG_UBICACION, "latitud y longitud => $latitude,$longitude")
                    } else {
                        Log.e(TAG_UBICACION, "No se pudo obtener la ubicación actual")

                        Toast.makeText(
                            this@InformacionClienteActivity,
                            "No se pudo obtener la ubicación actual",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                })
            } else {
                Log.e(TAG_UBICACION, "NOOOOOOOO tiene encendido el gps")

                UtilidadesHelper.mensajeIndicador(
                    this,
                    getString(R.string.app_name),
                    getString(R.string.solicitar_asistencia_permitir_activar_ubicacion)
                )
                    .setPositiveButton(R.string.btn_aceptar,
                        DialogInterface.OnClickListener { _, _ ->
                            startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                        })
                    .setCancelable(false)
                    .show()
            }
        } else {
            Log.e("JOSELUIS", "NOOOOO hay permisos de ubicación")
        }
    }

    fun busquedaCliente(){
        ViewHelper.hideKeyboardInActivity(this)

        if(edtBusqueda.text.toString().trim().isEmpty()){
            Toast.makeText(this, "Ingresa información para buscar", Toast.LENGTH_LONG).show()
        }
        else{
            showProgressBar()
            cliente = ControllerApiClientes.getClientByCode(edtBusqueda.text.toString().trim())

            if(cliente.codigo != ""){
                informacionCliente.codigoSap = cliente.codigo
                informacionCliente.cliente = cliente.nombre
                informacionCliente.localDireccion = cliente.calle
                informacionCliente.localTelefono = cliente.telefono
                informacionCliente.territorio = cliente.territorioDescripcion
                informacionCliente.vendedor = cliente.ejecutivo
                informacionCliente.vendedorTelefono = cliente.ejecutivoTelefono

                setData()
            }
            else{
                UtilidadesHelper.mensajeAlerta(this, "Aviso", "No existe cliente con el código que estás buscando.").show()
            }
            hideProgressBar()
        }
    }

    @SuppressLint("SetTextI18n")
    fun setData(){
        if(informacionCliente.codigoSap.trim().isNotEmpty()){
            edtBusqueda.setText(informacionCliente.codigoSap)
            edtInfoClienteNombreCliente.setText(informacionCliente.cliente)
            edtInfoClienteLocalNombre.setText(informacionCliente.localNombre)
            edtInfoClienteLocalDireccion.setText(informacionCliente.localDireccion)
            edtInfoClienteLocalTelefono.setText(informacionCliente.localTelefono)
            edtInfoClienteTerritorio.setText(informacionCliente.territorio)
            edtInfoClienteLocalVendedor.setText(informacionCliente.vendedor)
            edtInfoClienteLocalVendedorTelefono.setText(informacionCliente.vendedorTelefono)
            edtInfoClienteGeolocalizacion.setText(informacionCliente.latitud+","+informacionCliente.longitud)

            if(informacionCliente.idFormulario != -1){
                val indexAux = listCiudad.indexOfFirst { it.toLowerCase() == informacionCliente.localCiudad.toLowerCase() }
                positionCiudadSeleccionado = indexAux

                if(indexAux >= 0){
                    edtInfoClienteLocalCiudad.setText(listCiudad[positionCiudadSeleccionado])
                }else{
                    edtInfoClienteLocalCiudad.setText(informacionCliente.localCiudad)
                }
            }
            else{
                edtInfoClienteLocalCiudad.setText(informacionCliente.localCiudad)
            }

            contenedorFormulario.visibility = View.VISIBLE
            setForaneo()

            if(formulario.tipoFormulario == TIPO_FORMULARIO_MEDICION_CONSUMO_KWH_EN_PDV) {
                tvLabelInfoClienteForaneo.visibility = View.VISIBLE
                rgInfoClienteForaneo.visibility = View.VISIBLE
            } else {
                tvLabelInfoClienteForaneo.visibility = View.GONE
                rgInfoClienteForaneo.visibility = View.GONE
            }
        }
    }

    fun setForaneo(){
        if(positionCiudadSeleccionado == 0) rbInfoClienteForaneoNo.isChecked = true else rbInfoClienteForaneoSi.isChecked = true
    }

    fun selectedCiudad(){
        ViewHelper.hideKeyboardInActivity(this)
        listPopupListSelected.clear()

        for(item in listCiudad){
            descripcionCiudad = item
            listPopupListSelected.add(
                PopupListSelected(descripcion = descripcionCiudad)
            )
        }

        val intent = Intent(this, PopupListSelectedActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        intent.putExtra("index", positionCiudadSeleccionado)
        intent.putExtra("titulo", "Ciudades")
        intent.putParcelableArrayListExtra("lista", listPopupListSelected)
        startActivityForResult(intent, LISTA_CIUDAD)
    }

    fun formValido(): Boolean{
        var retorna = true

        if(edtInfoClienteLocalNombre.text.toString().isEmpty()){
            edtInfoClienteLocalNombre.error = getString(R.string.campo_necesario)
            retorna = false
        }
        else{
            edtInfoClienteLocalNombre.error = null
        }

        if(edtInfoClienteLocalDireccion.text.toString().isEmpty()){
            edtInfoClienteLocalDireccion.error = getString(R.string.campo_necesario)
            retorna = false
        }
        else{
            edtInfoClienteLocalDireccion.error = null
        }

        if(edtInfoClienteLocalTelefono.text.toString().isEmpty()){
            edtInfoClienteLocalTelefono.error = getString(R.string.campo_necesario)
            retorna = false
        }
        else{
            edtInfoClienteLocalTelefono.error = null
        }

        if(edtInfoClienteLocalCiudad.text.toString().isEmpty()){
            edtInfoClienteLocalCiudad.error = getString(R.string.campo_necesario)
            retorna = false
        }
        else{
            edtInfoClienteLocalCiudad.error = null
        }

        if(edtInfoClienteLocalVendedor.text.toString().isEmpty()){
            edtInfoClienteLocalVendedor.error = getString(R.string.campo_necesario)
            retorna = false
        }
        else{
            edtInfoClienteLocalVendedor.error = null
        }

        if(edtInfoClienteLocalVendedorTelefono.text.toString().isEmpty()){
            edtInfoClienteLocalVendedorTelefono.error = getString(R.string.campo_necesario)
            retorna = false
        }
        else{
            edtInfoClienteLocalVendedorTelefono.error = null
        }

        if(retorna){
            informacionCliente.formularioFinalizado = 1
        }
        else{
            informacionCliente.formularioFinalizado = 0
        }

        return retorna
    }

    fun guardarForm(){
        ViewHelper.hideKeyboardInActivity(this)

        informacionCliente.codigoSap = informacionCliente.codigoSap
        informacionCliente.cliente = edtInfoClienteNombreCliente.text.toString().trim()
        informacionCliente.localNombre = edtInfoClienteLocalNombre.text.toString().trim()
        informacionCliente.localDireccion = edtInfoClienteLocalDireccion.text.toString().trim()
        informacionCliente.localTelefono = edtInfoClienteLocalTelefono.text.toString().trim()
        informacionCliente.localCiudad = edtInfoClienteLocalCiudad.text.toString().trim()
        informacionCliente.foraneo = if(rbInfoClienteForaneoSi.isChecked) "SI" else "NO"
        informacionCliente.territorio = edtInfoClienteTerritorio.text.toString().trim()
        informacionCliente.vendedor = edtInfoClienteLocalVendedor.text.toString().trim()
        informacionCliente.vendedorTelefono = edtInfoClienteLocalVendedorTelefono.text.toString().trim()
        informacionCliente.latitud = edtInfoClienteGeolocalizacion.text.toString().split(",")[0]
        informacionCliente.longitud = edtInfoClienteGeolocalizacion.text.toString().split(",")[1]
        informacionCliente.idFormulario = idFormulario

        if(formValido()){
            mensaje = getString(R.string.guardado_exitoso)
            informacionCliente.formularioFinalizado = 1
        }
        else{
            mensaje = getString(R.string.error_campo_faltante)
            informacionCliente.formularioFinalizado = 0
        }

        ControllerInformacionCliente.createForm(informacionCliente)
        Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show()
        finish()
    }

    @SuppressLint("SetTextI18n")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            val indexPosition = data!!.getIntExtra("indexItem", 0)

            if (requestCode == LISTA_CIUDAD) {
                descripcionCiudad = listCiudad[indexPosition]
                edtInfoClienteLocalCiudad.setText(descripcionCiudad)
                positionCiudadSeleccionado = indexPosition

                setForaneo()
            }
        }
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        ViewHelper.hideKeyboardInActivity(this)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            ConstantPermisos.Request.REQUEST_CODE_ACCESS_FINE_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getUbication()
                } else {
                    UtilidadesHelper.mensajePermisos(this)!!
                        .setPositiveButton(
                            getString(R.string.dialogos_aceptar),
                            DialogInterface.OnClickListener { _, _ ->
                                PermisosHelper.validarSolicitudPermiso(
                                    this,
                                    Manifest.permission.ACCESS_FINE_LOCATION,
                                    getString(
                                        R.string.permission_description_rejected,
                                        ConstantPermisos.Name.UBICACION,
                                        ConstantPermisos.Reason.UBICACION_ACTUAL
                                    ),
                                    ConstantPermisos.Request.REQUEST_CODE_ACCESS_FINE_LOCATION
                                )
                            })
                        .show()
                }
                return
            }
        }
    }
}