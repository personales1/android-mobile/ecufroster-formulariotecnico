package com.ecufroster.formulario.view.activity

import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.MenuItem
import android.widget.Toast
import androidx.core.content.FileProvider
import com.ecufroster.formulario.R
import com.ecufroster.formulario.core.*
import com.ecufroster.formulario.db.controller.ControllerFormularios
import com.ecufroster.formulario.db.controller.ControllerImagenFormulario
import com.ecufroster.formulario.db.controller.ControllerInformacionCliente
import com.ecufroster.formulario.db.model.DBFormularioImagen
import com.ecufroster.formulario.db.model.DBFormularioInformacionCliente
import com.ecufroster.formulario.helper.CameraHelper
import com.ecufroster.formulario.helper.PermisosHelper
import com.ecufroster.formulario.helper.UtilidadesHelper
import com.ecufroster.formulario.model.PermisosApp
import kotlinx.android.synthetic.main.activity_fotos_evidencia.*
import java.io.*
import java.util.ArrayList

class FotosEvidenciaActivity : BaseActivity() {
    var idFormulario: Int = 0
    var fotoEscogida = 0
    var codigoSapCliente: String = ""
    var file: File? = null
    var bitmap: Bitmap?= null
    val widthMax = 400.0
    var pictureImagePath = ""
    var informacionCliente = DBFormularioInformacionCliente()
    var listFotos : ArrayList<DBFormularioImagen> = ArrayList<DBFormularioImagen>()
    internal lateinit var permissionsToRequest: ArrayList<String>
    private lateinit var listPermisos: ArrayList<PermisosApp>
    private lateinit var baos: ByteArrayOutputStream
    internal lateinit var uri: Uri

    private val CAMERA_TAKE_REQUEST = 200

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState, R.layout.activity_fotos_evidencia, this)
        configToolbar(R.id.toolbar, "Fotos")
        configProgressBar(R.id.layoutProgressBar)

        if(intent.hasExtra("id_formulario")){
            idFormulario = intent.getIntExtra("id_formulario", 0)
        }else{
            Toast.makeText(this, "No se puede iniciar formulario por falta de información", Toast.LENGTH_LONG).show()
            finish()
        }

        informacionCliente = ControllerInformacionCliente.getDataByIdForm(idFormulario)
        if(informacionCliente.codigoSap.trim().isNotEmpty()){
            codigoSapCliente = informacionCliente.codigoSap.trim()
        }
        else{
            UtilidadesHelper.mensajeAlerta(this, "Advertencia", "Para poder tomar fotos, es necesario que primero llenes la sección 'Información del cliente/usuario' del formulario.")
                .setPositiveButton("Aceptar", DialogInterface.OnClickListener { _, _ ->
                    finish()
                })
                .setCancelable(false)
                .show()
        }

        listPermisos = arrayListOf()

        listFotos = ControllerImagenFormulario.getFotosByIdForm(idFormulario, IMAGEN_FOTOS_REPORTE)
        setData()

        if(ControllerFormularios.isFormEnviado(idFormulario)){
            deshabilitaCampos()
        }

        imvFoto1.setOnClickListener {
            fotoEscogida = 1
            abrirCamara()
        }
        imvFoto2.setOnClickListener {
            fotoEscogida = 2
            abrirCamara()
        }
        imvFoto3.setOnClickListener {
            fotoEscogida = 3
            abrirCamara()
        }
        imvFoto4.setOnClickListener {
            fotoEscogida = 4
            abrirCamara()
        }
        imvFoto5.setOnClickListener {
            fotoEscogida = 5
            abrirCamara()
        }
    }

    fun deshabilitaCampos(){
        imvFoto1.isEnabled = false
        imvFoto2.isEnabled = false
        imvFoto3.isEnabled = false
        imvFoto4.isEnabled = false
        imvFoto5.isEnabled = false
    }

    fun setData(){
        for (item in listFotos){
            bitmap = BitmapFactory.decodeFile(item.rutaImagen)
            if(bitmap != null){
                try {
                    bitmap = CameraHelper.rotarImagen(item.rutaImagen, bitmap!!)
                } catch (e: IOException) {
                    e.printStackTrace()
                }

                baos = ByteArrayOutputStream()
                bitmap = CameraHelper.escalarImagen(bitmap!!, widthMax)
                bitmap!!.compress(Bitmap.CompressFormat.PNG, 100, baos)
                //remplazo la img de la camara por una menos pesada

                if(item.numFoto == 1){
                    imvFoto1.setImageBitmap(bitmap)
                }
                else if(item.numFoto == 2){
                    imvFoto2.setImageBitmap(bitmap)
                }
                else if(item.numFoto == 3){
                    imvFoto3.setImageBitmap(bitmap)
                }
                else if(item.numFoto == 4){
                    imvFoto4.setImageBitmap(bitmap)
                }
                else if(item.numFoto == 5){
                    imvFoto5.setImageBitmap(bitmap)
                }
            }
            else{
                if(item.numFoto == 1){
                    imvFoto1.setImageResource(R.drawable.error)
                }
                else if(item.numFoto == 2){
                    imvFoto2.setImageResource(R.drawable.error)
                }
                else if(item.numFoto == 3){
                    imvFoto3.setImageResource(R.drawable.error)
                }
                else if(item.numFoto == 4){
                    imvFoto4.setImageResource(R.drawable.error)
                }
                else if(item.numFoto == 5){
                    imvFoto5.setImageResource(R.drawable.error)
                }
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun abrirCamara() {
        if(CameraHelper.checkCameraExists(this)){
            permissionsToRequest = CameraHelper.checkCameraPermissions(this)
            if(permissionsToRequest.size > 0){

                listPermisos.clear()
                listPermisos.add(
                    PermisosApp(
                        permiso = Manifest.permission.CAMERA,
                        descripcion = getString(R.string.permission_description_camara),
                        descripcionPermisoRechazo = getString(R.string.permission_description_rejected, ConstantPermisos.Name.CAMARA, ConstantPermisos.Reason.CAMARA_FOTO)
                    )
                )
                listPermisos.add(
                    PermisosApp(
                        permiso = Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        descripcion = getString(R.string.permission_description_almacenamiento_guardar),
                        descripcionPermisoRechazo = getString(R.string.permission_description_rejected, ConstantPermisos.Name.ALMACENAMIENTO, ConstantPermisos.Reason.ALMACENAMIENTO_GALERIA_GUARDAR_FIRMA)
                    )
                )

                PermisosHelper.solicitarPermisoInicial(this, listPermisos, ConstantPermisos.Request.REQUEST_CODE_MULTIPLE_PERMISSION);
            }
            else{
                Toast.makeText(this, getString(R.string.camara_disponible), Toast.LENGTH_LONG).show()
                launchCamera()
            }
        }
        else{
            Toast.makeText(this, getString(R.string.camara_no_disponible), Toast.LENGTH_SHORT).show()
        }
    }

    private fun launchCamera() {
        val imageFileName = "foto-form$idFormulario-$codigoSapCliente-${"foto$fotoEscogida"}.png"

        val storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
        val directorySave = storageDir.absolutePath + File.separator + DIRECTORY_FOLDER + File.separator + DIRECTORY_FOLDER_FOTOS + File.separator + "formulario$idFormulario"

        val folderImage = File(directorySave)
        if (!folderImage.exists()) {
            folderImage.mkdirs()
        }

        pictureImagePath = directorySave + File.separator + imageFileName

        file = File(pictureImagePath)
        uri = FileProvider.getUriForFile(this,this.applicationContext.packageName + ".provider", file!!)

        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, uri)
        startActivityForResult(cameraIntent, CAMERA_TAKE_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        var selectedImage: Uri? = null

        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                CAMERA_TAKE_REQUEST -> {
                    showProgressBar()

                    bitmap = BitmapFactory.decodeFile(file!!.absolutePath)
                    try {
                        bitmap = CameraHelper.rotarImagen(file!!.absolutePath, bitmap!!)
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }

                    baos = ByteArrayOutputStream()
                    bitmap = CameraHelper.escalarImagen(bitmap!!, widthMax)
                    //bitmap = CameraHelper.comprimirImagenJpeg(bitmap!!, baos)
                    bitmap!!.compress(Bitmap.CompressFormat.PNG, 100, baos)
                    //remplazo la img de la camara por una menos pesada


                    //convierto a base64
                    when (fotoEscogida) {
                        1 -> {
                            imvFoto1.setImageBitmap(bitmap)
                        }
                        2 -> {
                            imvFoto2.setImageBitmap(bitmap)
                        }
                        3 -> {
                            imvFoto3.setImageBitmap(bitmap)
                        }
                        4 -> {
                            imvFoto4.setImageBitmap(bitmap)
                        }
                        5 -> {
                            imvFoto5.setImageBitmap(bitmap)
                        }
                    }

                    ControllerImagenFormulario.createFormFotosReporte(
                        DBFormularioImagen(
                            rutaImagen = file!!.absolutePath,
                            tipoImagen = TIPO_IMAGEN_FOTOS,
                            pertenece = IMAGEN_FOTOS_REPORTE,
                            idFormulario = idFormulario,
                            numFoto = fotoEscogida
                        )
                    )

                    reemplazarImgAComprimida(file!!)
                    hideProgressBar()
                }

                0 -> Toast.makeText(this, "Acción cancelada", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun reemplazarImgAComprimida(file: File) {
        var fo: FileOutputStream? = null
        try {
            file.createNewFile()
            fo = FileOutputStream(file)
            fo.write(baos.toByteArray())
            fo.close()
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            ConstantPermisos.Request.REQUEST_CODE_MULTIPLE_PERMISSION -> {
                val listPermisosFaltantes = arrayListOf<PermisosApp>()
                listPermisosFaltantes.clear()

                for (permission in listPermisos){
                    if(! PermisosHelper.hasPermiso(this, permission.permiso)){
                        listPermisosFaltantes.add(permission)
                    }
                }

                if (listPermisosFaltantes.size > 0) {
                    UtilidadesHelper.mensajePermisos(this)!!
                        .setPositiveButton(getString(R.string.dialogos_aceptar), DialogInterface.OnClickListener { _, _ ->
                            PermisosHelper.validarSolicitudPermiso(
                                this,
                                listPermisosFaltantes[0].permiso,
                                listPermisosFaltantes[0].descripcionPermisoRechazo,
                                ConstantPermisos.Request.REQUEST_CODE_MULTIPLE_PERMISSION)
                        })
                        .show()
                } else {
                    Toast.makeText(this, "Permisos concedidos.", Toast.LENGTH_LONG).show()
                    launchCamera()
                }

                return
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}