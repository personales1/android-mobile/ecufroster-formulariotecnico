package com.ecufroster.formulario.view.activity.formulario_lista

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import com.ecufroster.formulario.R
import com.ecufroster.formulario.`interface`.onFormulariosPorEnviar
import com.ecufroster.formulario.core.BaseActivity
import com.ecufroster.formulario.db.controller.ControllerFormularios
import com.ecufroster.formulario.view.activity.MenuOpcionesFormularioActivity
import com.ecufroster.formulario.view.adapter.AdapterFormulariosPorEnviar
import kotlinx.android.synthetic.main.activity_formulario_list_por_enviar.*

class FormularioListPorEnviarActivity : BaseActivity(), onFormulariosPorEnviar {
    var listFormularios : ArrayList<Int> = ArrayList<Int>()
    lateinit var adapter : AdapterFormulariosPorEnviar

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState, R.layout.activity_formulario_list_por_enviar, this)
        configToolbar(R.id.toolbar, "Formularios por enviar")

        listFormularios = ControllerFormularios.getListIdFormulariosPendientesPorSubir()
    }

    override fun select(idFormulario: Int) {
        val intent = Intent(this@FormularioListPorEnviarActivity, MenuOpcionesFormularioActivity::class.java)
        intent.putExtra("idForm", idFormulario)
        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()

        adapter = AdapterFormulariosPorEnviar(listFormularios, this)
        rcvFormulariosPorEnviar.layoutManager = LinearLayoutManager(this)
        rcvFormulariosPorEnviar.setHasFixedSize(true)
        rcvFormulariosPorEnviar.adapter = adapter
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}