package com.ecufroster.formulario.view.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.ecufroster.formulario.R
import com.ecufroster.formulario.`interface`.OnDatePickerListener
import com.ecufroster.formulario.core.BaseActivity
import com.ecufroster.formulario.core.TIPO_FORMULARIO_MEDICION_CONSUMO_KWH_EN_PDV
import com.ecufroster.formulario.core.TIPO_FORMULARIO_MOVILIZACION_CABINETS
import com.ecufroster.formulario.db.controller.ControllerApiCabinets
import com.ecufroster.formulario.db.controller.ControllerApiPlacas
import com.ecufroster.formulario.db.controller.ControllerFormularios
import com.ecufroster.formulario.db.controller.ControllerInformacionCabinet
import com.ecufroster.formulario.db.model.DBFormulario
import com.ecufroster.formulario.db.model.DBFormularioInformacionCabinet
import com.ecufroster.formulario.helper.ViewHelper
import com.ecufroster.formulario.model.PopupListSelected
import com.ecufroster.formulario.retrofit.model.response.ApiResponseCabinet
import kotlinx.android.synthetic.main.activity_informacion_cabinet.*
import kotlinx.android.synthetic.main.layout_editext_fecha.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class InformacionCabinetActivity : BaseActivity() {
    var idFormulario: Int = 0
    var formulario = DBFormulario()
    var informacionCabinet = DBFormularioInformacionCabinet()
    var listCabinet: ArrayList<ApiResponseCabinet> = ArrayList<ApiResponseCabinet>()
    var listYear: ArrayList<String> = arrayListOf()
    var listPopupListSelected = ArrayList<PopupListSelected>()
    var descripcionCabinet : String = ""
    var positionCabinetSeleccionado : Int = -1
    val LISTA_CABINET : Int = 1
    var mensaje: String = ""

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState, R.layout.activity_informacion_cabinet, this)
        configToolbar(R.id.toolbar, "Información básica del cabinet")

        if(intent.hasExtra("id_formulario")){
            idFormulario = intent.getIntExtra("id_formulario", 0)
            formulario = ControllerFormularios.getDataByIdForm(idFormulario)
        }else{
            Toast.makeText(this, "No se puede iniciar formulario por falta de información", Toast.LENGTH_LONG).show()
            finish()
        }

        if(formulario.tipoFormulario == TIPO_FORMULARIO_MOVILIZACION_CABINETS) {
            tvLabelAnioImportacion.visibility = View.GONE
            edtInfoCabinetAnioImportacion.visibility = View.GONE
        }
        else if(formulario.tipoFormulario == TIPO_FORMULARIO_MEDICION_CONSUMO_KWH_EN_PDV) {
            tvLabelAnioImportacion.visibility = View.VISIBLE
            edtInfoCabinetAnioImportacion.visibility = View.VISIBLE
            edtInfoCabinetAnioImportacion.isEnabled = true
        }

        for(i in Calendar.getInstance().get(Calendar.YEAR) downTo 2000){
            listYear.add(i.toString())
        }

        listCabinet = ControllerApiCabinets.getCabinet()
        informacionCabinet = ControllerInformacionCabinet.getDataByIdForm(idFormulario)
        setData()

        if(ControllerFormularios.isFormEnviado(idFormulario)){
            deshabilitaCampos()
        }

        edtInfoCabinetMarca.setOnClickListener { selectedCabinet() }
        fabInfoCabinetGuardar.setOnClickListener { guardarForm() }

        edtInfoCabinetMarca.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if(edtInfoCabinetMarca.text.toString().trim().isNotEmpty()){
                    edtInfoCabinetMarca.error = null
                }
            }
        })

        edtInfoCabinetPlaca.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if(edtInfoCabinetPlaca.text.toString().trim().isNotEmpty()){
                    edtInfoCabinetPlaca.error = null
                }

                if(formulario.tipoFormulario != TIPO_FORMULARIO_MEDICION_CONSUMO_KWH_EN_PDV) {
                    val anioImportacion = ControllerApiPlacas.getFormByPlaca(s.toString()).anioImportacion.toString()
                    edtInfoCabinetAnioImportacion.setText(anioImportacion)
                }
            }
        })
    }

    fun deshabilitaCampos(){
        edtInfoCabinetMarca.isEnabled = false
        edtInfoCabinetModelo.isEnabled = false
        edtInfoCabinetTipoEquipo.isEnabled = false
        edtInfoCabinetCanastillasParrillas.isEnabled = false
        edtInfoCabinetPlaca.isEnabled = false
        edtInfoCabinetSerie.isEnabled = false
        edtInfoCabinetAnioImportacion.isEnabled = false
        fabInfoCabinetGuardar.visibility = View.GONE
    }

    fun setData(){
        edtInfoCabinetModelo.setText(informacionCabinet.modelo)
        edtInfoCabinetTipoEquipo.setText(informacionCabinet.tipoEquipo)
        edtInfoCabinetPlaca.setText(informacionCabinet.placa)
        edtInfoCabinetSerie.setText(informacionCabinet.serie)
        edtInfoCabinetCanastillasParrillas.setText(informacionCabinet.numCanastilas)
        edtInfoCabinetAnioImportacion.setText(informacionCabinet.anioImportacion)

        if(informacionCabinet.idFormulario != -1){
            val indexAux = listCabinet.indexOfFirst { it.marca.toLowerCase()+"/"+it.modelo.toLowerCase()+"/"+it.tipo.toLowerCase() == informacionCabinet.marca.toLowerCase()+"/"+informacionCabinet.modelo.toLowerCase()+"/"+informacionCabinet.tipoEquipo.toLowerCase() }
            positionCabinetSeleccionado = indexAux

            if(indexAux >= 0){
                edtInfoCabinetMarca.setText(listCabinet[positionCabinetSeleccionado].marca)
            }else{
                edtInfoCabinetMarca.setText(informacionCabinet.marca)
            }
        }
        else{
            edtInfoCabinetMarca.setText(informacionCabinet.marca)
        }
    }

    fun selectedCabinet(){
        ViewHelper.hideKeyboardInActivity(this)
        listPopupListSelected.clear()

        for(item in listCabinet){
            descripcionCabinet = item.marca + " / " + item.modelo + " / " + item.tipo
            listPopupListSelected.add(PopupListSelected(descripcion = descripcionCabinet))
        }

        val intent = Intent(this, PopupListSelectedActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        intent.putExtra("index", positionCabinetSeleccionado)
        intent.putExtra("titulo", "Cabinet (Marca / Modelo / Tipo)")
        intent.putParcelableArrayListExtra("lista", listPopupListSelected)
        startActivityForResult(intent, LISTA_CABINET)
    }

    fun datePickerDialog() {
        ViewHelper.hideKeyboardInActivity(this)
        ViewHelper.showCalendarDateBirth(this, 2020, 2, 20
            , object : OnDatePickerListener {
                override fun onSelected(year: Int, monthOfYear: Int, dayOfMonth: Int) {
                }
            })
    }

    fun formValido(): Boolean{
        var retorna = true

        if(edtInfoCabinetMarca.text.toString().trim().isEmpty()){
            edtInfoCabinetMarca.error = getString(R.string.campo_necesario)
            retorna = false
        }
        else{
            edtInfoCabinetMarca.error = null
        }

        if(edtInfoCabinetPlaca.text.toString().trim().isEmpty()){
            edtInfoCabinetPlaca.error = getString(R.string.campo_necesario)
            retorna = false
        }
        else{
            edtInfoCabinetPlaca.error = null
        }

        if(formulario.tipoFormulario == TIPO_FORMULARIO_MEDICION_CONSUMO_KWH_EN_PDV) {
            if(edtInfoCabinetAnioImportacion.text.toString().trim().isEmpty()){
                edtInfoCabinetAnioImportacion.error = getString(R.string.campo_necesario)
                retorna = false
            }
            else{
                edtInfoCabinetAnioImportacion.error = null
            }
        }

        return retorna
    }

    fun guardarForm(){
        informacionCabinet.marca = edtInfoCabinetMarca.text.toString().trim()
        informacionCabinet.modelo = edtInfoCabinetModelo.text.toString().trim()
        informacionCabinet.tipoEquipo = edtInfoCabinetTipoEquipo.text.toString().trim()
        informacionCabinet.numCanastilas = edtInfoCabinetCanastillasParrillas.text.toString().trim()
        informacionCabinet.placa = edtInfoCabinetPlaca.text.toString().trim()
        informacionCabinet.serie = edtInfoCabinetSerie.text.toString().trim()
        informacionCabinet.anioImportacion = edtInfoCabinetAnioImportacion.text.toString().trim()
        informacionCabinet.idFormulario = idFormulario

        if(formValido()){
            mensaje = getString(R.string.guardado_exitoso)
            informacionCabinet.formularioFinalizado = 1
        }
        else{
            mensaje = getString(R.string.error_campo_faltante)
            informacionCabinet.formularioFinalizado = 0
        }

        ControllerInformacionCabinet.createForm(informacionCabinet)
        Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show()
        finish()
    }

    @SuppressLint("SetTextI18n")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            val indexPosition = data!!.getIntExtra("indexItem", 0)

            if (requestCode == LISTA_CABINET) {
                positionCabinetSeleccionado = indexPosition

                informacionCabinet.marca = listCabinet[indexPosition].marca
                informacionCabinet.modelo = listCabinet[indexPosition].modelo
                informacionCabinet.tipoEquipo = listCabinet[indexPosition].tipo
                informacionCabinet.numCanastilas = listCabinet[indexPosition].canastillasParrillas

                setData()
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}