package com.ecufroster.formulario.view.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.ecufroster.formulario.R
import com.ecufroster.formulario.core.*
import com.ecufroster.formulario.db.controller.*
import com.ecufroster.formulario.helper.UtilidadesHelper
import com.ecufroster.formulario.helper.ViewHelper
import com.ecufroster.formulario.`interface`.OnApiResponse
import com.ecufroster.formulario.model.PopupListSelected
import com.ecufroster.formulario.retrofit.RecursoApi
import com.ecufroster.formulario.retrofit.error.ServiceError
import com.ecufroster.formulario.retrofit.model.request.RequestFirmaTecnico
import com.ecufroster.formulario.retrofit.model.request.RequestFormulario
import com.ecufroster.formulario.retrofit.model.response.*
import com.ecufroster.formulario.retrofit.service.Api
import com.ecufroster.formulario.view.activity.formulario_lista.FormularioListEnviadasActivity
import com.ecufroster.formulario.view.activity.formulario_lista.FormularioListPorEnviarActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.io.ByteArrayOutputStream
import java.io.File

class MainActivity : BaseActivity() {
    var listIdForm: ArrayList<Int> = ArrayList()
    var listIdFormError: ArrayList<Int> = ArrayList()
    var listPopupListSelected = ArrayList<PopupListSelected>()
    var listTiposFormulario = ArrayList<ApiResponseTiposFormulario>()
    var positionTipoFormSeleccionado : Int = 0
    val LISTA_TIPO_FORM : Int = 1

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState, R.layout.activity_main, this)
        configToolbar(R.id.toolbar, getString(R.string.app_name) + " Formulario Técnico", false)
        configProgressBar(R.id.layoutProgressBar)
        configProgressBarDescriptivo(R.id.contenedorProgressbarDescriptivo)

        listIdFormError.clear()

        imvFormularios.setOnClickListener { iniciarFormulario() }
        imvDescargaDatos.setOnClickListener { descargarDatos() }
        imvCargaDatos.setOnClickListener { cargarDatos() }
    }

    fun iniciarFormulario(){
        if(ControllerFormularios.existFormPending()){
            if(ControllerApiClientes.existData() && ControllerApiTiposFormulario.existData() && ControllerApiCabinets.existData()){
                val intent = Intent(this@MainActivity, MenuOpcionesFormularioActivity::class.java)
                startActivity(intent)
            }
            else{
                UtilidadesHelper.mensajeAlerta(
                    this@MainActivity,
                    "Advertencia",
                    "No puede iniciar formulario debido a que tienes información pendiente por descargar. Por favor conéctate a internet y descarga los datos."
                ).show()
            }
        }
        else{
            UtilidadesHelper.mensajeConfirmacion(
                this@MainActivity,
                getString(R.string.app_name),
                "Vas a iniciar un formulario nuevo, recuerda que debes guardar el formulario que vas a iniciar para luego realizar otro."
            )
                ?.setPositiveButton("Iniciar formulario", DialogInterface.OnClickListener { _, _ ->

                    if (ControllerApiClientes.existData() && ControllerApiTiposFormulario.existData() && ControllerApiCabinets.existData()) {
                        Toast.makeText(
                            AppController.mInstance.applicationContext,
                            "Debes seleccionar un tipo de formulario para iniciar el trabajo",
                            Toast.LENGTH_LONG
                        ).show()

                        selectedTipoFormulario()
                    } else {
                        UtilidadesHelper.mensajeAlerta(
                            this@MainActivity,
                            "Advertencia",
                            "No puedes iniciar un formulario debido a que tienes información pendiente por descargar. Por favor conéctate a internet y descarga los datos."
                        ).show()
                    }
                })
                ?.setNegativeButton("Cancelar", DialogInterface.OnClickListener { _, _ -> })
                ?.show()
        }
    }

    fun selectedTipoFormulario(){
        listTiposFormulario = ControllerApiTiposFormulario.getData()
        ViewHelper.hideKeyboardInActivity(this)
        listPopupListSelected.clear()

        for(item in listTiposFormulario){
            listPopupListSelected.add(PopupListSelected(uid = item.codigo, descripcion = item.nombre))
        }

        val intent = Intent(this, PopupListSelectedActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        intent.putExtra("index", positionTipoFormSeleccionado)
        intent.putExtra("titulo", "Selecciona un tipo de formulario para iniciar el trabajo.")
        intent.putExtra("seleccion_rapida", false)
        intent.putParcelableArrayListExtra("lista", listPopupListSelected)
        startActivityForResult(intent, LISTA_TIPO_FORM)
    }

    fun descargarDatos(){
        UtilidadesHelper.mensajeAlerta(
            this@MainActivity,
            "Aviso",
            "Recuerda tener una conexión a internet para realizar esta acción."
        )
            .setPositiveButton("Descargar datos", DialogInterface.OnClickListener { _, _ ->
                downloadDataFromApiCliente()

            })
            .setNegativeButton("Cancelar", DialogInterface.OnClickListener { _, _ -> })
            .show()
    }

    fun downloadDataFromApiCliente(){
        showProgressBarDescriptivo("Descargando datos de clientes...")
        val apiService = Api.settingService()
        val objetoApi = apiService.getClientes()

        RecursoApi.consumeApi(objetoApi, object : OnApiResponse<List<ApiResponseCliente>> {
            override fun onSuccess(response: List<ApiResponseCliente>) {
                ControllerApiClientes.deleteAll()
                ControllerApiClientes.createNewClient(response)
                downloadDataFromApiTipoForm()
            }

            override fun onError(error: ServiceError) {
                hideProgressBarDescriptivo()
                mostrarMensajeErrorResponseReintentar(this@MainActivity, error)
            }

            override fun onUpdateApp(error: ServiceError) {
                hideProgressBarDescriptivo()
                ViewHelper.alertaActualizaApp(
                    this@MainActivity,
                    error.noticias.titulo,
                    error.noticias.mensaje
                )
            }

            override fun onUnAuthenticated(error: ServiceError) {
                hideProgressBarDescriptivo()
                mostrarMensajeErrorResponse(this@MainActivity, error)
            }

            override fun onUnknownError() {
                hideProgressBarDescriptivo()
                mostrarMensajeErrorServidor(this@MainActivity)
            }

            override fun onFailure(throwable: Throwable) {
                hideProgressBarDescriptivo()
                mostrarMensajeErrorThrowable(this@MainActivity, throwable)
            }

            override fun onErrorException(error: Exception) {
                hideProgressBarDescriptivo()
                mostrarMensajeErrorException(this@MainActivity, error)
            }

            override fun onWithoutInternet() {
                hideProgressBarDescriptivo()
                mostrarMensajeErrorInternet(this@MainActivity)
            }
        })
    }

    fun downloadDataFromApiTipoForm(){
        showProgressBarDescriptivo("Descargando datos de tipos de formularios...")
        val apiService = Api.settingService()
        val objetoApi = apiService.getTiposFormularios()

        RecursoApi.consumeApi(objetoApi, object : OnApiResponse<List<ApiResponseTiposFormulario>> {
            override fun onSuccess(response: List<ApiResponseTiposFormulario>) {
                ControllerApiTiposFormulario.deleteAll()
                ControllerApiTiposFormulario.createNew(response)
                downloadDataFromApiCabinets()
            }

            override fun onError(error: ServiceError) {
                hideProgressBarDescriptivo()
                mostrarMensajeErrorResponseReintentar(this@MainActivity, error)
            }

            override fun onUpdateApp(error: ServiceError) {
                hideProgressBarDescriptivo()
                ViewHelper.alertaActualizaApp(
                    this@MainActivity,
                    error.noticias.titulo,
                    error.noticias.mensaje
                )
            }

            override fun onUnAuthenticated(error: ServiceError) {
                hideProgressBarDescriptivo()
                mostrarMensajeErrorResponse(this@MainActivity, error)
            }

            override fun onUnknownError() {
                hideProgressBarDescriptivo()
                mostrarMensajeErrorServidor(this@MainActivity)
            }

            override fun onFailure(throwable: Throwable) {
                hideProgressBarDescriptivo()
                mostrarMensajeErrorThrowable(this@MainActivity, throwable)
            }

            override fun onErrorException(error: Exception) {
                hideProgressBarDescriptivo()
                mostrarMensajeErrorException(this@MainActivity, error)
            }

            override fun onWithoutInternet() {
                hideProgressBarDescriptivo()
                mostrarMensajeErrorInternet(this@MainActivity)
            }
        })
    }

    fun downloadDataFromApiCabinets(){
        showProgressBarDescriptivo("Descargando datos de cabinets...")
        val apiService = Api.settingService()
        val objetoApi = apiService.getCabinets()

        RecursoApi.consumeApi(objetoApi, object : OnApiResponse<List<ApiResponseCabinet>> {
            override fun onSuccess(response: List<ApiResponseCabinet>) {
                ControllerApiCabinets.deleteAll()
                ControllerApiCabinets.createNew(response)
                downloadDataFromApiPlacas()
            }

            override fun onError(error: ServiceError) {
                hideProgressBarDescriptivo()
                mostrarMensajeErrorResponseReintentar(this@MainActivity, error)
            }

            override fun onUpdateApp(error: ServiceError) {
                hideProgressBarDescriptivo()
                ViewHelper.alertaActualizaApp(
                    this@MainActivity,
                    error.noticias.titulo,
                    error.noticias.mensaje
                )
            }

            override fun onUnAuthenticated(error: ServiceError) {
                hideProgressBarDescriptivo()
                mostrarMensajeErrorResponse(this@MainActivity, error)
            }

            override fun onUnknownError() {
                hideProgressBarDescriptivo()
                mostrarMensajeErrorServidor(this@MainActivity)
            }

            override fun onFailure(throwable: Throwable) {
                hideProgressBarDescriptivo()
                mostrarMensajeErrorThrowable(this@MainActivity, throwable)
            }

            override fun onErrorException(error: Exception) {
                hideProgressBarDescriptivo()
                mostrarMensajeErrorException(this@MainActivity, error)
            }

            override fun onWithoutInternet() {
                hideProgressBarDescriptivo()
                mostrarMensajeErrorInternet(this@MainActivity)
            }
        })
    }

    fun downloadDataFromApiPlacas(){
        showProgressBarDescriptivo("Descargando datos de placas...")
        val apiService = Api.settingService()
        val objetoApi = apiService.getPlacas()

        RecursoApi.consumeApi(objetoApi, object : OnApiResponse<List<ApiResponsePlaca>> {
            override fun onSuccess(response: List<ApiResponsePlaca>) {
                ControllerApiPlacas.deleteAll()
                ControllerApiPlacas.createNew(response)

                hideProgressBarDescriptivo()

                UtilidadesHelper.mensajeConfirmacion(
                    this@MainActivity,
                    getString(R.string.app_name),
                    "Toda la información se ha descargado exitosamente"
                )?.show()
            }

            override fun onError(error: ServiceError) {
                hideProgressBarDescriptivo()
                mostrarMensajeErrorResponseReintentar(this@MainActivity, error)
            }

            override fun onUpdateApp(error: ServiceError) {
                hideProgressBarDescriptivo()
                ViewHelper.alertaActualizaApp(
                    this@MainActivity,
                    error.noticias.titulo,
                    error.noticias.mensaje
                )
            }

            override fun onUnAuthenticated(error: ServiceError) {
                hideProgressBarDescriptivo()
                mostrarMensajeErrorResponse(this@MainActivity, error)
            }

            override fun onUnknownError() {
                hideProgressBarDescriptivo()
                mostrarMensajeErrorServidor(this@MainActivity)
            }

            override fun onFailure(throwable: Throwable) {
                hideProgressBarDescriptivo()
                mostrarMensajeErrorThrowable(this@MainActivity, throwable)
            }

            override fun onErrorException(error: Exception) {
                hideProgressBarDescriptivo()
                mostrarMensajeErrorException(this@MainActivity, error)
            }

            override fun onWithoutInternet() {
                hideProgressBarDescriptivo()
                mostrarMensajeErrorInternet(this@MainActivity)
            }
        })
    }


    fun cargarDatos(){
        if(ControllerFormularios.existFormPending()){
            UtilidadesHelper.mensajeAlerta(
                this,
                "Atención",
                "Tienes un formulario pendiente de terminar. ¿Aún deseas seguir con el proceso de subir la información al servidor?"
            )
                .setPositiveButton("Aceptar", DialogInterface.OnClickListener { _, _ ->
                    confirmarSubirFormularios()
                })
                .setNegativeButton("Cancelar", DialogInterface.OnClickListener { _, _ -> })
                .setCancelable(false)
                .show()
        }
        else{
            confirmarSubirFormularios()
        }
    }

    fun confirmarSubirFormularios(){
        UtilidadesHelper.mensajeConfirmacion(
            this,
            "Advertencia",
            "Al subir los formularios, estos se borrarán de la app y no podrás verlos ni editarlos. ¿Estás seguro de subir la información ahora?"
        )!!
            .setPositiveButton("Aceptar", DialogInterface.OnClickListener { _, _ ->
                listIdForm = ControllerFormularios.getListIdFormulariosFinalizadosPorSubir()

                if (listIdForm.size == 0) {
                    UtilidadesHelper.mensajeAlerta(
                        this,
                        "Atención",
                        "No tienes información que subir."
                    )
                        .setCancelable(false)
                        .show()
                } else {
                    listIdFormError.clear()
                    enviarFormulario(0)
                }
            })
            .setNegativeButton("Cancelar", DialogInterface.OnClickListener { _, _ -> })
            .setCancelable(false)
            .show()
    }

    fun enviarFormulario(item: Int){
        var hayErrorFoto = false

        if(item < listIdForm.size){
            showProgressBarDescriptivo("Subiendo datos del formulario ${listIdForm[item]} ...")
            val request = armarRequest(item)


            //BASE 64 FOTOS REPORTE
            for((index, itemFoto) in request.formFotosReporte.withIndex()){
                val fileFotos = File(itemFoto.rutaImagen)
                if(fileFotos.exists()){
                    val bitmapFotos = MediaStore.Images.Media.getBitmap(applicationContext.contentResolver, Uri.fromFile(fileFotos))
                    val outFotos = ByteArrayOutputStream()
                    bitmapFotos.compress(Bitmap.CompressFormat.PNG, 40, outFotos)
                    val byteArrayFotos = outFotos.toByteArray()
                    val encoded64Fotos = Base64.encodeToString(byteArrayFotos, Base64.DEFAULT)
                    request.formFotosReporte[index].rutaImagen = encoded64Fotos
                }
                else{
                    hayErrorFoto = true
                    break
                }
            }


            if(hayErrorFoto){
                listIdFormError.add(listIdForm[item])
                enviarFormulario(item + 1)
            }
            else{
                //BASE 64 FIRMA CLIENTE
                val file = File(request.formFirmaCliente.rutaImagen)
                val bitmap = MediaStore.Images.Media.getBitmap(applicationContext.contentResolver, Uri.fromFile(file))
                val out = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.PNG, 40, out)
                val byteArray = out.toByteArray()
                val encoded64 = Base64.encodeToString(byteArray, Base64.DEFAULT)
                request.formFirmaCliente.rutaImagen = encoded64


                val apiService = Api.settingService()
                val objetoApi = apiService.enviarFormulario(request)

                RecursoApi.consumeApi(objetoApi, object : OnApiResponse<ResponseFormulario> {
                    override fun onSuccess(response: ResponseFormulario) {
                        ControllerFormularios.setFormEnviado(listIdForm[item])
                        enviarFormulario(item + 1)
                    }

                    override fun onError(error: ServiceError) {
                        listIdFormError.add(listIdForm[item])
                        enviarFormulario(item + 1)
                    }

                    override fun onUpdateApp(error: ServiceError) {
                    }

                    override fun onUnAuthenticated(error: ServiceError) {
                    }

                    override fun onUnknownError() {
                    }

                    override fun onFailure(throwable: Throwable) {
                        listIdFormError.add(listIdForm[item])
                        enviarFormulario(item + 1)
                    }

                    override fun onErrorException(error: Exception) {
                    }

                    override fun onWithoutInternet() {
                        hideProgressBarDescriptivo()
                        UtilidadesHelper.mensajeAlerta(
                            this@MainActivity, getString(R.string.error_internet_title), getString(
                                R.string.error_internet_message
                            )
                        )
                            .setCancelable(false)
                            .show()
                    }
                })
            }
        }
        else{
            if(listIdForm.size > 0 && listIdForm.size > listIdFormError.size){
                enviarFirmaTecnico()
            }
            else{
                if(listIdForm.size == 0){
                    hideProgressBarDescriptivo()
                    UtilidadesHelper.mensajeConfirmacion(
                        this@MainActivity,
                        "¡Atención!",
                        "No tienes datos que subir, proceda a ingresar datos en los formularios e intenta nuevamente de enviar al servidor."
                    )!!
                        .setCancelable(false)
                        .show()
                }
                else{
                    mensajeFinalizar()
                }
            }
        }
    }

    fun armarRequest(index: Int): RequestFormulario{
        return RequestFormulario(
            idFormularioApp = listIdForm[index],
            idUsuarioApp = preferencias.getPrefPerfilId(),
            formularioApp = ControllerFormularios.getDataByIdForm(listIdForm[index]),
            formInformacionFormulario = ControllerInformacionFormulario.getDataByIdForm(listIdForm[index]),
            formInformacionCliente = ControllerInformacionCliente.getDataByIdForm(listIdForm[index]),
            formInformacionBasicaCabinet = ControllerInformacionCabinet.getDataByIdForm(listIdForm[index]),
            formInformacionTecnicaCabinet = ControllerInformacionCabinetTecnico.getDataByIdForm(listIdForm[index]),
            formObservacionTecnica = ControllerObservacionTecnica.getDataByIdForm(listIdForm[index]),
            formFirmaCliente = ControllerImagenFormulario.getFirmaByIdForm(listIdForm[index], IMAGEN_FIRMA_CLIENTE),
            formFotosReporte = ControllerImagenFormulario.getFotosByIdForm(listIdForm[index], IMAGEN_FOTOS_REPORTE)
        )
    }

    fun enviarFirmaTecnico(){
        showProgressBarDescriptivo("Subiendo firma del técnico...")

        val file = File(preferencias.getPrefFirmaUsuarioApp())
        val bitmap = MediaStore.Images.Media.getBitmap(
            applicationContext.contentResolver, Uri.fromFile(
                file
            )
        )
        val out = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 40, out)
        val byteArray = out.toByteArray()
        val encoded64 = Base64.encodeToString(byteArray, Base64.DEFAULT)

        val requestFirma = RequestFirmaTecnico(
            idTecnico = preferencias.getPrefPerfilId(),
            firma = encoded64
        )

        val apiService = Api.settingService()
        val objetoApi = apiService.enviarFirmaTecnico(requestFirma)

        RecursoApi.consumeApi(objetoApi, object : OnApiResponse<ResponseFormulario> {
            override fun onSuccess(response: ResponseFormulario) {
                mensajeFinalizar()
            }

            override fun onError(error: ServiceError) {
                Toast.makeText(
                    this@MainActivity,
                    "Hubo un error, intente nuevamente.",
                    Toast.LENGTH_LONG
                ).show()
                mensajeFinalizar()
            }

            override fun onUpdateApp(error: ServiceError) {
            }

            override fun onUnAuthenticated(error: ServiceError) {
            }

            override fun onUnknownError() {
            }

            override fun onFailure(throwable: Throwable) {
                Toast.makeText(
                    this@MainActivity,
                    "Error onFailure " + throwable.message,
                    Toast.LENGTH_LONG
                ).show()
                mensajeFinalizar()
            }

            override fun onErrorException(error: Exception) {
            }

            override fun onWithoutInternet() {
                hideProgressBarDescriptivo()
                UtilidadesHelper.mensajeAlerta(
                    this@MainActivity, getString(R.string.error_internet_title), getString(
                        R.string.error_internet_message
                    )
                )
                    .setCancelable(false)
                    .show()
            }
        })
    }

    fun mensajeFinalizar(){
        hideProgressBarDescriptivo()

        if(listIdFormError.size > 0){
            UtilidadesHelper.mensajeAlerta(
                this,
                "!Alerta!",
                "Hay formularios que presentaron error al subir al servidor, los formularios con errores son:  " + listIdFormError.joinToString(
                    separator = ":"
                )
            )
                .setCancelable(false)
                .show()
        }else{
            UtilidadesHelper.mensajeConfirmacion(
                this,
                getString(R.string.app_name),
                "Toda la información se ha subido al servidor satisfactoriamente."
            )!!
                .setCancelable(false)
                .show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_principal, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_cerrar_sesion -> {
                UtilidadesHelper.mensajeConfirmacion(
                    this@MainActivity,
                    "¡Atención!",
                    "¿Estás seguro de querer finalizar sesión?"
                )!!
                    .setCancelable(false)
                    .setPositiveButton("Aceptar", DialogInterface.OnClickListener { _, _ ->
                        UtilidadesHelper.clearAllSharedPreferences(this@MainActivity)
                        UtilidadesHelper.limpiarBaseDeDatos(this@MainActivity)
                        UtilidadesHelper.llamarPantallaPrincipal()
                    })
                    .setNegativeButton("Cancelar", DialogInterface.OnClickListener { _, _ -> })
                    .show()

                return true
            }
            R.id.menu_formularios_por_enviar -> {
                val intent = Intent(this@MainActivity, FormularioListPorEnviarActivity::class.java)
                startActivity(intent)
            }
            R.id.menu_formularios_enviado -> {
                val intent = Intent(this@MainActivity, FormularioListEnviadasActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    @SuppressLint("SetTextI18n")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            val indexPosition = data!!.getIntExtra("indexItem", 0)

            if (requestCode == LISTA_TIPO_FORM) {
                //positionTipoFormSeleccionado = indexPosition

                val idFormulario = ControllerFormularios.createNewForm(
                    listTiposFormulario[indexPosition].id.toInt(),
                    listTiposFormulario[indexPosition].codigo,
                )

                val intent = Intent(this@MainActivity, MenuOpcionesFormularioActivity::class.java)
                intent.putExtra("nuevo", "S")
                intent.putExtra("idForm", idFormulario)
                startActivity(intent)
            }
        }
    }
}