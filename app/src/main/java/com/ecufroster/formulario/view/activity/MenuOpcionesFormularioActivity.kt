package com.ecufroster.formulario.view.activity

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.ecufroster.formulario.R
import com.ecufroster.formulario.core.*
import com.ecufroster.formulario.db.controller.*
import com.ecufroster.formulario.db.model.DBFormulario
import com.ecufroster.formulario.helper.UtilidadesHelper
import com.ecufroster.formulario.`interface`.onMenuOpcionesListener
import com.ecufroster.formulario.model.MenuOpciones
import com.ecufroster.formulario.retrofit.model.response.ApiResponseTiposFormulario
import com.ecufroster.formulario.view.adapter.AdapterMenuOpciones
import kotlinx.android.synthetic.main.activity_informacion_formulario.*
import kotlinx.android.synthetic.main.activity_menu_opciones_formulario.*

class MenuOpcionesFormularioActivity : BaseActivity(), onMenuOpcionesListener {
    var listMenu: ArrayList<MenuOpciones>? = null
    val adapter: AdapterMenuOpciones = AdapterMenuOpciones()
    var idFormulario: Int = 0
    var formulario = DBFormulario()
    var listTiposFormulario = ArrayList<ApiResponseTiposFormulario>()

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState, R.layout.activity_menu_opciones_formulario, this)

        listMenu = arrayListOf()

        if(intent.hasExtra("nuevo")){
            listTiposFormulario = ControllerApiTiposFormulario.getData()
            idFormulario = intent.getIntExtra("idForm", 0)
        }
        else{
            idFormulario = if(intent.hasExtra("idForm")){
                intent.getIntExtra("idForm", 0)
            } else{
                ControllerFormularios.getIdFormPending()
            }
        }

        configToolbar(R.id.toolbar, "Formulario $idFormulario")
        inicializeActivity()

        btnFinalizarForm.setOnClickListener { finalizarFormulario() }
    }

    private fun inicializeActivity() {
        formulario = ControllerFormularios.getDataByIdForm(idFormulario)
        setData()
        configRecycler()
        setListaMenu()
    }

    @SuppressLint("SetTextI18n")
    private fun setData() {
        tvTipoFormulario.text = ControllerApiTiposFormulario.getFormByCode(formulario.tipoFormulario).nombre
        tvTecnicoNombre.text = preferencias.getPrefPerfilNombre()
        tvTecnicoIdentificacion.text = "C.I. " + preferencias.getPrefPerfilIdentificacion()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun configRecycler() {
        rcvMenuOpciones.layoutManager = LinearLayoutManager(this)
        adapter.MenuOpcionesAdapter(listMenu!!, this)
        rcvMenuOpciones.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    fun setListaMenu(){
        val menuObservacionTecnica = if(formulario.tipoFormulario == TIPO_FORMULARIO_MOVILIZACION_CABINETS) {
            "Observación del movimiento"
        } else {
            "Observación técnica"
        }

        listMenu!!.clear()
        listMenu!!.add(MenuOpciones("Información del formulario", RUTA_MENU + "InformacionFormularioActivity", false))
        listMenu!!.add(MenuOpciones("Información del cliente/usuario", RUTA_MENU + "InformacionClienteActivity", false))
        listMenu!!.add(MenuOpciones("Información básica del cabinet", RUTA_MENU + "InformacionCabinetActivity", false))

        if(formulario.tipoFormulario != TIPO_FORMULARIO_MOVILIZACION_CABINETS) {
            listMenu!!.add(MenuOpciones("Información técnica del cabinet", RUTA_MENU + "InformacionCabinetTecnicaActivity", false))
        }

        listMenu!!.add(MenuOpciones(menuObservacionTecnica, RUTA_MENU + "ObservacionTecnicaActivity", false))
        listMenu!!.add(MenuOpciones("Reporte de fotos/evidencias", RUTA_MENU + "FotosEvidenciaActivity", false))
        listMenu!!.add(MenuOpciones("Firma del técnico/usuario", RUTA_MENU + "FirmaVisitaActivity", false))
    }

    fun finalizarFormulario(){
        if(
            listMenu!![0].completo &&
            listMenu!![1].completo
        ){

            var formCompleto = false

            if(formulario.tipoFormulario == TIPO_FORMULARIO_MOVILIZACION_CABINETS) {
                if(
                    listMenu!![0].completo &&
                    listMenu!![1].completo &&
                    listMenu!![2].completo &&
                    listMenu!![3].completo &&
                    listMenu!![4].completo &&
                    listMenu!![5].completo
                ) {
                    formCompleto = true
                }
            } else {
                if(
                    listMenu!![0].completo &&
                    listMenu!![1].completo &&
                    listMenu!![2].completo &&
                    listMenu!![3].completo &&
                    listMenu!![4].completo &&
                    listMenu!![5].completo &&
                    listMenu!![6].completo
                ) {
                    formCompleto = true
                }
            }

            if(formCompleto) {
                ControllerFormularios.setFormFinalizado(1, idFormulario)
            }
            else {
                ControllerFormularios.setFormFinalizado(0, idFormulario)
            }

            if(ControllerFormularios.isFormFinalizado(idFormulario)){
                UtilidadesHelper.mensajeConfirmacion(this, getString(R.string.app_name), "Su formulario está listo para ser enviado al servidor en cualquier momento, solo asegúrese de tener conexión a internet cuando lo haga.")!!
                    .setPositiveButton("Entendido", DialogInterface.OnClickListener { _, _ ->
                        finish()
                    })
                    .setCancelable(false)
                    .show()
            }
            else{
                UtilidadesHelper.mensajeAlerta(this, "¡Atención!", "El formulario está incompleto, recuerda que para subir la información debes terminar de llenar todas las secciones. Para seguir completando el formulario puedes ir a la opción 'Formularios por enviar' que se encuentra en la pantalla inicial de la app.")
                    .setPositiveButton("Entendido", DialogInterface.OnClickListener { _, _ ->
                        finish()
                    })
                    .setCancelable(false)
                    .show()
            }
        }
        else{
            UtilidadesHelper.mensajeAlerta(this, "Atención", "Se requiere que al menos completes las secciones '${listMenu!![0].titulo}' e '${listMenu!![1].titulo}' para guardar el formulario.")
                .setPositiveButton("OK", DialogInterface.OnClickListener { _, _ ->  })
                .setCancelable(false)
                .show()
        }
    }

    override fun seleccionarOpcion(menuOpciones: MenuOpciones) {
        val i = Intent()
        i.setClassName(applicationContext,packageName + menuOpciones.nombre_clase)
        i.putExtra("id_formulario", idFormulario)
        startActivity(i)
    }

    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()

        if(idFormulario > 0) {
            actualizaListadoOpciones()
        }
    }

    @SuppressLint("MissingPermission", "NotifyDataSetChanged")
    private fun actualizaListadoOpciones() {
        if(formulario.tipoFormulario == TIPO_FORMULARIO_MOVILIZACION_CABINETS) {
            listMenu!![0].completo = ControllerInformacionFormulario.formIsFinalizado(idFormulario)
            listMenu!![1].completo = ControllerInformacionCliente.formIsFinalizado(idFormulario)
            listMenu!![2].completo = ControllerInformacionCabinet.formIsFinalizado(idFormulario)
            listMenu!![3].completo = ControllerObservacionTecnica.formIsFinalizado(idFormulario)
            listMenu!![4].completo = ControllerImagenFormulario.getFotosByIdForm(idFormulario, IMAGEN_FOTOS_REPORTE).size > 0
            listMenu!![5].completo = preferencias.getPrefFirmaUsuarioApp().trim().isNotEmpty() && ControllerImagenFormulario.getFirmaByIdForm(idFormulario, IMAGEN_FIRMA_CLIENTE).rutaImagen.trim().isNotEmpty()
        } else {
            listMenu!![0].completo = ControllerInformacionFormulario.formIsFinalizado(idFormulario)
            listMenu!![1].completo = ControllerInformacionCliente.formIsFinalizado(idFormulario)
            listMenu!![2].completo = ControllerInformacionCabinet.formIsFinalizado(idFormulario)
            listMenu!![3].completo = ControllerInformacionCabinetTecnico.formIsFinalizado(idFormulario)
            listMenu!![4].completo = ControllerObservacionTecnica.formIsFinalizado(idFormulario)
            listMenu!![5].completo = ControllerImagenFormulario.getFotosByIdForm(idFormulario, IMAGEN_FOTOS_REPORTE).size > 0
            listMenu!![6].completo = preferencias.getPrefFirmaUsuarioApp().trim().isNotEmpty() && ControllerImagenFormulario.getFirmaByIdForm(idFormulario, IMAGEN_FIRMA_CLIENTE).rutaImagen.trim().isNotEmpty()
        }

        adapter.notifyDataSetChanged()

        btnFinalizarForm.visibility = View.VISIBLE
        if(ControllerFormularios.isFormEnviado(idFormulario)){
            btnFinalizarForm.visibility = View.GONE
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}