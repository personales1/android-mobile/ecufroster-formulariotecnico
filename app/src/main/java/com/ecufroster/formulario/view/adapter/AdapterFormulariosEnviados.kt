package com.ecufroster.formulario.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ecufroster.formulario.R
import com.ecufroster.formulario.`interface`.onFormulariosEnviadas
import com.ecufroster.formulario.`interface`.onFormulariosPorEnviar
import com.ecufroster.formulario.core.AppController
import com.ecufroster.formulario.core.IMAGEN_FIRMA_CLIENTE
import com.ecufroster.formulario.core.IMAGEN_FOTOS_REPORTE
import com.ecufroster.formulario.core.Preferencias
import com.ecufroster.formulario.db.controller.*
import com.ecufroster.formulario.db.model.DBFormularioInformacion
import com.ecufroster.formulario.db.model.DBFormularioInformacionCliente
import kotlinx.android.synthetic.main.adapter_formularios_por_enviar.view.*

class AdapterFormulariosEnviados(private val listado: ArrayList<Int> = ArrayList(), private val listenerInterface: onFormulariosEnviadas)
    : RecyclerView.Adapter<AdapterFormulariosEnviados.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.adapter_formularios_enviados, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = listado[position]
        holder.bind(item, listenerInterface)
    }

    override fun getItemCount() = listado.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(item: Int, listener: onFormulariosEnviadas) = with(itemView){
            val informacionFormularios : DBFormularioInformacion = ControllerInformacionFormulario.getDataByIdForm(item)
            val informacionCliente : DBFormularioInformacionCliente = ControllerInformacionCliente.getDataByIdForm(item)

            tvNumeroFormulario.text = "Formulario $item"
            tvTipoFormulario.text = informacionFormularios.tipoFormulario
            tvFechaFormulario.text = "Fecha y hora del requerimiento el ${informacionFormularios.fechaRequerimiento} a las ${informacionFormularios.horaRequerimiento}"
            tvClienteFormulario.text = "Cliente ${informacionCliente.cliente}"
            tvClienteSapFormulario.text = "Código SAP ${informacionCliente.codigoSap}"

            contenedorRoot.setOnClickListener{ listener.select(item) }
        }
    }
}