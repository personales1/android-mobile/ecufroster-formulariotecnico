package com.ecufroster.formulario.view.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.ecufroster.formulario.R
import com.ecufroster.formulario.`interface`.onPopupListSelectedListener
import com.ecufroster.formulario.core.BaseActivity
import com.ecufroster.formulario.model.PopupListSelected
import com.ecufroster.formulario.view.adapter.PopupListSelectedAdapter
import kotlinx.android.synthetic.main.activity_popup_list_selected.*

class PopupListSelectedActivity : BaseActivity(), onPopupListSelectedListener {
    private var positionSelection: Int = 0
    private var itemSelection: Int = 0
    private var titulo: String = ""
    private var seleccionRapida: Boolean = true
    lateinit var listPopupListSelected: ArrayList<PopupListSelected>

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState, R.layout.activity_popup_list_selected, this)
        configNoHayDatos(R.id.contenedorNoHayData, R.id.tvMensajeNoData)

        window!!.setBackgroundDrawableResource(android.R.color.transparent)

        listPopupListSelected = arrayListOf()

        itemSelection = intent.getIntExtra("index", 0)
        titulo = intent.getStringExtra("titulo") as String
        seleccionRapida = intent.getBooleanExtra("seleccion_rapida", true)
        listPopupListSelected = intent.getParcelableArrayListExtra("lista")!!

        tvTitulo.text = titulo
        if(titulo == ""){
            tvTitulo.visibility = View.GONE
        }

        if(seleccionRapida) {
            btnSeleccionarOpcion.visibility = View.GONE
        } else {
            btnSeleccionarOpcion.visibility = View.VISIBLE
        }

        if(listPopupListSelected.size > 0) {
            /*Inicializando recycler view*/
            rcvListado.apply {
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(this@PopupListSelectedActivity)
                adapter = PopupListSelectedAdapter(listPopupListSelected, itemSelection, this@PopupListSelectedActivity)
            }
        } else {
            showNoData("No hay items disponibles en la lista para seleccionar.")
        }

        btnSeleccionarOpcion.setOnClickListener {
            seleccionarOpcion()
        }
    }

    override fun onSelected(position: Int) {
        positionSelection = position

        if(seleccionRapida) {
            seleccionarOpcion()
        } else {
            rcvListado.adapter = PopupListSelectedAdapter(listPopupListSelected, position, this@PopupListSelectedActivity)
        }
    }

    private fun seleccionarOpcion() {
        val intent = Intent()
        intent.putExtra("indexItem", positionSelection)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}