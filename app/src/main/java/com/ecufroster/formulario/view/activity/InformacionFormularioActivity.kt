package com.ecufroster.formulario.view.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.ecufroster.formulario.R
import com.ecufroster.formulario.`interface`.OnDatePickerListener
import com.ecufroster.formulario.`interface`.OnTimerPickerListener
import com.ecufroster.formulario.core.BaseActivity
import com.ecufroster.formulario.core.TIPO_FORMULARIO_MOVILIZACION_CABINETS
import com.ecufroster.formulario.db.controller.ControllerFormularios
import com.ecufroster.formulario.db.controller.ControllerInformacionFormulario
import com.ecufroster.formulario.db.model.DBFormulario
import com.ecufroster.formulario.db.model.DBFormularioInformacion
import com.ecufroster.formulario.helper.ViewHelper
import kotlinx.android.synthetic.main.activity_informacion_formulario.*
import kotlinx.android.synthetic.main.layout_editext_fecha.*
import kotlinx.android.synthetic.main.layout_editext_hora.*
import java.text.SimpleDateFormat
import java.util.*

class InformacionFormularioActivity : BaseActivity() {
    var idFormulario: Int = 0
    var informacionFormularios = DBFormularioInformacion()
    var formulario = DBFormulario()
    var sdfFecha: SimpleDateFormat? = null
    var sdfHora: SimpleDateFormat? = null
    val calFecha = Calendar.getInstance()
    val today : Date = Date()
    var mensaje: String = ""

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState, R.layout.activity_informacion_formulario, this)
        configToolbar(R.id.toolbar, "Información del formulario")

        if(intent.hasExtra("id_formulario")){
            idFormulario = intent.getIntExtra("id_formulario", 0)
            formulario = ControllerFormularios.getDataByIdForm(idFormulario)
        }else{
            Toast.makeText(this, "No se puede iniciar formulario por falta de información", Toast.LENGTH_LONG).show()
            finish()
        }

        sdfFecha = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        sdfHora = SimpleDateFormat("HH:mm:ss", Locale.US)
        calFecha.time = today

        informacionFormularios = ControllerInformacionFormulario.getDataByIdForm(idFormulario)
        informacionFormularios.tecnicoAsignado = preferencias.getPrefPerfilNombre()
        setData()

        if(ControllerFormularios.isFormEnviado(idFormulario)){
            deshabilitaCampos()
        }

        if(formulario.tipoFormulario == TIPO_FORMULARIO_MOVILIZACION_CABINETS) {
            tvLabelTecnicoAsignado.text = "Conductor asignado"
        }

        tvLabelFecha.text = "Fecha de requerimiento"
        tvLabelHora.text = "Hora de requerimiento"

        btnFecha.setOnClickListener { datePickerDialog() }
        fabGuardar.setOnClickListener { guardarForm() }

        edtFecha.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if(edtFecha.text.toString().trim().isNotEmpty()){
                    edtFecha.error = null
                }
            }
        })

        edtHora.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if(edtHora.text.toString().trim().isNotEmpty()){
                    edtHora.error = null
                }
            }
        })
    }

    fun deshabilitaCampos(){
        edtInfoFormCliente.isEnabled = false
        edtInfoFormTecnicoAsignado.isEnabled = false
        btnFecha.isEnabled = false
        fabGuardar.visibility = View.GONE
    }

    fun setData(){
        edtInfoFormCliente.setText(informacionFormularios.cliente)
        edtInfoFormTecnicoAsignado.setText(informacionFormularios.tecnicoAsignado)
        edtFecha.setText(informacionFormularios.fechaRequerimiento)

        if(informacionFormularios.horaRequerimiento.trim().isEmpty()){
            edtHora.setText(sdfHora!!.format(calFecha.time))
        }else{
            edtHora.setText(informacionFormularios.horaRequerimiento)
        }
    }

    fun datePickerDialog() {
        ViewHelper.hideKeyboardInActivity(this)
        ViewHelper.showCalendarDateBirth(this, calFecha.get(Calendar.YEAR), calFecha.get(Calendar.MONTH), calFecha.get(Calendar.DAY_OF_MONTH)
            , object : OnDatePickerListener {
                override fun onSelected(year: Int, monthOfYear: Int, dayOfMonth: Int) {
                    calFecha.set(year, monthOfYear, dayOfMonth)
                    sdfFecha = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                    edtFecha.setText(sdfFecha!!.format(calFecha.time))
                }
            })
    }

    fun horaPickerDialog(){
        ViewHelper.hideKeyboardInActivity(this)
        ViewHelper.showClockTimerPicker(this, calFecha.get(Calendar.HOUR_OF_DAY), calFecha.get(Calendar.MINUTE), object :
            OnTimerPickerListener {
            override fun onSelected(hourOfDay: Int, minute: Int) {
                calFecha.set(Calendar.HOUR_OF_DAY, hourOfDay)
                calFecha.set(Calendar.MINUTE, minute)
                edtHora.setText(sdfHora!!.format(calFecha.time))
            }
        })
    }

    fun formValido(): Boolean{
        var retorna = true

        if(edtInfoFormCliente.text.toString().trim().isEmpty()){
            edtInfoFormCliente.error = getString(R.string.campo_necesario)
            retorna = false
        }
        else{
            edtInfoFormCliente.error = null
        }

        if(edtFecha.text.toString().trim().isEmpty()){
            edtFecha.error = getString(R.string.campo_necesario)
            retorna = false
        }
        else{
            edtFecha.error = null
        }

        if(edtHora.text.toString().trim().isEmpty()){
            edtHora.error = getString(R.string.campo_necesario)
            retorna = false
        }
        else{
            edtHora.error = null
        }

        if(retorna){
            informacionFormularios.formularioFinalizado = 1
        }
        else{
            informacionFormularios.formularioFinalizado = 0
        }

        return retorna
    }

    fun guardarForm(){
        informacionFormularios.cliente = edtInfoFormCliente.text.toString().trim()
        informacionFormularios.tecnicoAsignado = edtInfoFormTecnicoAsignado.text.toString().trim()
        informacionFormularios.fechaRequerimiento = edtFecha.text.toString().trim()
        informacionFormularios.horaRequerimiento = edtHora.text.toString().trim()
        informacionFormularios.idFormulario = idFormulario

        if(formValido()){
            mensaje = getString(R.string.guardado_exitoso)
            informacionFormularios.formularioFinalizado = 1
        }
        else{
            mensaje = getString(R.string.error_campo_faltante)
            informacionFormularios.formularioFinalizado = 0
        }

        ControllerInformacionFormulario.createForm(informacionFormularios)
        Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show()
        finish()
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        ViewHelper.hideKeyboardInActivity(this@InformacionFormularioActivity)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}