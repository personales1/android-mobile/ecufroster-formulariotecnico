package com.ecufroster.formulario.view.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import com.ecufroster.formulario.R
import com.ecufroster.formulario.`interface`.OnApiResponse
import com.ecufroster.formulario.core.BaseActivity
import com.ecufroster.formulario.helper.UtilidadesHelper
import com.ecufroster.formulario.helper.ViewHelper
import com.ecufroster.formulario.retrofit.RecursoApi
import com.ecufroster.formulario.retrofit.error.ServiceError
import com.ecufroster.formulario.retrofit.model.response.ApiResponsePerfilAuth
import com.ecufroster.formulario.retrofit.service.Api
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity() {

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState, R.layout.activity_login, this)
        configProgressBar(R.id.layoutProgressBar)

        imvLoginVisible.setOnClickListener { ViewHelper.passwordToggleWithImage(edtLoginClave, imvLoginVisible, imvLoginNoVisible, true) }
        imvLoginNoVisible.setOnClickListener { ViewHelper.passwordToggleWithImage(edtLoginClave, imvLoginVisible, imvLoginNoVisible, false) }
        btnLogin.setOnClickListener { login() }
    }

    fun formValido(): Boolean{
        var retorna = true

        if(edtLoginUsuario.text.toString().trim().isEmpty()){
            tilLoginUsuario.error = "Campo obligatorio";
            retorna = false
        }

        if(edtLoginClave.text.toString().trim().isEmpty()){
            tilLoginClave.error = "Campo obligatorio";
            retorna = false
        }

        return retorna
    }

    fun login(){
        ViewHelper.hideKeyboardInActivity(this@LoginActivity)

        if(formValido()){
            showProgressBar()
            val apiService = Api.settingService()
            val objetoApi = apiService.login(
                edtLoginUsuario.text.toString().trim(),
                edtLoginClave.text.toString().trim()
            )

            RecursoApi.consumeApi(objetoApi, object : OnApiResponse<Int> {
                override fun onSuccess(response: Int) {
                    if(response==1){
                        perfil(edtLoginUsuario.text.toString().trim())
                    }else{
                        hideProgressBar()
                        UtilidadesHelper.mensajeAlerta(this@LoginActivity, "Aviso", "Credenciales incorrectas para inicio de sesión").show()
                    }
                }

                override fun onError(error: ServiceError) {
                    hideProgressBar()
                    mostrarMensajeErrorResponseReintentar(this@LoginActivity, error)
                }

                override fun onUpdateApp(error: ServiceError) {
                    hideProgressBar()
                }

                override fun onUnAuthenticated(error: ServiceError) {
                    hideProgressBar()
                    mostrarMensajeErrorResponse(this@LoginActivity, error)
                }

                override fun onUnknownError() {
                    hideProgressBar()
                    mostrarMensajeErrorServidor(this@LoginActivity)
                }

                override fun onFailure(throwable: Throwable) {
                    hideProgressBar()
                    mostrarMensajeErrorThrowable(this@LoginActivity, throwable)
                }

                override fun onErrorException(error: Exception) {
                    hideProgressBar()
                    mostrarMensajeErrorException(this@LoginActivity, error)
                }

                override fun onWithoutInternet() {
                    hideProgressBar()
                    mostrarMensajeErrorInternet(this@LoginActivity)
                }
            })
        }
    }

    fun perfil(identificacion: String){
        showProgressBar()
        val apiService = Api.settingService()
        val objetoApi = apiService.perfil(
            identificacion.trim()
        )

        RecursoApi.consumeApi(objetoApi, object : OnApiResponse<ApiResponsePerfilAuth> {
            override fun onSuccess(response: ApiResponsePerfilAuth) {
                hideProgressBar()
                preferencias.setPrefPerfilId(UtilidadesHelper.isValidNull(response.id))
                preferencias.setPrefPerfilNombre(UtilidadesHelper.isValidNull(response.nombre))
                preferencias.setPrefPerfilIdentificacion(UtilidadesHelper.isValidNull(response.identificacion))
                preferencias.setPrefPerfilTelefono(UtilidadesHelper.isValidNull(response.telefono))
                preferencias.setPrefPerfilCorreo(UtilidadesHelper.isValidNull(response.correo))
                preferencias.setPrefSessionUser("S")

                startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                finish()
            }

            override fun onError(error: ServiceError) {
                hideProgressBar()
                mostrarMensajeErrorResponseReintentar(this@LoginActivity, error)
            }

            override fun onUpdateApp(error: ServiceError) {
                hideProgressBar()
            }

            override fun onUnAuthenticated(error: ServiceError) {
                hideProgressBar()
                mostrarMensajeErrorResponse(this@LoginActivity, error)
            }

            override fun onUnknownError() {
                hideProgressBar()
                mostrarMensajeErrorServidor(this@LoginActivity)
            }

            override fun onFailure(throwable: Throwable) {
                hideProgressBar()
                mostrarMensajeErrorThrowable(this@LoginActivity, throwable)
            }

            override fun onErrorException(error: Exception) {
                hideProgressBar()
                mostrarMensajeErrorException(this@LoginActivity, error)
            }

            override fun onWithoutInternet() {
                hideProgressBar()
                mostrarMensajeErrorInternet(this@LoginActivity)
            }
        })
    }
}