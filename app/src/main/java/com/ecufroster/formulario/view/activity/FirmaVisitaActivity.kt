package com.ecufroster.formulario.view.activity

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import com.ecufroster.formulario.R
import com.ecufroster.formulario.core.BaseActivity
import com.ecufroster.formulario.db.controller.ControllerInformacionCliente
import com.ecufroster.formulario.db.model.DBFormularioInformacionCliente
import com.ecufroster.formulario.helper.UtilidadesHelper
import kotlinx.android.synthetic.main.activity_firma_visita.*

class FirmaVisitaActivity : BaseActivity() {
    var idFormulario: Int = 0
    var informacionCliente = DBFormularioInformacionCliente()

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState, R.layout.activity_firma_visita, this)
        configToolbar(R.id.toolbar, "Firmas")

        if(intent.hasExtra("id_formulario")){
            idFormulario = intent.getIntExtra("id_formulario", 0)
        }else{
            Toast.makeText(this, "No se puede iniciar formulario por falta de información", Toast.LENGTH_LONG).show()
            finish()
        }

        btnFirmaTecnico.setOnClickListener {
            val intent = Intent(this, FirmaTecnicoActivity::class.java)
            intent.putExtra("id_formulario", idFormulario)
            startActivity(intent)
        }

        btnFirmaCliente.setOnClickListener {
            informacionCliente = ControllerInformacionCliente.getDataByIdForm(idFormulario)

            if(informacionCliente.codigoSap.trim().isNotEmpty()){
                val intent = Intent(this, FirmaClienteActivity::class.java)
                intent.putExtra("id_formulario", idFormulario)
                intent.putExtra("codigo_sap_cliente", informacionCliente.codigoSap.trim())
                startActivity(intent)
            }
            else{
                UtilidadesHelper.mensajeAlerta(this, "Advertencia", "Para que el cliente realice su firma, es necesario que primero llenes la sección 'Información del cliente/usuario' del formulario.").show()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}