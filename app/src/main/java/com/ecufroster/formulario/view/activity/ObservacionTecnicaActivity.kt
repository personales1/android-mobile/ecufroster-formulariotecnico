package com.ecufroster.formulario.view.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.ecufroster.formulario.R
import com.ecufroster.formulario.core.BaseActivity
import com.ecufroster.formulario.core.TIPO_FORMULARIO_EMERGENCIA_EN_CIUDAD
import com.ecufroster.formulario.core.TIPO_FORMULARIO_MOVILIZACION_CABINETS
import com.ecufroster.formulario.db.controller.ControllerFormularios
import com.ecufroster.formulario.db.controller.ControllerObservacionTecnica
import com.ecufroster.formulario.db.model.DBFormulario
import com.ecufroster.formulario.db.model.DBFormularioObservacionTecnica
import com.ecufroster.formulario.helper.ViewHelper
import com.ecufroster.formulario.model.PopupListSelected
import kotlinx.android.synthetic.main.activity_observacion_tecnica.*

class ObservacionTecnicaActivity : BaseActivity() {
    var idFormulario: Int = 0
    var formulario = DBFormulario()
    var informacionObservacionTecnica = DBFormularioObservacionTecnica()
    var listPopupListSelected = ArrayList<PopupListSelected>()
    var descripcionStatus : String = ""
    var descripcionTipoMovimiento : String = ""
    var positionStatusSeleccionado : Int = -1
    var positionTipoMovimientoSeleccionado : Int = -1
    val LISTA_STATUS : Int = 1
    val LISTA_TIPO_MOVIMIENTO : Int = 2
    var mensaje: String = ""

    var listStatus = arrayListOf("Operativo", "Visita fallida", "Orden de retiro")
    var listTipoMovimiento = arrayListOf(
        "Entrega cliente nuevo",
        "Entrega por cambio",
        "Retiro por cambio",
        "Retiro definitivo o captura",
        "Entrega por Emergencia",
        "Retiro por Emergencia",
        "Eventos",
        "Otros",
    )

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState, R.layout.activity_observacion_tecnica, this)
        configToolbar(R.id.toolbar, "Observación técnica")

        if(intent.hasExtra("id_formulario")){
            idFormulario = intent.getIntExtra("id_formulario", 0)
            formulario = ControllerFormularios.getDataByIdForm(idFormulario)
        }else{
            Toast.makeText(this, "No se puede iniciar formulario por falta de información", Toast.LENGTH_LONG).show()
            finish()
        }

        if(formulario.tipoFormulario == TIPO_FORMULARIO_MOVILIZACION_CABINETS) {
            listStatus = arrayListOf("Movimiento operativo", "Visita fallida")
        } else {
            labelObsTecTipoMovimiento.visibility = View.GONE
            edtObsTecTipoMovimiento.visibility = View.GONE
        }

        informacionObservacionTecnica = ControllerObservacionTecnica.getDataByIdForm(idFormulario)
        setData()

        if(ControllerFormularios.isFormEnviado(idFormulario)){
            deshabilitaCampos()
        }

        edtObsTecTipoStatus.setOnClickListener { selectedStatus() }
        edtObsTecTipoMovimiento.setOnClickListener { selectedTipoMovimiento() }
        fabObservacionTecnicaGuardar.setOnClickListener { guardarForm() }

        edtObsTecTipoStatus.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if(edtObsTecTipoStatus.text.toString().trim().isNotEmpty()){
                    edtObsTecTipoStatus.error = null
                }
            }
        })

        edtObsTecObservacionTecnica.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if(edtObsTecObservacionTecnica.text.toString().trim().isNotEmpty()){
                    edtObsTecObservacionTecnica.error = null
                }
            }
        })
    }

    fun deshabilitaCampos(){
        edtObsTecTipoStatus.isEnabled = false
        edtObsTecTipoMovimiento.isEnabled = false
        edtObsTecObservacionTecnica.isEnabled = false
        fabObservacionTecnicaGuardar.visibility = View.GONE
    }

    fun setData(){
        edtObsTecObservacionTecnica.setText(informacionObservacionTecnica.observacion)

        if(informacionObservacionTecnica.idFormulario != -1){
            /**
             * CAMPO TIPO DE STATUS
             */
            val indexAux = listStatus.indexOfFirst { it.toLowerCase() == informacionObservacionTecnica.tipoStatus.toLowerCase() }
            positionStatusSeleccionado = indexAux

            if(indexAux >= 0){
                edtObsTecTipoStatus.setText(listStatus[positionStatusSeleccionado])
            }else{
                edtObsTecTipoStatus.setText(informacionObservacionTecnica.tipoStatus)
            }

            /**
             * CAMPO TIPO DE MOVIMIENTO
             */
            val indexAuxTM = listTipoMovimiento.indexOfFirst { it.toLowerCase() == informacionObservacionTecnica.tipoMovimiento.toLowerCase() }
            positionTipoMovimientoSeleccionado = indexAuxTM

            if(indexAuxTM >= 0){
                edtObsTecTipoMovimiento.setText(listTipoMovimiento[positionTipoMovimientoSeleccionado])
            }else{
                edtObsTecTipoMovimiento.setText(informacionObservacionTecnica.tipoMovimiento)
            }
        }
        else{
            edtObsTecTipoStatus.setText(informacionObservacionTecnica.tipoStatus)
            edtObsTecTipoMovimiento.setText(informacionObservacionTecnica.tipoMovimiento)
        }
    }

    fun selectedStatus(){
        ViewHelper.hideKeyboardInActivity(this)
        listPopupListSelected.clear()

        for(item in listStatus){
            descripcionStatus = item
            listPopupListSelected.add(PopupListSelected(descripcion = descripcionStatus))
        }

        val intent = Intent(this, PopupListSelectedActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        intent.putExtra("index", positionStatusSeleccionado)
        intent.putExtra("titulo", "Status del cabinet")
        intent.putParcelableArrayListExtra("lista", listPopupListSelected)
        startActivityForResult(intent, LISTA_STATUS)
    }

    fun selectedTipoMovimiento(){
        ViewHelper.hideKeyboardInActivity(this)
        listPopupListSelected.clear()

        for(item in listTipoMovimiento){
            descripcionTipoMovimiento = item
            listPopupListSelected.add(PopupListSelected(descripcion = descripcionTipoMovimiento))
        }

        val intent = Intent(this, PopupListSelectedActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        intent.putExtra("index", positionTipoMovimientoSeleccionado)
        intent.putExtra("titulo", "Tipo de movimiento")
        intent.putParcelableArrayListExtra("lista", listPopupListSelected)
        startActivityForResult(intent, LISTA_TIPO_MOVIMIENTO)
    }

    fun formValido(): Boolean{
        var retorna = true

        if(edtObsTecTipoStatus.text.toString().isEmpty()){
            edtObsTecTipoStatus.error = getString(R.string.campo_necesario)
            retorna = false
        }
        else{
            edtObsTecTipoStatus.error = null
        }

        if(edtObsTecObservacionTecnica.text.toString().isEmpty()){
            edtObsTecObservacionTecnica.error = getString(R.string.campo_necesario)
            retorna = false
        }
        else{
            edtObsTecObservacionTecnica.error = null
        }

        return retorna
    }

    fun guardarForm(){
        informacionObservacionTecnica.tipoStatus = edtObsTecTipoStatus.text.toString().trim()
        informacionObservacionTecnica.tipoMovimiento = edtObsTecTipoMovimiento.text.toString().trim()
        informacionObservacionTecnica.observacion = edtObsTecObservacionTecnica.text.toString().trim()
        informacionObservacionTecnica.idFormulario = idFormulario

        if(formValido()){
            mensaje = getString(R.string.guardado_exitoso)
            informacionObservacionTecnica.formularioFinalizado = 1
        }
        else{
            mensaje = getString(R.string.error_campo_faltante)
            informacionObservacionTecnica.formularioFinalizado = 0
        }

        ControllerObservacionTecnica.createForm(informacionObservacionTecnica)
        Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show()
        finish()
    }

    @SuppressLint("SetTextI18n")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            val indexPosition = data!!.getIntExtra("indexItem", 0)

            if (requestCode == LISTA_STATUS) {
                descripcionStatus = listStatus[indexPosition]
                edtObsTecTipoStatus.setText(descripcionStatus)
                positionStatusSeleccionado = indexPosition
            }

            if (requestCode == LISTA_TIPO_MOVIMIENTO) {
                descripcionTipoMovimiento = listTipoMovimiento[indexPosition]
                edtObsTecTipoMovimiento.setText(descripcionTipoMovimiento)
                positionTipoMovimientoSeleccionado = indexPosition
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}