package com.ecufroster.formulario.view.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.ecufroster.formulario.R
import com.ecufroster.formulario.core.BaseActivity
import com.ecufroster.formulario.core.TIPO_FORMULARIO_MEDICION_CONSUMO_KWH_EN_PDV
import com.ecufroster.formulario.db.controller.ControllerFormularios
import com.ecufroster.formulario.db.controller.ControllerInformacionCabinetTecnico
import com.ecufroster.formulario.db.model.DBFormulario
import com.ecufroster.formulario.db.model.DBFormularioInformacionCabinetTecnica
import com.ecufroster.formulario.helper.ViewHelper
import com.ecufroster.formulario.model.PopupListSelected
import kotlinx.android.synthetic.main.activity_informacion_cabinet_tecnica.*
import kotlinx.android.synthetic.main.activity_informacion_cliente.*
import kotlinx.android.synthetic.main.activity_observacion_tecnica.*

class InformacionCabinetTecnicaActivity : BaseActivity() {
    var idFormulario: Int = 0
    var informacionCabinetTecnica = DBFormularioInformacionCabinetTecnica()
    var formulario = DBFormulario()
    val listTipoGas = arrayListOf("R-134A", "R-404A", "R-290", "R-600A")
    val listClasificacionTipoGas = arrayListOf("HFC - HIDROGENOFLUORCARBONO", "HFC - HIDROGENOFLUORCARBONO", "HC - PROPANO", "HC - ISOBUTANO")
    var listBeneficios : ArrayList<String>? = null
    var listPopupListSelected = ArrayList<PopupListSelected>()
    var descripcionTipoGas : String = ""
    var positionTipoGasSeleccionado : Int = -1
    val LISTA_TIPO_GAS : Int = 1
    var mensaje: String = ""

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState, R.layout.activity_informacion_cabinet_tecnica, this)
        configToolbar(R.id.toolbar, "Información técnica del cabinet")

        if(intent.hasExtra("id_formulario")){
            idFormulario = intent.getIntExtra("id_formulario", 0)
            formulario = ControllerFormularios.getDataByIdForm(idFormulario)
        }else{
            Toast.makeText(this, "No se puede iniciar formulario por falta de información", Toast.LENGTH_LONG).show()
            finish()
        }

        listBeneficios = arrayListOf(getString(R.string.beneficios_1), getString(R.string.beneficios_1), getString(R.string.beneficios_2), getString(R.string.beneficios_2))
        informacionCabinetTecnica = ControllerInformacionCabinetTecnico.getDataByIdForm(idFormulario)

        setData()

        if(ControllerFormularios.isFormEnviado(idFormulario)){
            deshabilitaCampos()
        }

        edtInfoCabinetTecnicoTipoGas.setOnClickListener { selectedTipoGas() }
        fabInfoCabinetTecnicoGuardar.setOnClickListener { guardarForm() }

        edtInfoCabinetTecnicoTipoGas.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if(edtInfoCabinetTecnicoTipoGas.text.toString().trim().isNotEmpty()){
                    edtInfoCabinetTecnicoTipoGas.error = null
                }
            }
        })

        edtInfoCabinetTecnicoCargaGas.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if(edtInfoCabinetTecnicoCargaGas.text.toString().trim().isNotEmpty()){
                    edtInfoCabinetTecnicoCargaGas.error = null
                }
            }
        })

        edtInfoCabinetTecnicoVoltaje.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if(edtInfoCabinetTecnicoVoltaje.text.toString().trim().isNotEmpty()){
                    edtInfoCabinetTecnicoVoltaje.error = null
                }
            }
        })

        edtInfoCabinetTecnicoConsumo.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if(edtInfoCabinetTecnicoConsumo.text.toString().trim().isNotEmpty()){
                    edtInfoCabinetTecnicoConsumo.error = null
                }
            }
        })

        edtInfoCabinetTecnicoKwh24.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if(edtInfoCabinetTecnicoKwh24.text.toString().trim().isNotEmpty()){
                    edtInfoCabinetTecnicoKwh24.error = null

                    val valor = edtInfoCabinetTecnicoKwh24.text.toString().toFloat() * 30
                    edtInfoCabinetTecnicoKwhMes.setText(valor.toString())
                }
                else{
                    edtInfoCabinetTecnicoKwhMes.setText("")
                }
            }
        })
    }

    fun deshabilitaCampos(){
        edtInfoCabinetTecnicoTipoGas.isEnabled = false
        edtInfoCabinetTecnicoTipoRefrigerante.isEnabled = false
        edtInfoCabinetTecnicoCargaGas.isEnabled = false
        edtInfoCabinetTecnicoVoltaje.isEnabled = false
        edtInfoCabinetTecnicoConsumo.isEnabled = false
        edtInfoCabinetTecnicoKwh24.isEnabled = false
        edtInfoCabinetTecnicoKwhMes.isEnabled = false
        edtInfoCabinetTecnicoBeneficios.isEnabled = false
        fabInfoCabinetTecnicoGuardar.visibility = View.GONE
    }

    fun setData(){
        edtInfoCabinetTecnicoTipoRefrigerante.setText(informacionCabinetTecnica.familiaTipoGas)
        edtInfoCabinetTecnicoCargaGas.setText(informacionCabinetTecnica.cargaGas)
        edtInfoCabinetTecnicoVoltaje.setText(informacionCabinetTecnica.voltaje)
        edtInfoCabinetTecnicoConsumo.setText(informacionCabinetTecnica.consumo)
        edtInfoCabinetTecnicoKwh24.setText(informacionCabinetTecnica.kwh24Horas)
        edtInfoCabinetTecnicoKwhMes.setText(informacionCabinetTecnica.kwhMesAproximado)
        edtInfoCabinetTecnicoBeneficios.setText(informacionCabinetTecnica.beneficioMedioAmbiente)

        if(informacionCabinetTecnica.idFormulario != -1){
            val indexAux = listTipoGas.indexOfFirst { it.toLowerCase() == informacionCabinetTecnica.tipoGas.toLowerCase() }
            positionTipoGasSeleccionado = indexAux

            if(indexAux >= 0){
                edtInfoCabinetTecnicoTipoGas.setText(listTipoGas[positionTipoGasSeleccionado])
            }else{
                edtInfoCabinetTecnicoTipoGas.setText(informacionCabinetTecnica.tipoGas)
            }
        }
        else{
            edtInfoCabinetTecnicoTipoGas.setText(informacionCabinetTecnica.tipoGas)
        }

        if(formulario.tipoFormulario == TIPO_FORMULARIO_MEDICION_CONSUMO_KWH_EN_PDV){
            tvInfoCabinetTecnicoKwh24.visibility = View.VISIBLE
            edtInfoCabinetTecnicoKwh24.visibility = View.VISIBLE

            tvInfoCabinetTecnicoKwhMes.visibility = View.VISIBLE
            edtInfoCabinetTecnicoKwhMes.visibility = View.VISIBLE

            tvInfoCabinetTecnicoBeneficios.visibility = View.VISIBLE
            edtInfoCabinetTecnicoBeneficios.visibility = View.VISIBLE
        }
        else{
            tvInfoCabinetTecnicoKwh24.visibility = View.GONE
            edtInfoCabinetTecnicoKwh24.visibility = View.GONE

            tvInfoCabinetTecnicoKwhMes.visibility = View.GONE
            edtInfoCabinetTecnicoKwhMes.visibility = View.GONE

            tvInfoCabinetTecnicoBeneficios.visibility = View.GONE
            edtInfoCabinetTecnicoBeneficios.visibility = View.GONE
        }
    }

    fun selectedTipoGas(){
        ViewHelper.hideKeyboardInActivity(this)
        listPopupListSelected.clear()

        var i = 0
        for(item in listTipoGas){
            descripcionTipoGas = item + " / " + listClasificacionTipoGas[i]
            listPopupListSelected.add(PopupListSelected(descripcion = descripcionTipoGas))
            i += 1
        }

        val intent = Intent(this, PopupListSelectedActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        intent.putExtra("index", positionTipoGasSeleccionado)
        intent.putExtra("titulo", "Tipo de gas / Tipo de refrigerante")
        intent.putParcelableArrayListExtra("lista", listPopupListSelected)
        startActivityForResult(intent, LISTA_TIPO_GAS)
    }

    fun formValido(): Boolean{
        var retorna = true

        if(edtInfoCabinetTecnicoTipoGas.text.toString().isEmpty()){
            edtInfoCabinetTecnicoTipoGas.error = getString(R.string.campo_necesario)
            retorna = false
        }
        else{
            edtInfoCabinetTecnicoTipoGas.error = null
        }

        if(edtInfoCabinetTecnicoCargaGas.text.toString().isEmpty()){
            edtInfoCabinetTecnicoCargaGas.error = getString(R.string.campo_necesario)
            retorna = false
        }
        else{
            edtInfoCabinetTecnicoCargaGas.error = null
        }

        if(edtInfoCabinetTecnicoVoltaje.text.toString().isEmpty()){
            edtInfoCabinetTecnicoVoltaje.error = getString(R.string.campo_necesario)
            retorna = false
        }
        else{
            edtInfoCabinetTecnicoVoltaje.error = null
        }

        if(edtInfoCabinetTecnicoConsumo.text.toString().isEmpty()){
            edtInfoCabinetTecnicoConsumo.error = getString(R.string.campo_necesario)
            retorna = false
        }
        else{
            edtInfoCabinetTecnicoConsumo.error = null
        }

        if(edtInfoCabinetTecnicoKwh24.visibility == View.VISIBLE && edtInfoCabinetTecnicoKwh24.text.toString().isEmpty()){
            edtInfoCabinetTecnicoKwh24.error = getString(R.string.campo_necesario)
            retorna = false
        }
        else{
            edtInfoCabinetTecnicoKwh24.error = null
        }

        return retorna
    }

    fun guardarForm(){
        informacionCabinetTecnica.tipoGas = edtInfoCabinetTecnicoTipoGas.text.toString().trim()
        informacionCabinetTecnica.familiaTipoGas = edtInfoCabinetTecnicoTipoRefrigerante.text.toString().trim()
        informacionCabinetTecnica.cargaGas = edtInfoCabinetTecnicoCargaGas.text.toString().trim()
        informacionCabinetTecnica.voltaje = edtInfoCabinetTecnicoVoltaje.text.toString().trim()
        informacionCabinetTecnica.consumo = edtInfoCabinetTecnicoConsumo.text.toString().trim()
        informacionCabinetTecnica.kwh24Horas = edtInfoCabinetTecnicoKwh24.text.toString().trim()
        informacionCabinetTecnica.kwhMesAproximado = edtInfoCabinetTecnicoKwhMes.text.toString().trim()
        informacionCabinetTecnica.beneficioMedioAmbiente = edtInfoCabinetTecnicoBeneficios.text.toString().trim()
        informacionCabinetTecnica.idFormulario = idFormulario

        if(formValido()){
            mensaje = getString(R.string.guardado_exitoso)
            informacionCabinetTecnica.formularioFinalizado = 1
        }
        else{
            mensaje = getString(R.string.error_campo_faltante)
            informacionCabinetTecnica.formularioFinalizado = 0
        }

        ControllerInformacionCabinetTecnico.createForm(informacionCabinetTecnica)
        Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show()
        finish()
    }

    @SuppressLint("SetTextI18n")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            val indexPosition = data!!.getIntExtra("indexItem", 0)

            if (requestCode == LISTA_TIPO_GAS) {
                positionTipoGasSeleccionado = indexPosition

                informacionCabinetTecnica.tipoGas = listTipoGas[positionTipoGasSeleccionado]
                informacionCabinetTecnica.familiaTipoGas = listClasificacionTipoGas[positionTipoGasSeleccionado]
                informacionCabinetTecnica.beneficioMedioAmbiente = listBeneficios!![positionTipoGasSeleccionado]

                setData()
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}