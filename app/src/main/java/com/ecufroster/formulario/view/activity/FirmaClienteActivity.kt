package com.ecufroster.formulario.view.activity

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.graphics.*
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Environment
import android.util.AttributeSet
import android.util.Log
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.ecufroster.formulario.R
import com.ecufroster.formulario.core.*
import com.ecufroster.formulario.db.controller.ControllerFormularios
import com.ecufroster.formulario.db.controller.ControllerImagenFormulario
import com.ecufroster.formulario.db.model.DBFormularioImagen
import com.ecufroster.formulario.helper.PermisosHelper
import com.ecufroster.formulario.helper.UtilidadesHelper
import kotlinx.android.synthetic.main.activity_firma_cliente.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream

private const val STROKE_WIDTH = 5f
private const val HALF_STROKE_WIDTH = STROKE_WIDTH / 2

class FirmaClienteActivity : BaseActivity() {
    var idFormulario: Int = 0
    var codigoSapCliente: String = ""

    lateinit var directory: File
    lateinit var dirPath:String
    var mSignature: Signature? = null
    var mBitmap: Bitmap? = null
    var mFile: File? = null

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState, R.layout.activity_firma_cliente, this)
        configToolbar(R.id.toolbar, "Firma del cliente")

        if(intent.hasExtra("id_formulario")){
            idFormulario = intent.getIntExtra("id_formulario", 0)
            codigoSapCliente = intent.getStringExtra("codigo_sap_cliente")!!
        }else{
            Toast.makeText(this, "No se puede iniciar formulario por falta de información", Toast.LENGTH_LONG).show()
            finish()
        }

        directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
        dirPath =  directory.absolutePath + File.separator + DIRECTORY_FOLDER + File.separator + DIRECTORY_FOLDER_CLIENTE

        val dirFile = File(dirPath)
        if (!dirFile.exists()) {
            dirFile.mkdirs()
        }

        mSignature = Signature(this, null)
        val rutaFirma = ControllerImagenFormulario.getFirmaByIdForm(idFormulario, IMAGEN_FIRMA_CLIENTE).rutaImagen

        if(rutaFirma.isNotEmpty()){
            mFile = File(rutaFirma)
            mSignature!!.background = Drawable.createFromPath(rutaFirma)
        }
        else{
            mSignature!!.setBackgroundColor(Color.WHITE)
            val nombreImagen = "$IMAGEN_FIRMA_CLIENTE-$idFormulario-$codigoSapCliente-${UtilidadesHelper.dateTimeCurrentFormatStandar()}.png"
            mFile = File(dirPath + File.separator + nombreImagen)
        }

        contenedorFirmaCliente!!.addView(mSignature, ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT)

        if(ControllerFormularios.isFormEnviado(idFormulario)){
            deshabilitaCampos()
        }

        fabFirmaClienteLimpiar.setOnClickListener {
            mSignature!!.clear()
            mSignature!!.setBackgroundColor(Color.WHITE)
        }

        fabFirmaClienteGuardar.setOnClickListener { guardarFirmaEnLocal() }
    }

    fun deshabilitaCampos(){
        floating_menu.visibility = View.GONE
    }

    fun guardarFirmaEnLocal(){
        if(PermisosHelper.solicitarPermiso(this, Manifest.permission.WRITE_EXTERNAL_STORAGE, ConstantPermisos.Request.REQUEST_CODE_WRITE_EXTERNAL_STORAGE)){
            contenedorFirmaCliente!!.isDrawingCacheEnabled = true
            mSignature!!.save(contenedorFirmaCliente)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            ConstantPermisos.Request.REQUEST_CODE_WRITE_EXTERNAL_STORAGE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    guardarFirmaEnLocal()
                }
                else {
                    UtilidadesHelper.mensajePermisos(this)!!
                        .setPositiveButton(getString(R.string.dialogos_aceptar), DialogInterface.OnClickListener { _, _ ->
                            PermisosHelper.validarSolicitudPermiso(
                                this,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                getString(R.string.permission_description_rejected, ConstantPermisos.Name.ALMACENAMIENTO, ConstantPermisos.Reason.ALMACENAMIENTO_GALERIA_GUARDAR_FIRMA),
                                ConstantPermisos.Request.REQUEST_CODE_WRITE_EXTERNAL_STORAGE
                            )
                        })
                        .show()
                }
                return
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    inner class Signature(mContext: Context, attrs: AttributeSet?) : View(mContext, attrs) {
        private val paint = Paint()
        private val path = Path()
        private var lastTouchX = 0f
        private var lastTouchY = 0f
        private val dirtyRect = RectF()

        fun save(v: View?) {
            Log.v("log_tag", "Width: " + v!!.width)
            Log.v("log_tag", "Height: " + v.height)
            if (mBitmap == null) {
                mBitmap = Bitmap.createBitmap(v.width, v.height, Bitmap.Config.RGB_565)
            }
            val canvas = Canvas(mBitmap!!)
            try { // In case you want to delete the file
                v.draw(canvas)
                val bytest = ByteArrayOutputStream()
                mBitmap!!.compress(Bitmap.CompressFormat.PNG, 100, bytest)
                val bfoto = bytest.toByteArray()
                val mFileOutStream = FileOutputStream(mFile!!)
                mFileOutStream.write(bfoto)
                mFileOutStream.flush()
                mFileOutStream.close()

                ControllerImagenFormulario.createFormFirma(
                    DBFormularioImagen(
                        rutaImagen = mFile!!.absolutePath,
                        tipoImagen = TIPO_IMAGEN_FIRMA,
                        pertenece = IMAGEN_FIRMA_CLIENTE,
                        idFormulario = idFormulario
                    )
                )
                Toast.makeText(this@FirmaClienteActivity, "Firma guardada con éxito", Toast.LENGTH_SHORT).show()
                finish()
            } catch (e: Exception) {
                Log.e("try", e.message!!)
                Toast.makeText(this@FirmaClienteActivity, "erro try " + e.message, Toast.LENGTH_SHORT).show()
            }
        }

        fun clear() {
            path.reset()
            invalidate()
        }

        override fun onDraw(canvas: Canvas) {
            canvas.drawPath(path, paint)
        }

        override fun onTouchEvent(event: MotionEvent): Boolean {
            val eventX = event.x
            val eventY = event.y
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    path.moveTo(eventX, eventY)
                    lastTouchX = eventX
                    lastTouchY = eventY
                    return true
                }
                MotionEvent.ACTION_MOVE, MotionEvent.ACTION_UP -> {
                    resetDirtyRect(eventX, eventY)
                    val historySize = event.historySize
                    var i = 0
                    while (i < historySize) {
                        val historicalX = event.getHistoricalX(i)
                        val historicalY = event.getHistoricalY(i)
                        expandDirtyRect(historicalX, historicalY)
                        path.lineTo(historicalX, historicalY)
                        i++
                    }
                    path.lineTo(eventX, eventY)
                }
                else -> return false
            }
            invalidate((dirtyRect.left - HALF_STROKE_WIDTH).toInt(), (dirtyRect.top - HALF_STROKE_WIDTH).toInt(), (dirtyRect.right + HALF_STROKE_WIDTH).toInt(), (dirtyRect.bottom + HALF_STROKE_WIDTH).toInt())
            lastTouchX = eventX
            lastTouchY = eventY
            return true
        }

        private fun expandDirtyRect(historicalX: Float, historicalY: Float) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX
            }
            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY
            }
        }

        private fun resetDirtyRect(eventX: Float, eventY: Float) {
            dirtyRect.left = Math.min(lastTouchX, eventX)
            dirtyRect.right = Math.max(lastTouchX, eventX)
            dirtyRect.top = Math.min(lastTouchY, eventY)
            dirtyRect.bottom = Math.max(lastTouchY, eventY)
        }

        init {
            paint.isAntiAlias = true
            paint.color = Color.BLACK
            paint.style = Paint.Style.STROKE
            paint.strokeJoin = Paint.Join.ROUND
            paint.strokeWidth = STROKE_WIDTH
        }
    }
}