package com.ecufroster.formulario.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.ecufroster.formulario.R
import com.ecufroster.formulario.`interface`.onFormulariosPorEnviar
import com.ecufroster.formulario.core.AppController
import com.ecufroster.formulario.core.IMAGEN_FIRMA_CLIENTE
import com.ecufroster.formulario.core.IMAGEN_FOTOS_REPORTE
import com.ecufroster.formulario.core.Preferencias
import com.ecufroster.formulario.db.controller.*
import com.ecufroster.formulario.db.model.DBFormularioInformacion
import com.ecufroster.formulario.db.model.DBFormularioInformacionCliente
import kotlinx.android.synthetic.main.adapter_formularios_por_enviar.view.*

class AdapterFormulariosPorEnviar (private val listado: ArrayList<Int> = ArrayList(), private val listenerInterface: onFormulariosPorEnviar)
    : RecyclerView.Adapter<AdapterFormulariosPorEnviar.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.adapter_formularios_por_enviar, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = listado[position]
        holder.bind(item, listenerInterface)
    }

    override fun getItemCount() = listado.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(item: Int, listener: onFormulariosPorEnviar) = with(itemView){

            val preferencias = Preferencias(AppController.mInstance.applicationContext)
            val informacionFormularios : DBFormularioInformacion = ControllerInformacionFormulario.getDataByIdForm(item)
            val informacionCliente : DBFormularioInformacionCliente = ControllerInformacionCliente.getDataByIdForm(item)

            tvNumeroFormulario.text = "Formulario $item"
            tvTipoFormulario.text = informacionFormularios.tipoFormulario
            tvFechaFormulario.text = "Fecha y hora del requerimiento el ${informacionFormularios.fechaRequerimiento} a las ${informacionFormularios.horaRequerimiento}"
            tvClienteFormulario.text = "Cliente ${informacionCliente.cliente}"
            tvClienteSapFormulario.text = "Código SAP ${informacionCliente.codigoSap}"

            if(
                ControllerInformacionFormulario.formIsFinalizado(item) &&
                ControllerInformacionCliente.formIsFinalizado(item) &&
                ControllerInformacionCabinet.formIsFinalizado(item) &&
                ControllerInformacionCabinetTecnico.formIsFinalizado(item) &&
                ControllerObservacionTecnica.formIsFinalizado(item) &&
                ControllerImagenFormulario.getFotosByIdForm(item, IMAGEN_FOTOS_REPORTE).size > 0 &&
                (preferencias.getPrefFirmaUsuarioApp().trim().isNotEmpty() && ControllerImagenFormulario.getFirmaByIdForm(item, IMAGEN_FIRMA_CLIENTE).rutaImagen.trim().isNotEmpty())
            ){
                imvFormularioCompleto.visibility = View.VISIBLE
                imvFormularioIncompleto.visibility = View.GONE
            }
            else{
                imvFormularioCompleto.visibility = View.GONE
                imvFormularioIncompleto.visibility = View.VISIBLE
            }

            if(ControllerFormularios.isFormFinalizado(item)){
                tvEstadoFormulario.text = "Formulario finalizado, listo para envío."
                tvEstadoFormulario.setTextColor(resources.getColor(R.color.colorVerde))
            }
            else{
                tvEstadoFormulario.text = "Formulario sin finalizar, no podrá ser enviado al servidor."
                tvEstadoFormulario.setTextColor(resources.getColor(R.color.colorRojo))
            }

            contenedorRoot.setOnClickListener{ listener.select(item) }
        }
    }
}