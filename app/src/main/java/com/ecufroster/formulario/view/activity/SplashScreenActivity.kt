package com.ecufroster.formulario.view.activity

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ecufroster.formulario.R
import com.ecufroster.formulario.core.BaseActivity
import com.ecufroster.formulario.helper.UtilidadesHelper.Companion.llamarPantallaPrincipal
import java.util.*

class SplashScreenActivity : BaseActivity() {
    lateinit var timer: Timer
    lateinit var timerTask: TimerTask

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState, R.layout.activity_splash_screen, this)
        continuar()
    }

    private fun continuar(){
        timer = Timer()
        initializeTimerTask()
        timer.schedule(timerTask, 3000)
    }

    private fun initializeTimerTask() {
        timerTask = object : TimerTask() {
            override fun run() {
                llamarPantallaPrincipal()
                finish()
            }
        }
    }
}