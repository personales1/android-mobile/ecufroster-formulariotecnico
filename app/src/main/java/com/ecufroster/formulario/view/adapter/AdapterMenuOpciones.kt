package com.ecufroster.formulario.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ecufroster.formulario.R
import com.ecufroster.formulario.`interface`.onMenuOpcionesListener
import com.ecufroster.formulario.model.MenuOpciones
import kotlinx.android.synthetic.main.rcv_menu_opciones.view.*

class AdapterMenuOpciones : RecyclerView.Adapter<AdapterMenuOpciones.ViewHolder>(){
    var listMenu : List<MenuOpciones> = ArrayList()
    lateinit var listener : onMenuOpcionesListener

    fun MenuOpcionesAdapter (listadoMenuOpciones: List<MenuOpciones>, listenerOn: onMenuOpcionesListener){
        this.listMenu = listadoMenuOpciones
        this.listener = listenerOn
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.rcv_menu_opciones, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = listMenu[position]
        holder.bind(item, listener)
    }

    override fun getItemCount() = listMenu.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: MenuOpciones, menulistener: onMenuOpcionesListener) = with(itemView){
            tvOpcion.text = item.titulo

            if(item.completo){
                imvCompleto.visibility = View.VISIBLE
                imvIncompleto.visibility = View.GONE
            }else{
                imvCompleto.visibility = View.GONE
                imvIncompleto.visibility = View.VISIBLE
            }

            ccMenu.setOnClickListener{ menulistener.seleccionarOpcion(item) }
        }
    }
}