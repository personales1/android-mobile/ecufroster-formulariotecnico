package com.ecufroster.formulario.view.activity.formulario_lista

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import com.ecufroster.formulario.R
import com.ecufroster.formulario.`interface`.onFormulariosEnviadas
import com.ecufroster.formulario.core.BaseActivity
import com.ecufroster.formulario.db.controller.ControllerFormularios
import com.ecufroster.formulario.view.activity.MenuOpcionesFormularioActivity
import com.ecufroster.formulario.view.adapter.AdapterFormulariosEnviados
import kotlinx.android.synthetic.main.activity_formulario_list_enviadas.*
import kotlinx.android.synthetic.main.activity_formulario_list_por_enviar.*
import kotlinx.android.synthetic.main.activity_formulario_list_por_enviar.rcvFormulariosPorEnviar

class FormularioListEnviadasActivity : BaseActivity(), onFormulariosEnviadas {
    var listFormularios : ArrayList<Int> = ArrayList<Int>()
    lateinit var adapter : AdapterFormulariosEnviados

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState, R.layout.activity_formulario_list_enviadas, this)
        configToolbar(R.id.toolbar, "Formularios enviados")

        listFormularios = ControllerFormularios.getListIdFormulariosEnviados()
    }

    override fun select(idFormulario: Int) {
        val intent = Intent(this@FormularioListEnviadasActivity, MenuOpcionesFormularioActivity::class.java)
        intent.putExtra("idForm", idFormulario)
        intent.putExtra("fin", "S")
        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()

        adapter = AdapterFormulariosEnviados(listFormularios, this)
        rcvFormulariosEnviadas.layoutManager = LinearLayoutManager(this)
        rcvFormulariosEnviadas.setHasFixedSize(true)
        rcvFormulariosEnviadas.adapter = adapter
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}