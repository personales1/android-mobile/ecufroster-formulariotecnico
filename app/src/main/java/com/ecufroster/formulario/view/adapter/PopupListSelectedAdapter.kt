package com.ecufroster.formulario.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ecufroster.formulario.R
import com.ecufroster.formulario.`interface`.onPopupListSelectedListener
import com.ecufroster.formulario.core.AppController
import com.ecufroster.formulario.model.PopupListSelected
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.adapter_popup.view.*

class PopupListSelectedAdapter(private val items: ArrayList<PopupListSelected>, private val itemSelected: Int, private val listener: onPopupListSelectedListener)
    : RecyclerView.Adapter<PopupListSelectedAdapter.PopupListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PopupListViewHolder {
        val viewInflate = LayoutInflater.from(parent.context).inflate(R.layout.adapter_popup, parent, false)
        return PopupListSelectedAdapter.PopupListViewHolder(viewInflate)
    }

    override fun onBindViewHolder(holder: PopupListViewHolder, position: Int) {
        val item: PopupListSelected = items[position]
        holder.bind(item, itemSelected, position, listener)
    }

    override fun getItemCount(): Int = items.size

    class PopupListViewHolder(view: View): RecyclerView.ViewHolder(view){
        private val imvLogo = view.imvLogo
        private val tvDescripciones = view.tvDescripcion
        private val imvCheckActivo = view.imvCheckActivo

        fun bind(itemList: PopupListSelected, itemSelected: Int, position: Int, listener: onPopupListSelectedListener){
            with(itemList){
                tvDescripciones.text = descripcion
                imvLogo.visibility = View.VISIBLE

                if(imagenLink != ""){
                    Picasso.get().load(imagenLink).into(imvLogo)
                }else{
                    if(imagenDrawable != -1){
                        if(colorHexadecimal != ""){
                            android.util.Log.e("color", colorHexadecimal)
                            imvLogo.drawable.setColorFilter(android.graphics.Color.parseColor(colorHexadecimal), android.graphics.PorterDuff.Mode.SRC_ATOP)
                        }
                    }else{
                        imvLogo.visibility = View.GONE
                    }
                }

                if(itemSelected == position){
                    imvCheckActivo.visibility = View.VISIBLE
                }else{
                    imvCheckActivo.visibility = View.GONE
                }
            }

            itemView.setOnClickListener { listener.onSelected(position) }
        }
    }
}