package com.ecufroster.formulario.db.model

import com.google.gson.annotations.SerializedName

class DBFormulario (
    @SerializedName("id") var id: Int = -1,
    @SerializedName("formularios_finalizados") var formulariosFinalizados: Int = 1,
    @SerializedName("subido_server") var subidoServer: Int = 1,
    @SerializedName("fecha_finalizado") var fechaFinalizado: String = "",
    @SerializedName("hora_finalizado") var horaFinalizado: String = "",
    @SerializedName("fecha_envio") var fechaEnvio: String = "",
    @SerializedName("hora_envio") var horaEnvio: String = "",
    @SerializedName("estado") var estado: Int = 1,
    @SerializedName("id_tipo_formulario") var idTipoFormulario: Int = -1,
    @SerializedName("tipo_formulario") var tipoFormulario: String = "",
)