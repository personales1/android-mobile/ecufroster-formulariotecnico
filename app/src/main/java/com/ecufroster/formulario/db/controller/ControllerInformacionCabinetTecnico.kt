package com.ecufroster.formulario.db.controller

import android.content.ContentValues
import android.database.Cursor
import com.ecufroster.formulario.core.AppController
import com.ecufroster.formulario.core.DataBase
import com.ecufroster.formulario.core.Db_TABLE_FORMULARIO_INFORMACION_CABINET_TECNICA
import com.ecufroster.formulario.core.Preferencias
import com.ecufroster.formulario.db.model.DBFormularioInformacionCabinetTecnica

class ControllerInformacionCabinetTecnico {
    companion object{
        var db: DataBase? = null
        var cursor: Cursor? = null
        var preferencias: Preferencias = Preferencias(AppController.mInstance.applicationContext)

        fun getDataByIdForm(idForm: Int): DBFormularioInformacionCabinetTecnica {
            val query = "select * from $Db_TABLE_FORMULARIO_INFORMACION_CABINET_TECNICA where id_formulario = $idForm"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if (cursor!!.moveToFirst()){ //Si existe un formulario que no se haya finalizado, envía true
                return DBFormularioInformacionCabinetTecnica(
                    tipoGas = cursor!!.getString(cursor!!.getColumnIndex("tipo_gas")).trim(),
                    familiaTipoGas = cursor!!.getString(cursor!!.getColumnIndex("familia_tipo_gas")).trim(),
                    cargaGas = cursor!!.getString(cursor!!.getColumnIndex("carga_gas")).trim(),
                    voltaje = cursor!!.getString(cursor!!.getColumnIndex("voltaje")).trim(),
                    consumo = cursor!!.getString(cursor!!.getColumnIndex("consumo")).trim(),
                    kwh24Horas = cursor!!.getString(cursor!!.getColumnIndex("kwh_24_horas")).trim(),
                    kwhMesAproximado = cursor!!.getString(cursor!!.getColumnIndex("kwh_mes_aproximado")).trim(),
                    beneficioMedioAmbiente = cursor!!.getString(cursor!!.getColumnIndex("beneficio_medio_ambiente")).trim(),
                    idFormulario = cursor!!.getInt(cursor!!.getColumnIndex("id_formulario")),
                    formularioFinalizado = cursor!!.getInt(cursor!!.getColumnIndex("formulario_finalizado"))
                )
            }

            return DBFormularioInformacionCabinetTecnica()
        }

        fun formIsFinalizado(idForm: Int): Boolean{
            val query = "select * from $Db_TABLE_FORMULARIO_INFORMACION_CABINET_TECNICA where id_formulario = $idForm"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if (cursor!!.moveToFirst()){
                var isFinalizado = cursor!!.getInt(
                    cursor!!.getColumnIndex("formulario_finalizado"))

                if(isFinalizado == 1){
                    return true
                }
            }

            return false
        }

        fun createForm(form: DBFormularioInformacionCabinetTecnica){
            val query = "select * from $Db_TABLE_FORMULARIO_INFORMACION_CABINET_TECNICA where id_formulario = ${form.idFormulario}"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if(cursor!!.moveToFirst()){
                updateForm(form)
            }
            else{
                insertForm(form)
            }
        }

        fun insertForm(form: DBFormularioInformacionCabinetTecnica){
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            val contentValue = ContentValues()

            contentValue.put("tipo_gas", form.tipoGas)
            contentValue.put("familia_tipo_gas", form.familiaTipoGas)
            contentValue.put("carga_gas", form.cargaGas)
            contentValue.put("voltaje", form.voltaje)
            contentValue.put("consumo", form.consumo)
            contentValue.put("kwh_24_horas", form.kwh24Horas)
            contentValue.put("kwh_mes_aproximado", form.kwhMesAproximado)
            contentValue.put("beneficio_medio_ambiente", form.beneficioMedioAmbiente)
            contentValue.put("id_formulario", form.idFormulario)
            contentValue.put("formulario_finalizado", form.formularioFinalizado)

            db!!.insert(Db_TABLE_FORMULARIO_INFORMACION_CABINET_TECNICA, contentValue)
            db!!.close()
        }

        fun updateForm(form: DBFormularioInformacionCabinetTecnica){
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            val contentValue = ContentValues()

            contentValue.put("tipo_gas", form.tipoGas)
            contentValue.put("familia_tipo_gas", form.familiaTipoGas)
            contentValue.put("carga_gas", form.cargaGas)
            contentValue.put("voltaje", form.voltaje)
            contentValue.put("consumo", form.consumo)
            contentValue.put("kwh_24_horas", form.kwh24Horas)
            contentValue.put("kwh_mes_aproximado", form.kwhMesAproximado)
            contentValue.put("beneficio_medio_ambiente", form.beneficioMedioAmbiente)
            contentValue.put("id_formulario", form.idFormulario)
            contentValue.put("formulario_finalizado", form.formularioFinalizado)

            db!!.update(Db_TABLE_FORMULARIO_INFORMACION_CABINET_TECNICA, contentValue, "id_formulario='"+ form.idFormulario +"'")
            db!!.close()
        }
    }
}