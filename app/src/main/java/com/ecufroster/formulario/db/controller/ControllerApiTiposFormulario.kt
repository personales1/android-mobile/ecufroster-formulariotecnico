package com.ecufroster.formulario.db.controller

import android.content.ContentValues
import android.database.Cursor
import com.ecufroster.formulario.core.*
import com.ecufroster.formulario.retrofit.model.response.ApiResponseTiposFormulario
import java.util.*

class ControllerApiTiposFormulario {
    companion object{
        var db: DataBase? = null
        var cursor: Cursor? = null
        var preferencias: Preferencias = Preferencias(AppController.mInstance.applicationContext)
        var listTiposForm: ArrayList<ApiResponseTiposFormulario> = ArrayList<ApiResponseTiposFormulario>()

        fun existData(): Boolean{
            val query = "select * from $Db_TABLE_API_TPOS_FORMULARIOS LIMIT 1"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            var retorna = false
            if(cursor!!.moveToFirst()){
                retorna = true
            }

            db!!.close()
            return retorna
        }

        fun getData(): ArrayList<ApiResponseTiposFormulario> {
            val query = "select * from $Db_TABLE_API_TPOS_FORMULARIOS"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            listTiposForm.clear()
            if (cursor!!.moveToFirst()){
                do{
                    listTiposForm.add(
                        ApiResponseTiposFormulario(
                            id = cursor!!.getString(cursor!!.getColumnIndex("id")).trim(),
                            nombre = cursor!!.getString(cursor!!.getColumnIndex("nombre")).trim(),
                            codigo = cursor!!.getString(cursor!!.getColumnIndex("codigo")).trim()
                        )
                    )
                }while(cursor!!.moveToNext());
            }

            cursor!!.close();
            db!!.close();
            return listTiposForm
        }

        fun createNew(list: List<ApiResponseTiposFormulario>){
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            val contentValue = ContentValues()

            for(item in list){
                contentValue.put("id", item.id)
                contentValue.put("nombre", item.nombre)
                contentValue.put("codigo", item.codigo)

                db!!.insert(Db_TABLE_API_TPOS_FORMULARIOS, contentValue)
            }

            db!!.close()
        }

        fun getFormByCode(code: String): ApiResponseTiposFormulario {
            val query = "select * from $Db_TABLE_API_TPOS_FORMULARIOS where codigo = '$code' LIMIT 1"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if (cursor!!.moveToFirst()){
                return ApiResponseTiposFormulario(
                    id = cursor!!.getString(cursor!!.getColumnIndex("id")),
                    codigo = cursor!!.getString(cursor!!.getColumnIndex("codigo")),
                    nombre = cursor!!.getString(cursor!!.getColumnIndex("nombre"))
                )
            }

            return ApiResponseTiposFormulario()
        }

        fun deleteAll(){
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            db!!.delete(Db_TABLE_API_TPOS_FORMULARIOS, "", null)
            db!!.close()
        }
    }
}