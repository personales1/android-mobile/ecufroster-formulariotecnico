package com.ecufroster.formulario.db.controller

import android.content.ContentValues
import android.database.Cursor
import com.ecufroster.formulario.core.AppController
import com.ecufroster.formulario.core.DataBase
import com.ecufroster.formulario.core.Db_TABLE_FORMULARIO_INFORMACION_CLIENTE
import com.ecufroster.formulario.core.Preferencias
import com.ecufroster.formulario.db.model.DBFormularioInformacionCliente

class ControllerInformacionCliente {
    companion object{
        var db: DataBase? = null
        var cursor: Cursor? = null
        var preferencias: Preferencias = Preferencias(AppController.mInstance.applicationContext)

        fun getDataByIdForm(idForm: Int): DBFormularioInformacionCliente {
            val query = "select * from $Db_TABLE_FORMULARIO_INFORMACION_CLIENTE where id_formulario = $idForm"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if (cursor!!.moveToFirst()){ //Si existe un formulario que no se haya finalizado, envía true
                return DBFormularioInformacionCliente(
                    codigoSap = cursor!!.getString(cursor!!.getColumnIndex("codigo_sap")).trim(),
                    cliente = cursor!!.getString(cursor!!.getColumnIndex("cliente")).trim(),
                    localNombre = cursor!!.getString(cursor!!.getColumnIndex("local_nombre")).trim(),
                    localDireccion = cursor!!.getString(cursor!!.getColumnIndex("local_direccion")).trim(),
                    localTelefono = cursor!!.getString(cursor!!.getColumnIndex("local_telefono")).trim(),
                    localCiudad = cursor!!.getString(cursor!!.getColumnIndex("local_ciudad")).trim(),
                    foraneo = cursor!!.getString(cursor!!.getColumnIndex("foraneo")).trim(),
                    territorio = cursor!!.getString(cursor!!.getColumnIndex("territorio")).trim(),
                    vendedor = cursor!!.getString(cursor!!.getColumnIndex("vendedor")).trim(),
                    vendedorTelefono = cursor!!.getString(cursor!!.getColumnIndex("vendedor_telefono")).trim(),
                    latitud = cursor!!.getString(cursor!!.getColumnIndex("latitud")).trim(),
                    longitud = cursor!!.getString(cursor!!.getColumnIndex("longitud")).trim(),
                    idFormulario = cursor!!.getInt(cursor!!.getColumnIndex("id_formulario")),
                    formularioFinalizado = cursor!!.getInt(cursor!!.getColumnIndex("formulario_finalizado"))
                )
            }

            return DBFormularioInformacionCliente()
        }

        fun createNew(data: DBFormularioInformacionCliente){
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()

            val contentValue = ContentValues()
            contentValue.put("codigo_sap", data.codigoSap)
            contentValue.put("cliente", data.cliente)
            contentValue.put("local_nombre", data.localNombre)
            contentValue.put("local_direccion", data.localDireccion)
            contentValue.put("local_telefono", data.localTelefono)
            contentValue.put("local_ciudad", data.localCiudad)
            contentValue.put("foraneo", data.foraneo)
            contentValue.put("territorio", data.territorio)
            contentValue.put("vendedor", data.vendedor)
            contentValue.put("vendedor_telefono", data.vendedorTelefono)
            contentValue.put("latitud", data.latitud)
            contentValue.put("longitud", data.longitud)
            contentValue.put("id_formulario", data.idFormulario)
            contentValue.put("formulario_finalizado", 1)

            if(getDataByIdForm(data.idFormulario).idFormulario == -1){
                db!!.insert(Db_TABLE_FORMULARIO_INFORMACION_CLIENTE, contentValue);
            }
            else{
                db!!.update(Db_TABLE_FORMULARIO_INFORMACION_CLIENTE, contentValue, "id_formulario='" + data.idFormulario + "'");
            }

            db!!.close()
        }

        fun formIsFinalizado(idForm: Int): Boolean{
            val query = "select * from $Db_TABLE_FORMULARIO_INFORMACION_CLIENTE where id_formulario = $idForm"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if (cursor!!.moveToFirst()){
                var isFinalizado = cursor!!.getInt(cursor!!.getColumnIndex("formulario_finalizado"))

                if(isFinalizado == 1){
                    return true
                }
            }

            return false
        }

        fun createForm(form: DBFormularioInformacionCliente){
            val query = "select * from $Db_TABLE_FORMULARIO_INFORMACION_CLIENTE where id_formulario = ${form.idFormulario}"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if(cursor!!.moveToFirst()){
                updateForm(form)
            }
            else{
                insertForm(form)
            }
        }

        fun insertForm(form: DBFormularioInformacionCliente){
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            val contentValue = ContentValues()

            contentValue.put("codigo_sap", form.codigoSap)
            contentValue.put("cliente", form.cliente)
            contentValue.put("local_nombre", form.localNombre)
            contentValue.put("local_direccion", form.localDireccion)
            contentValue.put("local_telefono", form.localTelefono)
            contentValue.put("local_ciudad", form.localCiudad)
            contentValue.put("foraneo", form.foraneo)
            contentValue.put("territorio", form.territorio)
            contentValue.put("vendedor", form.vendedor)
            contentValue.put("vendedor_telefono", form.vendedorTelefono)
            contentValue.put("latitud", form.latitud)
            contentValue.put("longitud", form.longitud)
            contentValue.put("id_formulario", form.idFormulario)
            contentValue.put("formulario_finalizado", form.formularioFinalizado)

            db!!.insert(Db_TABLE_FORMULARIO_INFORMACION_CLIENTE, contentValue)
            db!!.close()
        }

        fun updateForm(form: DBFormularioInformacionCliente){
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            val contentValue = ContentValues()

            contentValue.put("codigo_sap", form.codigoSap)
            contentValue.put("cliente", form.cliente)
            contentValue.put("local_nombre", form.localNombre)
            contentValue.put("local_direccion", form.localDireccion)
            contentValue.put("local_telefono", form.localTelefono)
            contentValue.put("local_ciudad", form.localCiudad)
            contentValue.put("foraneo", form.foraneo)
            contentValue.put("territorio", form.territorio)
            contentValue.put("vendedor", form.vendedor)
            contentValue.put("vendedor_telefono", form.vendedorTelefono)
            contentValue.put("latitud", form.latitud)
            contentValue.put("longitud", form.longitud)
            contentValue.put("id_formulario", form.idFormulario)
            contentValue.put("formulario_finalizado", form.formularioFinalizado)

            db!!.update(Db_TABLE_FORMULARIO_INFORMACION_CLIENTE, contentValue, "id_formulario='"+ form.idFormulario +"'")
            db!!.close()
        }
    }
}