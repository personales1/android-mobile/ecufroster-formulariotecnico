package com.ecufroster.formulario.db.controller

import android.content.ContentValues
import android.database.Cursor
import com.ecufroster.formulario.core.*
import com.ecufroster.formulario.db.model.DBFormularioImagen
import java.util.*

class ControllerImagenFormulario {
    companion object{
        var db: DataBase? = null
        var cursor: Cursor? = null
        var preferencias: Preferencias = Preferencias(AppController.mInstance.applicationContext)
        var listFotos: ArrayList<DBFormularioImagen> = ArrayList<DBFormularioImagen>()

        fun getFirmaByIdForm(idForm: Int, perteneceA: String): DBFormularioImagen {
            val query = "select * from $Db_TABLE_FORMULARIO_IMAGEN where id_formulario = $idForm and tipo_imagen = '$TIPO_IMAGEN_FIRMA' and pertenece = '$perteneceA'"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if (cursor!!.moveToFirst()){
                return DBFormularioImagen(
                    rutaImagen = cursor!!.getString(cursor!!.getColumnIndex("ruta_imagen")).trim(),
                    tipoImagen = cursor!!.getString(cursor!!.getColumnIndex("tipo_imagen")).trim(),
                    pertenece = cursor!!.getString(cursor!!.getColumnIndex("pertenece")).trim(),
                    idFormulario = cursor!!.getInt(cursor!!.getColumnIndex("id_formulario"))
                )
            }

            return DBFormularioImagen()
        }

        fun getFotosByIdForm(idForm: Int, perteneceA: String): ArrayList<DBFormularioImagen> {
            val query = "select * from $Db_TABLE_FORMULARIO_IMAGEN where id_formulario = $idForm and tipo_imagen = '$TIPO_IMAGEN_FOTOS' and pertenece = '$perteneceA'"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            listFotos.clear()

            if(cursor!!.moveToFirst()){
                do{
                    listFotos.add(
                        DBFormularioImagen(
                            rutaImagen = cursor!!.getString(cursor!!.getColumnIndex("ruta_imagen")).trim(),
                            tipoImagen = cursor!!.getString(cursor!!.getColumnIndex("tipo_imagen")).trim(),
                            pertenece = cursor!!.getString(cursor!!.getColumnIndex("pertenece")).trim(),
                            idFormulario = cursor!!.getInt(cursor!!.getColumnIndex("id_formulario")),
                            numFoto = cursor!!.getInt(cursor!!.getColumnIndex("num_foto"))
                        )
                    )
                }while(cursor!!.moveToNext())
            }

            return listFotos
        }

        fun createFormFirma(form: DBFormularioImagen){
            val query = "select * from $Db_TABLE_FORMULARIO_IMAGEN where id_formulario = ${form.idFormulario} and tipo_imagen = '${form.tipoImagen}' and pertenece = '${form.pertenece}'"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if(cursor!!.moveToFirst()){
                updateForm(form)
            }
            else{
                insertForm(form)
            }
        }

        fun createFormFotosReporte(form: DBFormularioImagen){
            val query = "select * from $Db_TABLE_FORMULARIO_IMAGEN where id_formulario=${form.idFormulario} and tipo_imagen='${form.tipoImagen}' and pertenece='${form.pertenece}' and ruta_imagen='${form.rutaImagen}'"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if(cursor!!.moveToFirst()){
                updateForm(form)
            }
            else{
                insertForm(form)
            }
        }

        fun insertForm(form: DBFormularioImagen){
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            val contentValue = ContentValues()

            contentValue.put("ruta_imagen", form.rutaImagen)
            contentValue.put("tipo_imagen", form.tipoImagen)
            contentValue.put("pertenece", form.pertenece)
            contentValue.put("id_formulario", form.idFormulario)
            contentValue.put("num_foto", form.numFoto)

            db!!.insert(Db_TABLE_FORMULARIO_IMAGEN, contentValue)
            db!!.close()
        }

        fun updateForm(form: DBFormularioImagen){
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            val contentValue = ContentValues()

            contentValue.put("ruta_imagen", form.rutaImagen)
            contentValue.put("tipo_imagen", form.tipoImagen)
            contentValue.put("pertenece", form.pertenece)
            contentValue.put("id_formulario", form.idFormulario)
            contentValue.put("num_foto", form.numFoto)

            db!!.update(Db_TABLE_FORMULARIO_IMAGEN, contentValue, "id_formulario='"+ form.idFormulario +"' and tipo_imagen='"+ form.tipoImagen +"' and pertenece='"+ form.pertenece +"' and ruta_imagen='"+ form.rutaImagen +"'")
            db!!.close()
        }
    }
}