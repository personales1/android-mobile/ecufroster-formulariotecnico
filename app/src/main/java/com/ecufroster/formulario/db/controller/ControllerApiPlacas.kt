package com.ecufroster.formulario.db.controller

import android.content.ContentValues
import android.database.Cursor
import com.ecufroster.formulario.core.*
import com.ecufroster.formulario.retrofit.model.response.ApiResponsePlaca
import java.util.ArrayList

class ControllerApiPlacas {
    companion object {
        var db: DataBase? = null
        var cursor: Cursor? = null
        var preferencias: Preferencias = Preferencias(AppController.mInstance.applicationContext)
        var listPlaca: ArrayList<ApiResponsePlaca> = ArrayList<ApiResponsePlaca>()

        fun existData(): Boolean{
            val query = "select * from $Db_TABLE_API_PLACAS LIMIT 1"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            var retorna = false
            if(cursor!!.moveToFirst()){
                retorna = true
            }

            db!!.close()
            return retorna
        }

        fun createNew(list: List<ApiResponsePlaca>){
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            val contentValue = ContentValues()

            for(item in list){
                contentValue.put("placa", item.placa)
                contentValue.put("anio_importacion", item.anioImportacion)

                db!!.insert(Db_TABLE_API_PLACAS, contentValue)
            }

            db!!.close()
        }

        fun getFormByPlaca(placa : String): ApiResponsePlaca {
            val query = "select * from $Db_TABLE_API_PLACAS where placa = '$placa' LIMIT 1"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if (cursor!!.moveToFirst()){
                return ApiResponsePlaca(
                    placa = cursor!!.getString(cursor!!.getColumnIndex("placa")),
                    anioImportacion = cursor!!.getInt(cursor!!.getColumnIndex("anio_importacion"))
                )
            }

            return ApiResponsePlaca()
        }

        fun deleteAll(){
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            db!!.delete(Db_TABLE_API_PLACAS, "", null)
            db!!.close()
        }
    }
}