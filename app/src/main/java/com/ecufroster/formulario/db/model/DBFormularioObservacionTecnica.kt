package com.ecufroster.formulario.db.model

import com.google.gson.annotations.SerializedName

class DBFormularioObservacionTecnica (
    @SerializedName("tipo_status") var tipoStatus: String = "",
    @SerializedName("tipo_movimiento") var tipoMovimiento: String = "",
    @SerializedName("observacion") var observacion: String = "",
    @SerializedName("id_formulario") var idFormulario: Int = -1,
    @SerializedName("formulario_finalizado")  var formularioFinalizado: Int = 0
)