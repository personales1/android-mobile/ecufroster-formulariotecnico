package com.ecufroster.formulario.db.model

import com.google.gson.annotations.SerializedName

class DBFormularioInformacion (
    @SerializedName("cliente") var cliente: String = "",
    @SerializedName("tipo_formulario") var tipoFormulario: String = "",
    @SerializedName("tecnico_asignado") var tecnicoAsignado: String = "",
    @SerializedName("fecha_requerimiento") var fechaRequerimiento: String = "",
    @SerializedName("hora_requerimiento") var horaRequerimiento: String = "",
    @SerializedName("id_formulario") var idFormulario: Int = -1,
    @SerializedName("formulario_finalizado") var formularioFinalizado: Int = 0
)