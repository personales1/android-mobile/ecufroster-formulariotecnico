package com.ecufroster.formulario.db.controller

import android.content.ContentValues
import android.database.Cursor
import com.ecufroster.formulario.core.AppController
import com.ecufroster.formulario.core.DataBase
import com.ecufroster.formulario.core.Db_TABLE_FORMULARIO
import com.ecufroster.formulario.core.Preferencias
import com.ecufroster.formulario.db.model.DBFormulario
import com.ecufroster.formulario.helper.UtilidadesHelper

class ControllerFormularios {
    companion object {
        var db: DataBase? = null
        var cursor: Cursor? = null
        var preferencias: Preferencias = Preferencias(AppController.mInstance.applicationContext)
        var listIdForm: ArrayList<Int> = ArrayList()

        fun getDataByIdForm(idForm: Int): DBFormulario {
            val query = "select * from $Db_TABLE_FORMULARIO where _id = $idForm"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if (cursor!!.moveToFirst()){ //Si existe un formulario que no se haya finalizado, envía true
                return DBFormulario(
                    id = cursor!!.getInt(cursor!!.getColumnIndex("_id")),
                    formulariosFinalizados = cursor!!.getInt(cursor!!.getColumnIndex("formularios_finalizados")),
                    subidoServer = cursor!!.getInt(cursor!!.getColumnIndex("subido_server")),
                    fechaFinalizado = cursor!!.getString(cursor!!.getColumnIndex("fecha_finalizado")).trim(),
                    horaFinalizado = cursor!!.getString(cursor!!.getColumnIndex("hora_finalizado")).trim(),
                    fechaEnvio = cursor!!.getString(cursor!!.getColumnIndex("fecha_envio")).trim(),
                    horaEnvio = cursor!!.getString(cursor!!.getColumnIndex("hora_envio")).trim(),
                    estado = cursor!!.getInt(cursor!!.getColumnIndex("estado")),
                    idTipoFormulario = cursor!!.getInt(cursor!!.getColumnIndex("id_tipo_formulario")),
                    tipoFormulario = cursor!!.getString(cursor!!.getColumnIndex("tipo_formulario")).trim(),
                )
            }

            return DBFormulario()
        }

        fun isFormFinalizado(idFormulario: Int): Boolean{
            val query = "select * from $Db_TABLE_FORMULARIO where _id = $idFormulario"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if (cursor!!.moveToFirst()){
                if(cursor!!.getInt(cursor!!.getColumnIndex("formularios_finalizados")) == 1){
                    return true
                }
            }

            return false
        }

        fun isFormEnviado(idFormulario: Int): Boolean{
            val query = "select * from $Db_TABLE_FORMULARIO where _id = $idFormulario"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if (cursor!!.moveToFirst()){
                if(cursor!!.getInt(cursor!!.getColumnIndex("subido_server")) == 1){
                    return true
                }
            }

            return false
        }

        fun existFormPending(): Boolean{
            val query = "select * from $Db_TABLE_FORMULARIO where formularios_finalizados = -1"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if (cursor!!.moveToFirst()){ //Si existe un formulario que no se haya finalizado, envía true
                return true
            }

            return false
        }

        fun getListIdFormulariosPendientesPorSubir(): ArrayList<Int>{
            val query = "select * from $Db_TABLE_FORMULARIO where formularios_finalizados != -1 and subido_server = 0"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);
            listIdForm.clear()

            if (cursor!!.moveToFirst()){
                do{
                    listIdForm.add(cursor!!.getInt(cursor!!.getColumnIndex("_id")))
                }while(cursor!!.moveToNext())
            }

            return listIdForm
        }

        fun getListIdFormulariosFinalizadosPorSubir(): ArrayList<Int>{
            val query = "select * from $Db_TABLE_FORMULARIO where formularios_finalizados = 1 and subido_server = 0"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);
            listIdForm.clear()

            if (cursor!!.moveToFirst()){
                do{
                    listIdForm.add(cursor!!.getInt(cursor!!.getColumnIndex("_id")))
                }while(cursor!!.moveToNext())
            }

            return listIdForm
        }

        fun getListIdFormulariosEnviados(): ArrayList<Int>{
            val query = "select * from $Db_TABLE_FORMULARIO where formularios_finalizados = 1 and subido_server = 1 order by _id desc"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);
            listIdForm.clear()

            if (cursor!!.moveToFirst()){
                do{
                    listIdForm.add(cursor!!.getInt(cursor!!.getColumnIndex("_id")))
                }while(cursor!!.moveToNext())
            }

            return listIdForm
        }

        fun createNewForm(idTipoFormulario: Int, codigoTipoFormulario: String): Int{
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()

            val contentValue = ContentValues()
            contentValue.put("formularios_finalizados", -1)
            contentValue.put("subido_server", 0)
            contentValue.put("estado", 1)
            contentValue.put("id_tipo_formulario", idTipoFormulario)
            contentValue.put("tipo_formulario", codigoTipoFormulario)
            val insertedId = db!!.insert(Db_TABLE_FORMULARIO, contentValue);
            db!!.close()

            return insertedId.toInt()
        }

        fun getIdFormPending(): Int{
            val query = "select * from $Db_TABLE_FORMULARIO where formularios_finalizados = -1"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if (cursor!!.moveToFirst()){
                return cursor!!.getInt(cursor!!.getColumnIndex("_id"))
            }

            return 0
        }

        fun setFormFinalizado(pending: Int, idFormulario: Int){
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()

            val contentValue = ContentValues()
            contentValue.put("formularios_finalizados", pending)
            contentValue.put("fecha_finalizado", UtilidadesHelper.getDateCurrentFormatStandar())
            contentValue.put("hora_finalizado", UtilidadesHelper.getTimeCurrentFormatStandar())

            db!!.update(Db_TABLE_FORMULARIO, contentValue, "_id='$idFormulario'")
            db!!.close()
        }

        fun setFormEnviado(idFormulario: Int){
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()

            val contentValue = ContentValues()
            contentValue.put("formularios_finalizados", 1)
            contentValue.put("subido_server", 1)
            contentValue.put("fecha_envio", UtilidadesHelper.getDateCurrentFormatStandar())
            contentValue.put("hora_envio", UtilidadesHelper.getTimeCurrentFormatStandar())

            db!!.update(Db_TABLE_FORMULARIO, contentValue, "_id='$idFormulario'")
            db!!.close()
        }
    }
}