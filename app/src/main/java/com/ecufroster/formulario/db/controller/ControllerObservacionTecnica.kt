package com.ecufroster.formulario.db.controller

import android.content.ContentValues
import android.database.Cursor
import com.ecufroster.formulario.core.*
import com.ecufroster.formulario.db.model.DBFormularioObservacionTecnica

class ControllerObservacionTecnica {
    companion object{
        var db: DataBase? = null
        var cursor: Cursor? = null
        var preferencias: Preferencias = Preferencias(AppController.mInstance.applicationContext)

        fun getDataByIdForm(idForm: Int): DBFormularioObservacionTecnica {
            val query = "select * from $Db_TABLE_FORMULARIO_OBSERVACION_TECNICA where id_formulario = $idForm"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if (cursor!!.moveToFirst()){ //Si existe un formulario que no se haya finalizado, envía true
                return DBFormularioObservacionTecnica(
                    tipoStatus = cursor!!.getString(cursor!!.getColumnIndex("tipo_status")).trim(),
                    tipoMovimiento = cursor!!.getString(cursor!!.getColumnIndex("tipo_movimiento")).trim(),
                    observacion = cursor!!.getString(cursor!!.getColumnIndex("observacion")).trim(),
                    idFormulario = cursor!!.getInt(cursor!!.getColumnIndex("id_formulario")),
                    formularioFinalizado = cursor!!.getInt(cursor!!.getColumnIndex("formulario_finalizado"))
                )
            }

            return DBFormularioObservacionTecnica()
        }

        fun formIsFinalizado(idForm: Int): Boolean{
            val query = "select * from $Db_TABLE_FORMULARIO_OBSERVACION_TECNICA where id_formulario = $idForm"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if (cursor!!.moveToFirst()){
                var isFinalizado = cursor!!.getInt(
                    cursor!!.getColumnIndex("formulario_finalizado"))

                if(isFinalizado == 1){
                    return true
                }
            }

            return false
        }

        fun createForm(form: DBFormularioObservacionTecnica){
            val query = "select * from $Db_TABLE_FORMULARIO_OBSERVACION_TECNICA where id_formulario = ${form.idFormulario}"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if(cursor!!.moveToFirst()){
                updateForm(form)
            }
            else{
                insertForm(form)
            }
        }

        fun insertForm(form: DBFormularioObservacionTecnica){
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            val contentValue = ContentValues()

            contentValue.put("tipo_status", form.tipoStatus)
            contentValue.put("tipo_movimiento", form.tipoMovimiento)
            contentValue.put("observacion", form.observacion)
            contentValue.put("id_formulario", form.idFormulario)
            contentValue.put("formulario_finalizado", form.formularioFinalizado)

            db!!.insert(Db_TABLE_FORMULARIO_OBSERVACION_TECNICA, contentValue)
            db!!.close()
        }

        fun updateForm(form: DBFormularioObservacionTecnica){
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            val contentValue = ContentValues()

            contentValue.put("tipo_status", form.tipoStatus)
            contentValue.put("tipo_movimiento", form.tipoMovimiento)
            contentValue.put("observacion", form.observacion)
            contentValue.put("id_formulario", form.idFormulario)
            contentValue.put("formulario_finalizado", form.formularioFinalizado)

            db!!.update(Db_TABLE_FORMULARIO_OBSERVACION_TECNICA, contentValue, "id_formulario='"+ form.idFormulario +"'")
            db!!.close()
        }
    }
}