package com.ecufroster.formulario.db.controller

import android.content.ContentValues
import android.database.Cursor
import com.ecufroster.formulario.core.*
import com.ecufroster.formulario.retrofit.model.response.ApiResponseCabinet
import java.util.ArrayList

class ControllerApiCabinets {
    companion object{
        var db: DataBase? = null
        var cursor: Cursor? = null
        var preferencias: Preferencias = Preferencias(AppController.mInstance.applicationContext)
        var listCabinet: ArrayList<ApiResponseCabinet> = ArrayList<ApiResponseCabinet>()

        fun existData(): Boolean{
            val query = "select * from $Db_TABLE_API_CABINETS LIMIT 1"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            var retorna = false
            if(cursor!!.moveToFirst()){
                retorna = true
            }

            db!!.close()
            return retorna
        }

        fun createNew(list: List<ApiResponseCabinet>){
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            val contentValue = ContentValues()

            for(item in list){
                contentValue.put("id", item.id)
                contentValue.put("marca", item.marca)
                contentValue.put("modelo", item.modelo)
                contentValue.put("tipo", item.tipo)
                contentValue.put("canastillas_parrillas", item.canastillasParrillas)

                db!!.insert(Db_TABLE_API_CABINETS, contentValue)
            }

            db!!.close()
        }

        fun getCabinet(): ArrayList<ApiResponseCabinet>{
            val query = "select * from $Db_TABLE_API_CABINETS"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            listCabinet.clear()

            if (cursor!!.moveToFirst()){
                do{
                    listCabinet.add(
                        ApiResponseCabinet(
                            id = cursor!!.getString(cursor!!.getColumnIndex("id")),
                            marca = cursor!!.getString(cursor!!.getColumnIndex("marca")),
                            modelo = cursor!!.getString(cursor!!.getColumnIndex("modelo")),
                            tipo = cursor!!.getString(cursor!!.getColumnIndex("tipo")),
                            canastillasParrillas = cursor!!.getString(cursor!!.getColumnIndex("canastillas_parrillas"))
                        )
                    )
                }while(cursor!!.moveToNext())
            }

            return listCabinet
        }

        fun getFormByCode(code : String): ApiResponseCabinet {
            val query = "select * from $Db_TABLE_API_CABINETS where codigo = '$code' LIMIT 1"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if (cursor!!.moveToFirst()){
                return ApiResponseCabinet(
                    id = cursor!!.getString(cursor!!.getColumnIndex("id")),
                    marca = cursor!!.getString(cursor!!.getColumnIndex("marca")),
                    modelo = cursor!!.getString(cursor!!.getColumnIndex("modelo")),
                    tipo = cursor!!.getString(cursor!!.getColumnIndex("tipo")),
                    canastillasParrillas = cursor!!.getString(cursor!!.getColumnIndex("canastillas_parrillas"))
                )
            }

            return ApiResponseCabinet()
        }

        fun deleteAll(){
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            db!!.delete(Db_TABLE_API_CABINETS, "", null)
            db!!.close()
        }
    }
}