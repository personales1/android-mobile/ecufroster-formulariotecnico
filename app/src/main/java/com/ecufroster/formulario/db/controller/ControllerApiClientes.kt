package com.ecufroster.formulario.db.controller

import android.content.ContentValues
import android.database.Cursor
import com.ecufroster.formulario.core.*
import com.ecufroster.formulario.retrofit.model.response.ApiResponseCliente

class ControllerApiClientes {
    companion object{
        var db: DataBase? = null
        var cursor: Cursor? = null
        var preferencias: Preferencias = Preferencias(AppController.mInstance.applicationContext)

        fun existData(): Boolean{
            val query = "select * from $Db_TABLE_API_CLIENTES LIMIT 1"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            var retorna = false
            if(cursor!!.moveToFirst()){
                retorna = true
            }

            db!!.close()
            return retorna
        }

        fun createNewClient(listClient: List<ApiResponseCliente>){
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            val contentValue = ContentValues()

            for(item in listClient){
                contentValue.put("id", item.id)
                contentValue.put("codigo", item.codigo)
                contentValue.put("nombre", item.nombre)
                contentValue.put("calle", item.calle)
                contentValue.put("fecha_creacion", item.fechaCreacion)
                contentValue.put("grupo", item.grupo)
                contentValue.put("telefono", item.telefono)
                contentValue.put("condicion_pago_descripcion", item.condicionPagoDescripcion)
                contentValue.put("grupo_descripcion", item.grupoDescripcion)
                contentValue.put("email", item.email)
                contentValue.put("territorio", item.territorio)
                contentValue.put("territorio_descripcion", item.territorioDescripcion)
                contentValue.put("ejecutivo", item.ejecutivo)
                contentValue.put("ejecutivo_telefono", item.ejecutivoTelefono)
                contentValue.put("productividad", item.productividad)
                contentValue.put("modelo_atencion", item.modeloAtencion)

                db!!.insert(Db_TABLE_API_CLIENTES, contentValue)
            }

            db!!.close()
        }

        fun getClientByCode(code : String): ApiResponseCliente {
            val query = "select * from $Db_TABLE_API_CLIENTES where codigo = '$code' LIMIT 1"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if (cursor!!.moveToFirst()){
                return ApiResponseCliente(
                    id = cursor!!.getString(cursor!!.getColumnIndex("id")),
                    codigo = cursor!!.getString(cursor!!.getColumnIndex("codigo")),
                    nombre = cursor!!.getString(cursor!!.getColumnIndex("nombre")),
                    calle = cursor!!.getString(cursor!!.getColumnIndex("calle")),
                    fechaCreacion = cursor!!.getString(cursor!!.getColumnIndex("fecha_creacion")),
                    grupo = cursor!!.getString(cursor!!.getColumnIndex("grupo")),
                    telefono = cursor!!.getString(cursor!!.getColumnIndex("telefono")),
                    condicionPagoDescripcion = cursor!!.getString(cursor!!.getColumnIndex("condicion_pago_descripcion")),
                    grupoDescripcion = cursor!!.getString(cursor!!.getColumnIndex("grupo_descripcion")),
                    email = cursor!!.getString(cursor!!.getColumnIndex("email")),
                    territorio = cursor!!.getString(cursor!!.getColumnIndex("territorio")),
                    territorioDescripcion = cursor!!.getString(cursor!!.getColumnIndex("territorio_descripcion")),
                    ejecutivo = cursor!!.getString(cursor!!.getColumnIndex("ejecutivo")),
                    ejecutivoTelefono = cursor!!.getString(cursor!!.getColumnIndex("ejecutivo_telefono")),
                    productividad = cursor!!.getString(cursor!!.getColumnIndex("productividad")),
                    modeloAtencion = cursor!!.getString(cursor!!.getColumnIndex("modelo_atencion"))
                )
            }

            db!!.close()
            return ApiResponseCliente()
        }

        fun deleteAll(){
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            db!!.delete(Db_TABLE_API_CLIENTES, "", null)
            db!!.close()
        }
    }
}