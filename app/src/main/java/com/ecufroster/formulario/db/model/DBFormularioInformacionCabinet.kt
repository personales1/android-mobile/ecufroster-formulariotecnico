package com.ecufroster.formulario.db.model

import com.google.gson.annotations.SerializedName

class DBFormularioInformacionCabinet (
    @SerializedName("tipo_equipo") var tipoEquipo: String = "",
    @SerializedName("marca") var marca: String = "",
    @SerializedName("modelo") var modelo: String = "",
    @SerializedName("placa") var placa: String = "",
    @SerializedName("serie") var serie: String = "",
    @SerializedName("num_canastilas") var numCanastilas: String = "",
    @SerializedName("anio_importacion") var anioImportacion: String = "",
    @SerializedName("id_formulario") var idFormulario: Int = -1,
    @SerializedName("formulario_finalizado") var formularioFinalizado: Int = 0
)