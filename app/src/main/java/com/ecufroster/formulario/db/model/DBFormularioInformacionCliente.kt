package com.ecufroster.formulario.db.model

import com.google.gson.annotations.SerializedName

class DBFormularioInformacionCliente (
    @SerializedName("codigo_sap") var codigoSap: String = "",
    @SerializedName("cliente") var cliente: String = "",
    @SerializedName("local_nombre") var localNombre: String = "",
    @SerializedName("local_direccion") var localDireccion: String = "",
    @SerializedName("local_telefono") var localTelefono: String = "",
    @SerializedName("local_ciudad") var localCiudad: String = "",
    @SerializedName("foraneo") var foraneo: String = "",
    @SerializedName("territorio") var territorio: String = "",
    @SerializedName("vendedor") var vendedor: String = "",
    @SerializedName("vendedor_telefono") var vendedorTelefono: String = "",
    @SerializedName("latitud") var latitud: String = "",
    @SerializedName("longitud") var longitud: String = "",
    @SerializedName("id_formulario") var idFormulario: Int = -1,
    @SerializedName("formulario_finalizado") var formularioFinalizado: Int = 0
)