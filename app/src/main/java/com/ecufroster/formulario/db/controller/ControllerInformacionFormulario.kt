package com.ecufroster.formulario.db.controller

import android.content.ContentValues
import android.database.Cursor
import com.ecufroster.formulario.core.*
import com.ecufroster.formulario.db.model.DBFormularioInformacion

class ControllerInformacionFormulario {
    companion object{
        var db: DataBase? = null
        var cursor: Cursor? = null
        var preferencias: Preferencias = Preferencias(AppController.mInstance.applicationContext)

        fun getDataByIdForm(idForm: Int): DBFormularioInformacion {
            val query = "select * from $Db_TABLE_FORMULARIO_INFORMACION where id_formulario = $idForm"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if (cursor!!.moveToFirst()){ //Si existe un formulario que no se haya finalizado, envía true
                return DBFormularioInformacion(
                    cliente = cursor!!.getString(cursor!!.getColumnIndex("cliente")).trim(),
                    tipoFormulario = cursor!!.getString(cursor!!.getColumnIndex("tipo_formulario")).trim(),
                    tecnicoAsignado = cursor!!.getString(cursor!!.getColumnIndex("tecnico_asignado")).trim(),
                    fechaRequerimiento = cursor!!.getString(cursor!!.getColumnIndex("fecha_requerimiento")).trim(),
                    horaRequerimiento = cursor!!.getString(cursor!!.getColumnIndex("hora_requerimiento")).trim(),
                    idFormulario = cursor!!.getInt(cursor!!.getColumnIndex("id_formulario")),
                    formularioFinalizado = cursor!!.getInt(cursor!!.getColumnIndex("formulario_finalizado"))
                )
            }

            return DBFormularioInformacion()
        }

        fun formIsFinalizado(idForm: Int): Boolean{
            val query = "select * from $Db_TABLE_FORMULARIO_INFORMACION where id_formulario = $idForm"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if (cursor!!.moveToFirst()){
                var isFinalizado = cursor!!.getInt(cursor!!.getColumnIndex("formulario_finalizado"))

                if(isFinalizado == 1){
                    return true
                }
            }

            return false
        }

        fun createForm(form: DBFormularioInformacion){
            val query = "select * from $Db_TABLE_FORMULARIO_INFORMACION where id_formulario = ${form.idFormulario}"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if(cursor!!.moveToFirst()){
                updateForm(form)
            }
            else{
                insertForm(form)
            }
        }

        fun insertForm(form: DBFormularioInformacion){
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            val contentValue = ContentValues()

            contentValue.put("cliente", form.cliente)
            contentValue.put("tipo_formulario", form.tipoFormulario)
            contentValue.put("tecnico_asignado", form.tecnicoAsignado)
            contentValue.put("fecha_requerimiento", form.fechaRequerimiento)
            contentValue.put("hora_requerimiento", form.horaRequerimiento)
            contentValue.put("id_formulario", form.idFormulario)
            contentValue.put("formulario_finalizado", form.formularioFinalizado)

            db!!.insert(Db_TABLE_FORMULARIO_INFORMACION, contentValue)
            db!!.close()
        }

        fun updateForm(form: DBFormularioInformacion){
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            val contentValue = ContentValues()

            contentValue.put("cliente", form.cliente)
            contentValue.put("tipo_formulario", form.tipoFormulario)
            contentValue.put("tecnico_asignado", form.tecnicoAsignado)
            contentValue.put("fecha_requerimiento", form.fechaRequerimiento)
            contentValue.put("hora_requerimiento", form.horaRequerimiento)
            contentValue.put("id_formulario", form.idFormulario)
            contentValue.put("formulario_finalizado", form.formularioFinalizado)

            db!!.update(Db_TABLE_FORMULARIO_INFORMACION, contentValue, "id_formulario='"+ form.idFormulario +"'")
            db!!.close()
        }
    }
}