package com.ecufroster.formulario.db.controller

import android.content.ContentValues
import android.database.Cursor
import com.ecufroster.formulario.core.AppController
import com.ecufroster.formulario.core.DataBase
import com.ecufroster.formulario.core.Db_TABLE_FORMULARIO_INFORMACION_CABINET
import com.ecufroster.formulario.core.Preferencias
import com.ecufroster.formulario.db.model.DBFormularioInformacionCabinet

class ControllerInformacionCabinet {
    companion object{
        var db: DataBase? = null
        var cursor: Cursor? = null
        var preferencias: Preferencias = Preferencias(AppController.mInstance.applicationContext)

        fun getDataByIdForm(idForm: Int): DBFormularioInformacionCabinet {
            val query = "select * from $Db_TABLE_FORMULARIO_INFORMACION_CABINET where id_formulario = $idForm"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if (cursor!!.moveToFirst()){ //Si existe un formulario que no se haya finalizado, envía true
                return DBFormularioInformacionCabinet(
                    marca = cursor!!.getString(cursor!!.getColumnIndex("marca")).trim(),
                    modelo = cursor!!.getString(cursor!!.getColumnIndex("modelo")).trim(),
                    tipoEquipo = cursor!!.getString(cursor!!.getColumnIndex("tipo_equipo")).trim(),
                    placa = cursor!!.getString(cursor!!.getColumnIndex("placa")).trim(),
                    serie = cursor!!.getString(cursor!!.getColumnIndex("serie")).trim(),
                    numCanastilas = cursor!!.getString(cursor!!.getColumnIndex("num_canastilas")).trim(),
                    anioImportacion = cursor!!.getString(cursor!!.getColumnIndex("anio_importacion")).trim(),
                    idFormulario = cursor!!.getInt(cursor!!.getColumnIndex("id_formulario")),
                    formularioFinalizado = cursor!!.getInt(cursor!!.getColumnIndex("formulario_finalizado"))
                )
            }

            return DBFormularioInformacionCabinet()
        }

        fun formIsFinalizado(idForm: Int): Boolean{
            val query = "select * from $Db_TABLE_FORMULARIO_INFORMACION_CABINET where id_formulario = $idForm"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if (cursor!!.moveToFirst()){
                var isFinalizado = cursor!!.getInt(cursor!!.getColumnIndex("formulario_finalizado"))

                if(isFinalizado == 1){
                    return true
                }
            }

            return false
        }

        fun createForm(form: DBFormularioInformacionCabinet){
            val query = "select * from $Db_TABLE_FORMULARIO_INFORMACION_CABINET where id_formulario = ${form.idFormulario}"
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            cursor = db!!.rawQuery(query, null);

            if(cursor!!.moveToFirst()){
                updateForm(form)
            }
            else{
                insertForm(form)
            }
        }

        fun insertForm(form: DBFormularioInformacionCabinet){
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            val contentValue = ContentValues()

            contentValue.put("marca", form.marca)
            contentValue.put("modelo", form.modelo)
            contentValue.put("tipo_equipo", form.tipoEquipo)
            contentValue.put("placa", form.placa)
            contentValue.put("serie", form.serie)
            contentValue.put("num_canastilas", form.numCanastilas)
            contentValue.put("anio_importacion", form.anioImportacion)
            contentValue.put("id_formulario", form.idFormulario)
            contentValue.put("formulario_finalizado", form.formularioFinalizado)

            db!!.insert(Db_TABLE_FORMULARIO_INFORMACION_CABINET, contentValue)
            db!!.close()
        }

        fun updateForm(form: DBFormularioInformacionCabinet){
            db = DataBase(AppController.mInstance.getInstance())
            db!!.open()
            val contentValue = ContentValues()

            contentValue.put("marca", form.marca)
            contentValue.put("modelo", form.modelo)
            contentValue.put("tipo_equipo", form.tipoEquipo)
            contentValue.put("placa", form.placa)
            contentValue.put("serie", form.serie)
            contentValue.put("num_canastilas", form.numCanastilas)
            contentValue.put("anio_importacion", form.anioImportacion)
            contentValue.put("id_formulario", form.idFormulario)
            contentValue.put("formulario_finalizado", form.formularioFinalizado)

            db!!.update(Db_TABLE_FORMULARIO_INFORMACION_CABINET, contentValue, "id_formulario='"+ form.idFormulario +"'")
            db!!.close()
        }
    }
}