package com.ecufroster.formulario.db.model

import com.google.gson.annotations.SerializedName

class DBFormularioImagen (
    @SerializedName("ruta_imagen") var rutaImagen: String = "",
    @SerializedName("tipo_imagen") var tipoImagen: String = "",
    @SerializedName("pertenece") var pertenece: String = "",
    @SerializedName("id_formulario") var idFormulario: Int = -1,
    @SerializedName("num_foto") var numFoto: Int = -1
)