package com.ecufroster.formulario.db.model

import com.google.gson.annotations.SerializedName

class DBFormularioInformacionCabinetTecnica (
    @SerializedName("tipo_gas") var tipoGas: String = "",
    @SerializedName("familia_tipo_gas") var familiaTipoGas: String = "",
    @SerializedName("carga_gas") var cargaGas: String = "",
    @SerializedName("voltaje") var voltaje: String = "",
    @SerializedName("consumo") var consumo: String = "",
    @SerializedName("kwh_24_horas") var kwh24Horas: String = "",
    @SerializedName("kwh_mes_aproximado")  var kwhMesAproximado: String = "",
    @SerializedName("beneficio_medio_ambiente")  var beneficioMedioAmbiente: String = "",
    @SerializedName("id_formulario")  var idFormulario: Int = -1,
    @SerializedName("formulario_finalizado")  var formularioFinalizado: Int = 0
)