package com.ecufroster.formulario.helper

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.Settings
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.ecufroster.formulario.BuildConfig
import com.ecufroster.formulario.R
import com.ecufroster.formulario.model.PermisosApp
import java.util.ArrayList

class PermisosHelper {
    companion object{
        fun hasPermiso(activity: Activity?, permission: String?): Boolean {
            return ContextCompat.checkSelfPermission(activity!!, permission!!) == PackageManager.PERMISSION_GRANTED
        }

        fun solicitarPermiso(activity: Activity?, permission: String, REQUEST_CODE_PERMISSIONS: Int): Boolean {
            val selfPermiso = ContextCompat.checkSelfPermission(activity!!, permission)

            /*SI NO TIENE EL PERMISO*/
            return if (selfPermiso != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity, arrayOf(permission), REQUEST_CODE_PERMISSIONS)
                false
            } else {
                true
            }
        }

        fun solicitarPermisoVariosApp(activity: Activity?, listPermission: List<PermisosApp>, REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: Int): Boolean {
            val selfListPermisos: MutableList<String> = ArrayList()

            for (permission in listPermission) {
                selfListPermisos.add(permission.permiso)
            }

            return solicitarPermisoVarios(activity, selfListPermisos, REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS)
        }

        fun solicitarPermisoVarios(activity: Activity?, listPermission: List<String>, REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: Int): Boolean {
            val selfListPermisos: MutableList<String> = ArrayList()

            for (perm in listPermission) {
                if (ContextCompat.checkSelfPermission(activity!!, perm) != PackageManager.PERMISSION_GRANTED) {
                    selfListPermisos.add(perm)
                }
            }

            return if (selfListPermisos.size > 0) {
                ActivityCompat.requestPermissions(activity!!, selfListPermisos.toTypedArray(), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS)
                false
            } else {
                true
            }
        }

        fun solicitarPermisoInicial(activity: Activity?, listPermission: List<PermisosApp>, REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: Int) {
            var solicitaPermiso = false
            val mensaje = StringBuilder("Necesitas darle acceso a:\n\n")

            for (permission in listPermission) {
                if (! hasPermiso(activity, permission.permiso)) {
                    mensaje.append(permission.descripcion)
                    mensaje.append("\n")
                    solicitaPermiso = true
                }
            }
            if (solicitaPermiso) {
                UtilidadesHelper.mensajeConfirmacion(activity!!, mensaje.toString())
                    .setPositiveButton(R.string.dialogos_aceptar, DialogInterface.OnClickListener { _, _ ->
                        solicitarPermisoVariosApp(
                            activity,
                            listPermission,
                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS
                        )
                    })
                    .setCancelable(false)
                    .show()
            }
        }

        fun validarSolicitudPermiso(activity: Activity, permission: String, motivoPermiso: String?, REQUEST_CODE_PERMISSIONS: Int): Boolean {
            val selfPermiso = ContextCompat.checkSelfPermission(activity, permission)

            /*SI NO TIENE PERMISO PARA HACER LO REQUERIDO*/
            return if (selfPermiso != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                    /*CUANDO RECHAZA LA SOLICITUD DE PERMISO*/
                    ActivityCompat.requestPermissions(activity, arrayOf(permission), REQUEST_CODE_PERMISSIONS)
                }
                else {
                    /**
                     * CUANDO HA RECHAZADO LA SOLICITUD DE PERMISO Y MARCÓ LA OPCIÓN DE "No volver a preguntar"
                     * EN ESTE CASO YA NO SE LE MOSTRARÁ EL DIÁLOGO DE ACEPTAR PERMISO QUE SE MUESTRA POR DEFECTO,
                     * DEBEMOS MOSTRAR UN DIÁLOGO PERSONALIZADO
                     */
                    UtilidadesHelper.mensajeAlerta(activity, activity.getString(R.string.app_name), motivoPermiso!!)
                        .setPositiveButton("Ir a Configuración") { _, _ ->
                            val intent = Intent(
                                Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.parse("package:" + BuildConfig.APPLICATION_ID)
                            )
                            activity.startActivity(intent)
                        }
                        .show()
                }
                false
            } else {
                true
            }
        }

        fun solicitarPermisoList(activity: Activity?, listPermission: List<String>, REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: Int) {
            if (listPermission.isNotEmpty()) {
                ActivityCompat.requestPermissions(activity!!, listPermission.toTypedArray(), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS)
            }
        }
    }
}