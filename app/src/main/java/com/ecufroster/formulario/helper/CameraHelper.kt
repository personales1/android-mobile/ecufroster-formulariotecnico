package com.ecufroster.formulario.helper

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Matrix
import android.media.ExifInterface
import android.os.Build
import android.util.Base64
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.ArrayList

class CameraHelper {
    companion object {
        @JvmStatic
        fun checkCameraExists(activity: Activity): Boolean {
            return activity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)
        }

        @JvmStatic
        fun escalarImagen(b: Bitmap, widthMax: Double): Bitmap {
            var result = b
            val width = java.lang.Double.valueOf(b.width.toDouble())
            val height = java.lang.Double.valueOf(b.height.toDouble())
            val newWidth = widthMax
            val newHeight: Double
            if (width > newWidth) {
                newHeight = height / width * newWidth
                result = Bitmap.createScaledBitmap(b, newWidth.toInt(), newHeight.toInt(), true)
            }
            return result
        }

        @JvmStatic
        fun comprimirImagenJpeg(b: Bitmap, baos: ByteArrayOutputStream): Bitmap {
            b.compress(Bitmap.CompressFormat.JPEG, 90, baos)
            return b
        }

        @JvmStatic
        fun rotateImg(source: Bitmap, angle: Float): Bitmap {
            val matrix = Matrix()
            matrix.postRotate(angle)
            return Bitmap.createBitmap(source, 0, 0, source.width, source.height, matrix, true)
        }

        @JvmStatic
        @Throws(IOException::class)
        fun rotarImagen(photoPath: String, bitmapGaleria: Bitmap): Bitmap? {
            val ei = ExifInterface(photoPath)
            val orientation = ei.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED)
            var rotatedBitmap: Bitmap? = null
            when (orientation) {
                ExifInterface.ORIENTATION_ROTATE_90 -> rotatedBitmap = rotateImg(bitmapGaleria, 90f)
                ExifInterface.ORIENTATION_ROTATE_180 -> rotatedBitmap = rotateImg(bitmapGaleria, 180f)
                ExifInterface.ORIENTATION_ROTATE_270 -> rotatedBitmap = rotateImg(bitmapGaleria, 270f)
                ExifInterface.ORIENTATION_NORMAL -> rotatedBitmap = rotateImg(bitmapGaleria, 360f)
                else -> rotatedBitmap = bitmapGaleria
            }
            return rotatedBitmap
        }

        //convierte la imagen de bitmap a string base 64
        @JvmStatic
        fun encodeTobase64(image: Bitmap): String {
            val baos = ByteArrayOutputStream()
            image.compress(Bitmap.CompressFormat.PNG, 90, baos)
            val b = baos.toByteArray()
            return Base64.encodeToString(b, Base64.DEFAULT)
        }

        @JvmStatic
        fun checkCameraPermissions(activity: Activity): ArrayList<String> {
            val permissions = ArrayList<String>()
            permissions.add(Manifest.permission.CAMERA)
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            return findUnAskedPermissions(permissions,activity)
        }
        private fun findUnAskedPermissions(wanted: ArrayList<String>, activity: Activity): ArrayList<String> {
            val result = ArrayList<String>()
            for (perm in wanted) {
                if (!hasPermission(perm,activity)) {
                    result.add(perm)
                }
            }
            return result
        }

        @JvmStatic
        fun hasPermission(permission: String,activity: Activity): Boolean {
            if (canAskPermission()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    return activity.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
                }
            }
            return true
        }
        private fun canAskPermission(): Boolean {
            return Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1
        }
    }
}