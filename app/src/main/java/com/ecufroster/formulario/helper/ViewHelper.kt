package com.ecufroster.formulario.helper

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.LocationManager
import android.net.Uri
import android.provider.Settings
import android.text.InputType
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.ecufroster.formulario.BuildConfig
import com.ecufroster.formulario.R
import com.ecufroster.formulario.`interface`.OnDatePickerListener
import com.ecufroster.formulario.`interface`.OnTimerPickerListener
import com.ecufroster.formulario.core.AppController
import com.ecufroster.formulario.helper.UtilidadesHelper.Companion.mensajeAlerta
import com.ecufroster.formulario.helper.UtilidadesHelper.Companion.mensajeConfirmacion
import com.ecufroster.formulario.model.PermisosApp
import java.util.*

object ViewHelper {
    fun deshabilitaInteraccionPantalla(myActivityReference: Activity) {
        myActivityReference.window.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )
    }

    fun habilitaInteraccionPantalla(myActivityReference: Activity) {
        myActivityReference.window.clearFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )
    }

    fun setSpinnerError(spinner: Spinner, error: String) {
        val selectedView = spinner.selectedView
        if (selectedView != null && selectedView is TextView) {
            spinner.requestFocus()
            val selectedTextView = selectedView
            selectedTextView.error = "onApiError" // any name of the onApiError will do
            selectedTextView.setTextColor(Color.RED) //text color in which you want your onApiError message to be displayed
            if (!error.trim { it <= ' ' }.isEmpty()) {
                selectedTextView.text = error // actual onApiError message
            }
            //spinner.performClick(); // to open the spinner list if onApiError is found.
        }
    }

    fun abreWebIntent(url: String?) {
        val uri = Uri.parse(url)
        val intent = Intent(Intent.ACTION_VIEW, uri)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        AppController.mInstance!!.applicationContext.startActivity(intent)
    }

    /*https://stackoverflow.com/questions/1109022/close-hide-the-android-soft-keyboard*/
    fun hideKeyboardInActivity(activity: Activity) {
        val imm = activity.getSystemService(
            Activity.INPUT_METHOD_SERVICE
        ) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun hideKeyboardInFragment(
        context: Context,
        view: View
    ) {
        val imm = context.getSystemService(
            Activity.INPUT_METHOD_SERVICE
        ) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun alertaActualizaApp(context: Context, titulo: String, mensaje: String) {
        mensajeAlerta(context, titulo, mensaje)
            .setPositiveButton("Ir a la PlayStore",
                DialogInterface.OnClickListener { _, _ ->
                    abreWebIntent("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)
                })
            .show()
    }

    fun solicitarPermisoEspecifico(activity: Activity, permission: String, motivoPermiso: String): Boolean { /*SI NO TIENE PERMISO PARA HACER LO REQUERIDO*/
        return if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) { /*CUANDO RECHAZA LA SOLICITUD DE PERMISO*/
                ActivityCompat.requestPermissions(activity, arrayOf(permission), 1)
            }
            else { /*CUANDO HA RECHAZADO LA SOLICITUD DE PERMISO Y MARCÓ LA OPCIÓN DE "No volver a preguntar"*/ /*EN ESTE CASO YA NO SE LE MOSTRARÁ EL DIÁLOGO DE ACEPTAR PERMISO QUE SE MUESTRA POR DEFECTO, DEBEMOS MOSTRAR UN DIÁLOGO PERSONALIZADO*/
                mensajeAlerta(activity, activity.getString(R.string.app_name), motivoPermiso)
                    .setPositiveButton(
                        "Ir a Configuración",
                        DialogInterface.OnClickListener { _, _ ->
                            val intent = Intent(
                                Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.parse("package:" + BuildConfig.APPLICATION_ID)
                            )
                            activity.startActivity(intent)
                        })
                    .show()
            }
            false
        } else {
            true
        }
    }

    /*Muestra calendario con la configuración para elegir fecha de nacimiento*/
    fun showCalendarDateBirth(context: Context, year: Int, month: Int, day: Int, callback: OnDatePickerListener) {
        var year = year
        var month = month
        var day = day
        val calendar = Calendar.getInstance()
        if (year < 1900) {
            year = Calendar.YEAR
            month = Calendar.MONTH
            day = Calendar.DAY_OF_MONTH
        }
        val datePickerDialog =
            DatePickerDialog(context,
                DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                    callback.onSelected(
                        year,
                        month,
                        dayOfMonth
                    )
                }, year, month, day
            )
        datePickerDialog.datePicker.maxDate = System.currentTimeMillis() - 1000
        datePickerDialog.show()
    }

    @SuppressLint("MissingPermission")
    fun llamadaTelefonica(activity: Activity, numeroTelefono: String) {
        if (solicitarPermisoEspecifico(
                activity,
                Manifest.permission.CALL_PHONE,
                "Necesitas otorgar permiso de teléfono a la app desde la opción Ajustes o Configuración para realizar llamadas"
            )
        ) { //Intent iCall = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:0984410809")); /*CON ACTION_DIAL NO SE NECESITA PERMISO DE TELEFONO, YA QUE CON ESTO SE MUESTRA EL PANEL DE LLAMADAS DEL PROPIO TELÉFONO PARA QUE EL USUARIO HAGA LA LLAMADA CON LA APP DE LLAMADA DEL DISPOSITIVO*/
            val iLlamar = Intent(Intent.ACTION_CALL)
            iLlamar.data = Uri.parse("tel:$numeroTelefono")
            activity.startActivity(iLlamar)
        }
    }

    fun revisarServicioUbicacion(activity: Activity): Boolean {
        val manager = activity.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            return false
        }
        return true
    }

    /*Muestra calendario sin bloquear ninguna fecha*/
    fun showCalendarDatePicker(
        context: Context?,
        callback: OnDatePickerListener?
    ) {
        val calendar = Calendar.getInstance()
        val datePickerDialog = DatePickerDialog(
            context!!,
            DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                callback!!.onSelected(
                    year,
                    month,
                    dayOfMonth
                )
            },
            calendar[Calendar.YEAR],
            calendar[Calendar.MONTH],
            calendar[Calendar.DAY_OF_MONTH]
        )
        datePickerDialog.show()
    }

    /*Muestra calendario seteando como fecha mínima la fecha actual del dispositivo y bloqueando las fechas anteriores a la fecha actual*/
    fun showCalendarMinDateCurrentDatePicker(context: Context?, callback: OnDatePickerListener?) {
        val calendar = Calendar.getInstance()
        val datePickerDialog = DatePickerDialog(
            context!!,
            DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                callback!!.onSelected(year, month, dayOfMonth)
            },
            calendar[Calendar.YEAR],
            calendar[Calendar.MONTH],
            calendar[Calendar.DAY_OF_MONTH]
        )
        datePickerDialog.datePicker.minDate = System.currentTimeMillis() - 1000
        datePickerDialog.show()
    }

    fun showClockTimerPicker(context: Context?, callback: OnTimerPickerListener?) {
        val calendar = Calendar.getInstance()
        val timePickerDialog = TimePickerDialog(
            context,
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                callback!!.onSelected(hourOfDay, minute)
            },
            calendar[Calendar.HOUR_OF_DAY],
            calendar[Calendar.MINUTE],
            true
        ) //Al colocar en false se muestra en formato 12 horas y true en formato 24 horas
        timePickerDialog.setTitle("")
        timePickerDialog.show()
    }

    fun showClockTimerPicker(context: Context?, hour: Int, minute: Int, callback: OnTimerPickerListener?) {
        val timePickerDialog = TimePickerDialog(
            context,
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                callback!!.onSelected(hourOfDay, minute)
            },
            hour,
            minute,
            true
        ) //Al colocar en false se muestra en formato 12 horas y true en formato 24 horas
        timePickerDialog.setTitle("")
        timePickerDialog.show()
    }

    @JvmStatic
    fun solicitaPermisosInicial(activity: Activity, listPermission: ArrayList<PermisosApp>) {
        var requierePermiso = false
        val mensaje = StringBuilder("Necesitas darle acceso a:\n")
        for (i in listPermission.indices) {
            if (ContextCompat.checkSelfPermission(activity, listPermission[i].permiso) != PackageManager.PERMISSION_GRANTED) {
                mensaje.append(listPermission[i].descripcion)
                mensaje.append("\n")
                requierePermiso = true
            }
        }
        if (requierePermiso) {
            mensajeConfirmacion(activity, mensaje.toString())
                .setPositiveButton(
                    activity.getString(R.string.dialogos_aceptar),
                    DialogInterface.OnClickListener { _, _ ->
                        solicitaPermisosVarios(activity, listPermission)
                    })
                .setCancelable(false)
                .show()
        }
    }

    fun solicitaPermisosVarios(activity: Activity?, listPermission: ArrayList<PermisosApp>) {
        val REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124
        val listPermissionMissing: MutableList<String> = ArrayList()
        for (i in listPermission.indices) {
            if (ContextCompat.checkSelfPermission(activity!!, listPermission[i].permiso) != PackageManager.PERMISSION_GRANTED) {
                listPermissionMissing.add(listPermission[i].permiso)
            }
        }
        if (listPermissionMissing.size > 0) {
            ActivityCompat.requestPermissions(activity!!, listPermissionMissing.toTypedArray(), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS)
        }
    }

    fun passwordToggle(editPass: EditText, btnVisible: Button, btnNoVisible: Button, mostrar: Boolean ){
        if(editPass.text.toString().isNotEmpty()){
            editPass.inputType = InputType.TYPE_CLASS_TEXT or if( mostrar ) InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD else InputType.TYPE_TEXT_VARIATION_PASSWORD
            btnVisible.visibility = if ( mostrar )  View.GONE else View.VISIBLE
            btnNoVisible.visibility = if ( mostrar )  View.VISIBLE else View.GONE
            editPass.setSelection(editPass.text.toString().length)
        }
    }

    fun passwordToggleWithImage(editPass: EditText, imvVisible: ImageView, imvNoVisible: ImageView, mostrar: Boolean ){
        if(editPass.text.toString().isNotEmpty()){
            editPass.inputType = InputType.TYPE_CLASS_TEXT or if( mostrar ) InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD else InputType.TYPE_TEXT_VARIATION_PASSWORD
            imvVisible.visibility = if ( mostrar )  View.GONE else View.VISIBLE
            imvNoVisible.visibility = if ( mostrar )  View.VISIBLE else View.GONE
            editPass.setSelection(editPass.text.toString().length)
        }
    }
}