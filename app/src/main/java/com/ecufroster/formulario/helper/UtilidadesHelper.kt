package com.ecufroster.formulario.helper

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.app.AlertDialog
import android.content.Context
import android.content.ContextWrapper
import android.content.DialogInterface
import android.content.Intent
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.media.MediaDrm
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.text.InputFilter
import android.text.InputType
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.ecufroster.formulario.R
import com.ecufroster.formulario.core.AppController
import com.ecufroster.formulario.core.Preferencias
import com.ecufroster.formulario.view.activity.LoginActivity
import com.ecufroster.formulario.view.activity.MainActivity
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.SimpleDateFormat
import java.util.*

class UtilidadesHelper {
    companion object {
        private var mensaje: AlertDialog.Builder? = null

        @JvmStatic
        @Suppress("DEPRECATION")
        fun isInternetAvailable(context: Context): Boolean {
            var result = false
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                cm?.run {
                    cm.getNetworkCapabilities(cm.activeNetwork)?.run {
                        result = when {
                            hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                            hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                            hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                            else -> false
                        }
                    }
                }
            } else {
                cm?.run {
                    cm.activeNetworkInfo?.run {
                        if (type == ConnectivityManager.TYPE_WIFI) {
                            result = true
                        } else if (type == ConnectivityManager.TYPE_MOBILE) {
                            result = true
                        }
                    }
                }
            }
            return result
        }

        @JvmStatic
        @SuppressWarnings("deprecation")
        fun getDeviceId(context: Context): String {
            //Solución encontrada en stackorveflow aquí https://stackoverflow.com/a/60194843
            val wideVineUuid = UUID(-0x121074568629b532L, -0x5c37d8232ae2de13L)
            try {
                val wvDrm = MediaDrm(wideVineUuid)
                val wideVineId = wvDrm.getPropertyByteArray(MediaDrm.PROPERTY_DEVICE_UNIQUE_ID)
                return wideVineId.contentToString();
            } catch (e: java.lang.Exception) {
                return wideVineUuid.toString();
            }
        }

        @JvmStatic
        fun getTextHtmlWithLink(cadena: String): String {
            var textoMostrar = ""
            if (cadena.contains("http")) {
                val listTexto = cadena.split(" ").toTypedArray()
                for (texto in listTexto) {
                    if (texto.contains(System.getProperty("line.separator")!!)) {
                        val listTextoSaltoLinea =
                            texto.split(System.getProperty("line.separator")!!).toTypedArray()
                        var aux = 1
                        for (textoSaltoLinea in listTextoSaltoLinea) {
                            textoMostrar = if (textoSaltoLinea.contains("http")) {
                                "$textoMostrar " + insertTagHtmlA(
                                    textoSaltoLinea,
                                    textoSaltoLinea
                                )
                            } else {
                                "$textoMostrar $textoSaltoLinea"
                            }
                            if (aux < listTextoSaltoLinea.size) //Para no agregar un salto de línea innecesario en la última palabra.
                                textoMostrar = "$textoMostrar<br>"
                            aux = aux + 1
                        }
                    } else {
                        textoMostrar = if (texto.contains("http")) {
                            textoMostrar + " " + insertTagHtmlA(texto, texto)
                        } else {
                            "$textoMostrar $texto"
                        }
                    }
                }
            } else {
                textoMostrar = cadena
            }
            return textoMostrar
        }

        @JvmStatic
        fun insertTagHtmlA(texto: String, url: String): String {
            val colorlink: Int =
                ContextCompat.getColor(AppController.mInstance!!.applicationContext, R.color.colorLink)
            return "<font color=$colorlink><a href='$url'>$texto</a></font>"
        }

        // generates a SHA-1 hash for any string
        @JvmStatic
        fun getHash(stringToHash: String): String {
            var digest: MessageDigest? = null
            try {
                digest = MessageDigest.getInstance("SHA-1")
            } catch (e: NoSuchAlgorithmException) {
                e.printStackTrace()
            }
            var result: ByteArray? = null
            try {
                result = digest!!.digest(stringToHash.toByteArray(charset("UTF-8")))
            } catch (e: UnsupportedEncodingException) {
                e.printStackTrace()
            }
            val sb = StringBuilder()
            for (b in result!!) {
                sb.append(String.format("%02X", b))
            }
            return sb.toString()
        }

        @JvmStatic
        fun llamarPantallaPrincipal() {
            val preferencias = Preferencias(AppController.mInstance.applicationContext)
            val startActivity: Intent

            if (preferencias.getPrefSessionUser().isNotEmpty()) {
                startActivity = Intent(AppController.mInstance.applicationContext, MainActivity::class.java)
            } else {
                startActivity = Intent(AppController.mInstance.applicationContext, LoginActivity::class.java)
            }

            startActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            AppController.mInstance.applicationContext.startActivity(startActivity)
        }

        @JvmStatic
        fun cerrarSesionNormal(context: Context) {
            clearAllSharedPreferences(context)
            limpiarBaseDeDatos(context)

            mensajeAlerta(context, context.getString(R.string.app_name), context.getString(R.string.cerrar_sesion_forzada))
                ?.setPositiveButton(
                    context.getString(R.string.dialogos_aceptar),
                    DialogInterface.OnClickListener { _, _ -> llamarPantallaPrincipal() })
                ?.show()
        }

        @JvmStatic
        fun cerrarSesionPush(context: Context) {
            /*
            val preferencias = Preferencias(context)
            //para que funcione bien, primero se debe llamar al Login y de ahi limpiar las preferencias
            if (preferencias.getAccessToken() != "") {
                val startActivity: Intent = Intent(context, LoginActivity::class.java)
                startActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                context.startActivity(startActivity)

                clearAllSharedPreferences(context)
                limpiarBaseDeDatos(context)
            }
             */
        }

        @JvmStatic
        fun clearAllSharedPreferences(context: Context) {
            val preferencias = Preferencias(context)
            preferencias.deleteAllPreferences()
        }

        fun limpiarBaseDeDatos(context: Context){
            /*
            val notificacionRepository = NotificacionRepository.getInstance(AppDatabase.getInstance(context).notificacionDao())
            val asistenciaRepository = AsistenciaRepository.getInstance(AppDatabase.getInstance(context).asistenciaDao())
            val categoriaRepositorio = CategoriaRepository.getInstance(AppDatabase.getInstance(context).categoriaDao())
            val paisRepositorio = PaisRepository.getInstance(AppDatabase.getInstance(context).paisDao())
            val perfilRepository = PerfilRepository.getInstance(AppDatabase.getInstance(context).perfilDao())
            val preguntasFrecuentesRepository = PreguntaFrecuenteRepository.getInstance(AppDatabase.getInstance(context).preguntaFrecuenteDao())

            val servicioRepository = ServicioRepository.getInstance(AppDatabase.getInstance(context).servicioDao())
            val grupoServicioRepository = GrupoServicioRepository.getInstance(AppDatabase.getInstance(context).grupoServicioDao())
            val paqueteServicioRepository = PaqueteServicioRepository.getInstance(AppDatabase.getInstance(context).paqueteServicioDao())

            notificacionRepository.deleteAll()
            asistenciaRepository.deleteAll()
            categoriaRepositorio.deleteAll()
            //paisRepositorio.deleteAll()
            perfilRepository.deleteAll()
            preguntasFrecuentesRepository.deleteAll()
            servicioRepository.deleteAll()
            grupoServicioRepository.deleteAll()
            paqueteServicioRepository.deleteAll()
             */
        }

        @JvmStatic
        fun isValidEmail(target: CharSequence?): Boolean {
            return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }

        @JvmStatic
        fun isValidNull(target: String?): String {
            return target ?: ""
        }

        @JvmStatic
        fun mensajeAlerta(contexto: Context, titulo: String, alerta: String): AlertDialog.Builder {
            inicializaAlertDialog(contexto)
            mensaje!!.setTitle(titulo)
            mensaje!!.setIcon(R.drawable.ic_alert_error)
            mensaje!!.setMessage(alerta)
            mensaje!!.setPositiveButton(
                R.string.dialogos_aceptar,
                DialogInterface.OnClickListener { _, _ ->
                    // FIRE ZE MISSILES!
                })
            return mensaje as AlertDialog.Builder
        }

        @JvmStatic
        fun mensajeAlertaReintentar(contexto: Context, titulo: String, alerta: String): AlertDialog.Builder {
            inicializaAlertDialog(contexto)
            mensaje!!.setTitle(titulo)
            mensaje!!.setIcon(R.drawable.ic_alert_error)
            mensaje!!.setMessage(alerta)
            mensaje!!.setPositiveButton(
                R.string.btn_reintentar,
                DialogInterface.OnClickListener { dialog, id ->
                    // FIRE ZE MISSILES!
                })
            return mensaje as AlertDialog.Builder
        }

        @JvmStatic
        fun mensajeAlerta(contexto: Context?, alerta: String?): AlertDialog.Builder? {
            mensaje!!.setTitle(AppController.mInstance!!.applicationContext.getString(R.string.app_name))
            mensaje!!.setIcon(R.drawable.ic_alert_error)
            mensaje!!.setMessage(alerta)
            mensaje!!.setPositiveButton(
                R.string.btn_aceptar,
                DialogInterface.OnClickListener { dialog, id ->
                    // FIRE ZE MISSILES!
                })
            return mensaje
        }

        @JvmStatic
        fun mensajeAlerta(contexto: Context?, titulo: String?, alerta: String?, icono: Int): AlertDialog.Builder? {
            inicializaAlertDialog(contexto)
            mensaje!!.setTitle(titulo)
            mensaje!!.setIcon(icono)
            mensaje!!.setMessage(alerta)
            mensaje!!.setPositiveButton(
                R.string.btn_aceptar,
                DialogInterface.OnClickListener { dialog, id ->
                    // FIRE ZE MISSILES!
                })
            return mensaje
        }

        @JvmStatic
        fun mensajeConfirmacion(contexto: Context, alerta: String): AlertDialog.Builder {
            inicializaAlertDialog(contexto)
            mensaje!!.setTitle(R.string.app_name)
            mensaje!!.setMessage(alerta)
            mensaje!!.setPositiveButton(
                R.string.btn_ok,
                DialogInterface.OnClickListener { dialog, id ->
                    // FIRE ZE MISSILES!
                })
            return mensaje as AlertDialog.Builder
        }

        @JvmStatic
        fun mensajeConfirmacion(contexto: Context?, titulo: String?, alerta: String?): AlertDialog.Builder? {
            inicializaAlertDialog(contexto)
            mensaje!!.setTitle(titulo)
            mensaje!!.setMessage(alerta)
            mensaje!!.setPositiveButton(
                R.string.btn_ok,
                DialogInterface.OnClickListener { dialog, id ->
                    // FIRE ZE MISSILES!
                })
            return mensaje
        }

        @JvmStatic
        fun mensajeConfirmacionCancelacion(contexto: Context?, titulo: String?, texto: String?): AlertDialog.Builder? {
            inicializaAlertDialog(contexto)
            mensaje!!.setTitle(titulo)
            mensaje!!.setMessage(texto)
            mensaje!!.setPositiveButton(
                R.string.dialogos_aceptar,
                DialogInterface.OnClickListener { dialog, id ->
                    // FIRE ZE MISSILES!
                })
            mensaje!!.setNegativeButton(
                R.string.dialogos_cancelar,
                DialogInterface.OnClickListener { dialog, which -> })
            return mensaje
        }

        @JvmStatic
        fun mensajeConfirmacionView(contexto: Context?, titulo: String?, view: View?): AlertDialog.Builder? {
            inicializaAlertDialog(contexto)
            mensaje!!.setTitle(titulo)
            mensaje!!.setView(view)
            mensaje!!.setPositiveButton(
                R.string.btn_ok,
                DialogInterface.OnClickListener { dialog, id ->
                    // FIRE ZE MISSILES!
                })
            return mensaje
        }

        @JvmStatic
        fun mensajeIndicador(contexto: Context, titulo: String, msj: String): AlertDialog.Builder {
            inicializaAlertDialog(contexto)
            mensaje!!.setTitle(titulo)
            mensaje!!.setCancelable(false)
            mensaje!!.setMessage(msj)
            mensaje!!.setPositiveButton(
                R.string.btn_aceptar,
                DialogInterface.OnClickListener { dialog, id ->
                    // FIRE ZE MISSILES!
                })
            return mensaje as AlertDialog.Builder
        }

        fun mensajePermisos(contexto: Context?): AlertDialog.Builder? {
            inicializaAlertDialog(contexto)
            mensaje!!.setTitle(R.string.permisos_necesarios_titulo)
            mensaje!!.setMessage(R.string.permisos_necesarios)
            mensaje!!.setPositiveButton(R.string.dialogos_aceptar) { _, _ -> }
            mensaje!!.setNegativeButton(R.string.dialogos_cancelar) { _, _ -> }
            return mensaje
        }

        @JvmStatic
        fun inicializaAlertDialog(contexto: Context?) { //Control de Versiones en Estilos del Dialogo
            mensaje = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                AlertDialog.Builder(contexto, R.style.AlertDialogCustomTheme)
            } else {
                AlertDialog.Builder(contexto)
            }
        }

        @JvmStatic
        fun letraCapital(string: String?): String? {
            var string = string
            if (string != null && string.length > 0) string =
                string[0].toString().toUpperCase() + string.substring(1).toLowerCase()
            return string
        }

        @JvmStatic
        fun switchClave(editText: EditText, imageView: ImageView, imageView2: ImageView, bandera: Boolean) {
            if (bandera) { //Muestra clave
                if (editText.text.toString().trim { it <= ' ' }.isNotEmpty()) {
                    editText.inputType =
                        InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                    imageView.visibility = View.GONE
                    imageView2.visibility = View.VISIBLE
                    editText.setSelection(editText.text.toString().length)
                    filtroCaracteres(editText)
                }
            } else { //OcultaClave
                if (editText.text.toString().trim { it <= ' ' }.isNotEmpty()) {
                    editText.inputType =
                        InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                    imageView.visibility = View.VISIBLE
                    imageView2.visibility = View.GONE
                    editText.setSelection(editText.text.toString().length)
                    filtroCaracteres(editText)
                }
            }
        }

        @JvmStatic
        fun filtroCaracteres(editText: EditText) {
            val filter = InputFilter { source, start, end, dest, dstart, dend ->
                for (i in start until end) {
                    if (!Character.isLetterOrDigit(source[i]) || Character.toString(
                            source[i]
                        ) == "ñ" || Character.toString(source[i]) == "Ñ"
                    ) {
                        return@InputFilter ""
                    }
                }
                null
            }
            editText.filters = arrayOf(filter, InputFilter.LengthFilter(20))
        }

        @JvmStatic
        fun obtenerAnioDispositivo(): Int {
            val calendar = Calendar.getInstance()
            return calendar[Calendar.YEAR]
        }

        @JvmStatic
        fun obtenerMesDispositivo(): Int {
            val calendar = Calendar.getInstance()
            return calendar[Calendar.MONTH] + 1
        }

        @JvmStatic
        val stringUUID: String
            get() {
                val uuid = UUID.randomUUID()
                return uuid.toString()
            }

        /*A pesar de ser una variable string, debe ser solo dígitos numéricos por pedido de zonapagos*/
        @JvmStatic
        fun uidPagoZonaPago(): String {
            val milisegundos = Calendar.getInstance().timeInMillis.toString()
            return milisegundos.substring(milisegundos.length - 5) + getDeviceId(AppController.mInstance!!.applicationContext).substring(0, 4)
            /*Los 5 últimos dígitos del tiempo en milisegundos concatenado con los 4 primeros dígitos del imei del dispositivo*/
        }

        @JvmStatic
        fun getTiempoHace(fecha: String, horas: String): String {
            val date = Date()
            val fechaDividida = fecha.split("-").toTypedArray()
            val horaDividida = horas.split(":").toTypedArray()
            val dia: Int
            val mes: Int
            val anio: Int
            val diaTelefono: Int
            val mesTelefono: Int
            val anioTelefono: Int
            val diaDiferencia: Int
            val mesDiferencia: Int
            val anioDiferencia: Int
            val hora: Int
            val minutos: Int
            val segundos: Int
            val horaTelefono: Int
            val minutoTelefono: Int
            val segundoTelefono: Int
            val horaDiferencia: Int
            val minutoDiferencia: Int
            val segundoDiferencia: Int
            anio = fechaDividida[0].toInt()
            mes = fechaDividida[1].toInt()
            dia = fechaDividida[2].toInt()
            hora = horaDividida[0].toInt()
            minutos = horaDividida[1].toInt()
            segundos = horaDividida[2].toInt()
            diaTelefono = date.date
            mesTelefono = date.month + 1
            anioTelefono = date.year + 1900
            horaTelefono = date.hours
            minutoTelefono = date.minutes
            segundoTelefono = date.seconds
            anioDiferencia = anioTelefono - anio
            mesDiferencia = mesTelefono - mes
            diaDiferencia = diaTelefono - dia
            horaDiferencia = horaTelefono - hora
            minutoDiferencia = minutoTelefono - minutos
            segundoDiferencia = segundoTelefono - segundos
            if (anioDiferencia == 1) return "solicitada el $fecha $horas"
            if (anioDiferencia > 1) return "solicitada el $fecha $horas"
            if (mesDiferencia == 1) return "solicitada el $fecha $horas"
            if (mesDiferencia > 1) return "solicitada el $fecha $horas"
            if (diaDiferencia == 1) return "solicitada hace 1 día atrás a las $horas"
            if (diaDiferencia > 1 && diaDiferencia <= 7) return "solicitada hace $diaDiferencia días atrás a las $horas"
            if (diaDiferencia > 7) return "solicitada el $fecha $horas"
            if (horaDiferencia == 1) return "solicitada hace 1 hora"
            if (horaDiferencia > 1) return "solicitada hace $horaDiferencia horas atrás"
            if (minutoDiferencia == 1) return "solicitada hace 1 minuto"
            return if (minutoDiferencia > 1) "solicitada hace $minutoDiferencia minutos atrás" else "solicitada hace menos de un minuto"
        }

        @JvmStatic
        fun textoFecha(fecha: String): String { //ano-mes-dia --> asi me viene
            val fechaDividida = fecha.split("-").toTypedArray()
            return fechaDividida[2] + " de " + getNombreMes(
                fechaDividida[1].toInt()
            ) + " del " + fechaDividida[0]
        }

        @JvmStatic
        fun getNombreMes(mes: Int): String {
            var nombreMes = ""
            nombreMes = when (mes) {
                1 -> "Enero"
                2 -> "Febrero"
                3 -> "Marzo"
                4 -> "Abril"
                5 -> "Mayo"
                6 -> "Junio"
                7 -> "Julio"
                8 -> "Agosto"
                9 -> "Septiembre"
                10 -> "Octubre"
                11 -> "Noviembre"
                else -> "Diciembre"
            }
            return nombreMes
        }

        @JvmStatic
        fun guardarImagenDescargadaConPicasso(context: Context, imageDir: String, imageName: String): Target {
            Log.d("picassoImageTarget", " picassoImageTarget")
            val cw = ContextWrapper(context)
            val directory = cw.getDir(
                imageDir,
                Context.MODE_PRIVATE
            ) // path to /data/data/com.your.app.package.path/imageDir/imageName.jpg
            return object : Target {
                override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom?) {
                    Thread(Runnable {
                        val myImageFile = File(
                            directory,
                            imageName
                        ) // Create image file
                        var fos: FileOutputStream? = null
                        try {
                            fos = FileOutputStream(myImageFile)
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos)
                        } catch (e: IOException) {
                            e.printStackTrace()
                        } finally {
                            try {
                                fos!!.close()
                            } catch (e: IOException) {
                                e.printStackTrace()
                            }
                        }
                        Log.i(
                            "image",
                            "image saved to >>>" + myImageFile.absolutePath
                        )
                    }).start()
                }

                override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                    if (placeHolderDrawable != null) {
                    }
                }

                override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {}
            }
        }

        fun isMyServiceRunning(context: Context, serviceName: String): Boolean {
            val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            for (service in manager
                .getRunningServices(Int.MAX_VALUE)) {
                if (serviceName == service.service
                        .className
                ) {
                    return true
                }
            }
            return false
        }

        @JvmStatic
        fun pixelEnDP(dp: Int): Int {
            val density: Float = AppController.mInstance!!.applicationContext.resources.displayMetrics.density
            return (dp * density).toInt()
        }

        @JvmStatic
        fun getHeightByFactorAndWidth(width: Int, factor: Float): Int {
            return (width * factor).toInt()
        }

        @JvmStatic
        val widthDevice: Int
            get() = Resources.getSystem().displayMetrics.widthPixels

        @JvmStatic
        val heightDevice: Int
            get() = Resources.getSystem().displayMetrics.heightPixels

        fun getColorHexadecimalByColorName(colorName : String) : String{
            return when(colorName.toUpperCase()){
                "AMARILLO" -> "#FFFF00"
                "ANARANJADO", "NARANJA" -> "#EF7F1A"
                "AZUL" -> "#0000FF"
                "BEIGE" -> "#E8C39E"
                "BLANCO" -> "#FFFFFF"
                "CAFE" -> "#E8C39E"
                "CREMA" -> "#FFF0C9"
                "GRIS" -> "#7F7F7F"
                "NEGRO" -> "#000000"
                "PLATA", "PLATEADO" -> "#C0C0C0"
                "ROJO" -> "#FF0000"
                "VERDE" -> "#009E00"
                else -> "#000000"
            }
        }

        @SuppressLint("SimpleDateFormat")
        fun dateTimeCurrentFormatStandar(): String{
            return SimpleDateFormat("ddMMyyyy_HHmmss").format(Date())
        }

        @SuppressLint("SimpleDateFormat")
        fun getDateCurrentFormatStandar(): String{
            return SimpleDateFormat("yyyy-MM-dd").format(Date())
        }

        @SuppressLint("SimpleDateFormat")
        fun getTimeCurrentFormatStandar(): String{
            return SimpleDateFormat("HH:mm:ss").format(Date())
        }
    }
}