package com.ecufroster.formulario.`interface`

interface OnTimerPickerListener {
    fun onSelected(hourOfDay: Int, minute: Int)
}