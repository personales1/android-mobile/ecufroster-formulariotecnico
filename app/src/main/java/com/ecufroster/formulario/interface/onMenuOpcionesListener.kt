package com.ecufroster.formulario.`interface`

import com.ecufroster.formulario.model.MenuOpciones

interface onMenuOpcionesListener {
    fun seleccionarOpcion(menuOpciones: MenuOpciones)
}