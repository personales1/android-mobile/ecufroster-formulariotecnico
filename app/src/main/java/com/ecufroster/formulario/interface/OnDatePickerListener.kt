package com.ecufroster.formulario.`interface`

interface OnDatePickerListener {
    fun onSelected(year: Int, monthOfYear: Int, dayOfMonth: Int)
}