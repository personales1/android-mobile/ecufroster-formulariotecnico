package com.ecufroster.formulario.`interface`

import com.ecufroster.formulario.retrofit.error.ServiceError

interface OnApiResponse<T> {
    fun onSuccess(response: T) /*Exito 200-399*/
    fun onError(error: ServiceError) /*Error solicitud 400-499 excepto 401 ni 410*/
    fun onUpdateApp(error: ServiceError) /*Error solicitud 410*/
    fun onUnAuthenticated(error: ServiceError) /*Error solicitud 401*/
    fun onErrorException(error: Exception) /*Error solicitud 401*/
    fun onUnknownError() /*Cuando es un error y no se ha parseado el mensaje de error desde el server*/
    fun onFailure(throwable: Throwable) /*Error server >=500*/
    fun onWithoutInternet()
}