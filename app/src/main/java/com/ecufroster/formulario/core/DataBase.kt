package com.ecufroster.formulario.core

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.ecufroster.formulario.BuildConfig
import com.ecufroster.formulario.R
import com.ecufroster.formulario.core.AppController.Companion.TAG
import javax.xml.parsers.DocumentBuilderFactory

const val DATABASE_NAME = "ecufroser"
const val DATABASE_VERSION = 2

class DataBase(private var context: Context?) {
    private var openHelper: OpenHelper? = null
    private var db: SQLiteDatabase? = null

    init {
        openHelper = OpenHelper(this.context)
        db = openHelper!!.writableDatabase
    }

    fun open() {
        if (!db!!.isOpen) {
            db = openHelper!!.writableDatabase
        }
    }

    fun insert(tableName: String, pairValues: ContentValues?): Long {
        Log.i(TAG, "Insertando registro en la tabla $tableName")
        val rowId = db!!.insertOrThrow(tableName, null, pairValues)
        if (BuildConfig.DEBUG) {
            Log.i(
                TAG, "************************* Insertando registro en la tabla " + tableName
                        + ", id = " + rowId
            )
        }
        return rowId
    }

    fun insertNoDuplicate(tableName: String, pairValues: ContentValues?): Long {
        Log.i(TAG, "Insertando registro en la tabla $tableName")
        val rowId =
            db!!.insertWithOnConflict(tableName, null, pairValues, SQLiteDatabase.CONFLICT_REPLACE)
        if (BuildConfig.DEBUG) {
            Log.i(
                TAG, "************************* Insertando registro en la tabla " + tableName
                        + ", id = " + rowId
            )
        }
        return rowId
    }

    fun insertString(tableName: String, query: String?) {
        if (BuildConfig.DEBUG) {
            Log.i(TAG, "Insertando registro en la tabla $tableName")
        }
        db!!.execSQL(query)
    }

    fun close() {
        db!!.close()
        openHelper!!.close()
    }

    fun update(tableName: String, pairValues: ContentValues?, where: String) {
        db!!.update(tableName, pairValues, where, null)
        if (BuildConfig.DEBUG) {
            Log.i(
                TAG,
                "************************* Actualizando registro en la tabla $tableName , condicion$where"
            )
        }
    }

    fun updateAll(tableName: String, pairValues: ContentValues?): Long {
        if (BuildConfig.DEBUG) {
            Log.i(TAG, "Actualizando registro en la tabla $tableName")
        }
        return db!!.update(tableName, pairValues, null, null).toLong()
    }

    fun delete(tableName: String, whereClause: String, whereArgs: Array<String?>?) {
        db!!.delete(tableName, whereClause, whereArgs)
        if (BuildConfig.DEBUG) {
            Log.i(
                TAG,
                "************************* Eliminando registros en la tabla $tableName $whereClause"
            )
        }
    }

    fun rawQuery(query: String?, selectionArgs: Array<String?>?): Cursor? {
        return db!!.rawQuery(query, selectionArgs)
    }

    fun query(
        tableName: String?, columnsName: Array<String?>?,
        selection: String?, selectionArgs: Array<String?>?, groupBy: String?,
        having: String?, orderBy: String?
    ): Cursor? {
        return db!!.query(
            tableName, columnsName, selection, selectionArgs,
            groupBy, having, orderBy
        )
    }

    private class OpenHelper internal constructor(private val context: Context?) :
        SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
        override fun onCreate(db: SQLiteDatabase) {
            var s: String
            try {
                val `in` = context!!.resources.openRawResource(R.raw.sql)
                val builder = DocumentBuilderFactory.newInstance().newDocumentBuilder()
                val doc = builder.parse(`in`, null)
                val statements = doc.getElementsByTagName("statement")
                for (i in 0 until statements.length) {
                    s = statements.item(i).childNodes.item(0).nodeValue
                    db.execSQL(s)
                }
            } catch (t: Throwable) {
                if (BuildConfig.DEBUG) {
                    Log.e(TAG, t.toString())
                }
            }
        }

        override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
            var c: Cursor
            var n: Int
            var aux: Int

            if (newVersion > oldVersion){
                onCreate(db)

                c = db.rawQuery("PRAGMA table_info($Db_TABLE_FORMULARIO_INFORMACION_CABINET)", null);
                n = c.getColumnIndexOrThrow("name");
                aux = 0;
                if (c.moveToFirst()) {
                    do {
                        if (c.getString(n) == "anio_importacion") {
                            aux = 1;
                        }
                    } while (c.moveToNext());
                    if (aux == 0) {
                        db.execSQL("alter table $Db_TABLE_FORMULARIO_INFORMACION_CABINET add anio_importacion TEXT not null default ''");
                    }
                }
            }
        }
    }
}