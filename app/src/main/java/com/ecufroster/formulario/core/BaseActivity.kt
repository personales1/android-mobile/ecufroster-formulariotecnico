package com.ecufroster.formulario.core

import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.ecufroster.formulario.R
import com.ecufroster.formulario.helper.UtilidadesHelper
import com.ecufroster.formulario.helper.ViewHelper
import com.ecufroster.formulario.retrofit.error.ServiceError
import java.util.*

open class BaseActivity: AppCompatActivity() {
    lateinit var contexto : Context
    lateinit var preferencias : Preferencias
    lateinit var toolbar: androidx.appcompat.widget.Toolbar
    lateinit var tvTituloToolbar: TextView
    lateinit var imvToolbar: ImageView
    var contenedorProgressBar: RelativeLayout? = null
    lateinit var contenedorProgressbarDescriptivo: RelativeLayout
    lateinit var contenedorNoHayInternet: RelativeLayout
    lateinit var contenedorNoHayData: RelativeLayout
    lateinit var imvNoData: ImageView
    lateinit var progressBar: ProgressBar
    lateinit var progressBarScrollInfinity: ProgressBar
    lateinit var tvMensajeCarga: TextView
    lateinit var tvMensajeNoData: TextView

    protected fun onCreate(savedInstanceState: Bundle?, layout: Int, con: Context) {
        super.onCreate(savedInstanceState)
        setContentView(layout)
        contexto = con
        preferencias = Preferencias(con)
        //setColorBarraEstado()
    }

    //set color background toolbar, si no viene el color en el paquete va a coger la preferencia que viene en R.color.colorPrimary
    fun setColorPrimary(idToolbar: Int){
        toolbar = findViewById<View>(idToolbar) as Toolbar
        if(preferencias.getPrefColorPrimary().isNotEmpty()){
            toolbar.setBackgroundDrawable( ColorDrawable(Color.parseColor(preferencias.getPrefColorPrimary())))
        }else{
            toolbar.setBackgroundColor(resources.getColor(R.color.colorPrimary))
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    protected fun setColorBarraEstado() {
        if (Build.VERSION.SDK_INT >= 21) {
            window.statusBarColor =
                ContextCompat.getColor(this,R.color.colorPrimaryDark) //barra de estado advance
        }
    }

    protected open fun inicializeToolbar(idToolbar: Int, textToolbar: String, showIcon: Boolean) {
        toolbar = findViewById<View>(idToolbar) as androidx.appcompat.widget.Toolbar
        //setColorPrimary(idToolbar)
        tvTituloToolbar = findViewById<View>(R.id.tvTituloToolbar) as TextView
        tvTituloToolbar.text = textToolbar
        setSupportActionBar(toolbar)
        Objects.requireNonNull(supportActionBar)!!.setDisplayHomeAsUpEnabled(showIcon)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
    }

    protected open fun configToolbar() {
        inicializeToolbar(R.id.toolbar, "", true)
        configToolbarIcon()
    }

    protected open fun configToolbar(toolbarTittle: String) {
        inicializeToolbar(R.id.toolbar, toolbarTittle, true)
        configToolbarIcon()
    }

    protected open fun configToolbar(toolbarId: Int, toolbarTittle: String) {
        inicializeToolbar(toolbarId, toolbarTittle, true)
        configToolbarIcon()
    }

    protected open fun configToolbar(toolbarId: Int, toolbarTittle: String, displayShow: Boolean) {
        inicializeToolbar(toolbarId, toolbarTittle, displayShow)
        if(displayShow){
            configToolbarIcon()
        }
    }

    protected open fun configToolbarFormularios(toolbarId: Int, toolbarTittle: String) {
        inicializeToolbar(toolbarId, toolbarTittle, true)
        toolbar.setNavigationIcon(R.drawable.ic_menu)
    }

    protected open fun configToolbarIcon() {
        toolbar.setNavigationIcon(R.drawable.ic_saeta_izquierda)
    }

    protected open fun setTittleToolbar(toolbarTittle: String) {
        tvTituloToolbar.text = toolbarTittle
    }

    protected open fun configProgressBar(progressBarId: Int) {
        contenedorProgressBar = findViewById<View>(progressBarId) as RelativeLayout
    }

    protected open fun configProgressBarSinFondo(progressBarId: Int) {
        progressBar = findViewById<View>(progressBarId) as ProgressBar
    }

    protected open fun configProgressBarDescriptivo(contenedorId: Int) {
        contenedorProgressbarDescriptivo = findViewById<View>(contenedorId) as RelativeLayout
        tvMensajeCarga = findViewById<View>(R.id.tvMensajeCarga) as TextView
    }

    protected open fun configProgressBarScrollInfinity(progressBarId: Int) {
        progressBarScrollInfinity = findViewById<View>(progressBarId) as ProgressBar
    }

    protected open fun configNoHayInternet(id: Int) {
        contenedorNoHayInternet = findViewById<View>(id) as RelativeLayout
    }

    protected open fun configNoHayDatos(idContenedor: Int) {
        contenedorNoHayData = findViewById<View>(idContenedor) as RelativeLayout
    }

    protected open fun configNoHayDatos(idContenedor: Int, idTexto: Int) {
        contenedorNoHayData = findViewById<View>(idContenedor) as RelativeLayout
        tvMensajeNoData = findViewById<View>(idTexto) as TextView
    }

    protected open fun configNoHayDatos(idContenedor: Int, idImagen: Int, idTexto: Int) {
        contenedorNoHayData = findViewById<View>(idContenedor) as RelativeLayout
        imvNoData = findViewById<View>(idImagen) as ImageView
        tvMensajeNoData = findViewById<View>(idTexto) as TextView
    }

    open fun showProgressBar() {
        if (contenedorProgressBar != null) {
            contenedorProgressBar!!.visibility = View.VISIBLE
        }
        ViewHelper.deshabilitaInteraccionPantalla(this)
    }

    open fun hideProgressBar() {
        if (contenedorProgressBar != null) {
            contenedorProgressBar!!.visibility = View.GONE
        }
        ViewHelper.habilitaInteraccionPantalla(this)
    }

    open fun showProgressBarSinFondo() {
        progressBar.visibility = View.VISIBLE
    }

    open fun hideProgressBarSinFondo() {
        progressBar.visibility = View.GONE
    }

    open fun showProgressBarDescriptivo(descripcion: String?) {
        contenedorProgressbarDescriptivo.visibility = View.VISIBLE
        tvMensajeCarga.text = descripcion
    }

    open fun hideProgressBarDescriptivo() {
        contenedorProgressbarDescriptivo.visibility = View.GONE
    }

    open fun showProgressBarScrollInfinity() {
        progressBarScrollInfinity.visibility = View.VISIBLE
    }

    open fun hideProgressBarScrollInfinity() {
        progressBarScrollInfinity.visibility = View.GONE
    }


    open fun showNoHayInternet() {
        contenedorNoHayInternet.visibility = View.VISIBLE
    }

    open fun hideNoHayInternet() {
        contenedorNoHayInternet.visibility = View.GONE
    }

    protected open fun showNoData() {
        contenedorNoHayData.visibility = View.VISIBLE
    }

    protected open fun showNoData(texto: String?) {
        contenedorNoHayData.visibility = View.VISIBLE
        tvMensajeNoData.text = texto
    }

    protected open fun showNoData(imagen: Int, texto: String?) {
        contenedorNoHayData.visibility = View.VISIBLE
        imvNoData.setImageDrawable( ContextCompat.getDrawable(contexto,imagen))
        tvMensajeNoData.text = texto
    }

    protected open fun hideNoData() {
        contenedorNoHayData.visibility = View.GONE
    }

    /****** mensajes de respuestas de Retrofit  */
    protected open fun mostrarMensajeErrorServidor(context: Context) {
        if (!(context as Activity).isFinishing) {
            UtilidadesHelper.mensajeAlerta(context, getString(R.string.error_server_title), getString(R.string.error_server_message, getString(R.string.app_name))).show()
        }
    }

    protected open fun mostrarMensajeErrorInternet(context: Context) {
        if (!(context as Activity).isFinishing) {
            UtilidadesHelper.mensajeAlerta(context, getString(R.string.error_internet_title), getString(R.string.error_internet_message)).show()
        }
    }

    protected open fun mostrarMensajeErrorException(context: Context, e: Exception) {
        if (!(context as Activity).isFinishing) {
            UtilidadesHelper.mensajeAlerta(context, getString(R.string.title_advertencia), e.message, R.drawable.ic_alert_error)!!.show()
        }
    }

    protected open fun mostrarMensajeErrorThrowable(context: Context, t: Throwable) {
        if (!(context as Activity).isFinishing) {
            UtilidadesHelper.mensajeAlerta(
                context,
                getString(R.string.error_server_title),
                getString(R.string.error_server_message, getString(R.string.app_name)) + "\n" + t.message,
                R.drawable.ic_alert_error
            )!!.show()
        }
    }

    protected open fun mostrarMensajeErrorResponse(context: Context, error: ServiceError) {
        if (!(context as Activity).isFinishing) {
            try {
                UtilidadesHelper.mensajeAlerta(context, error.noticias.titulo, error.noticias.mensaje).show()
            } catch (e: Exception) {
                UtilidadesHelper.mensajeAlerta(context, getString(R.string.error_server_title), getString(R.string.error_server_message, getString(R.string.app_name))).show()
            }
        }
    }

    protected open fun mostrarMensajeErrorResponseReintentar(context: Context, error: ServiceError) {
        if (!(context as Activity).isFinishing) {
            try {
                UtilidadesHelper.mensajeAlertaReintentar(context, error.noticias.titulo, error.noticias.mensaje).show()
            } catch (e: Exception) {
                UtilidadesHelper.mensajeAlertaReintentar(context, getString(R.string.error_server_title), getString(R.string.error_server_message, getString(R.string.app_name))).show()
            }
        }
    }
}