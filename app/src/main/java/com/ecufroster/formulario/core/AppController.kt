package com.ecufroster.formulario.core

import androidx.multidex.MultiDexApplication

class AppController : MultiDexApplication() {

    companion object {
        @JvmField val TAG = AppController::class.java.simpleName
        lateinit var mInstance: AppController
    }

    override fun onCreate() {
        super.onCreate()
        mInstance = this
    }

    @Synchronized
    fun getInstance(): AppController? {
        return mInstance
    }
}