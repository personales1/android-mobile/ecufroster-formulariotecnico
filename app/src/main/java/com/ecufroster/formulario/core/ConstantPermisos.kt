package com.ecufroster.formulario.core

class ConstantPermisos {
    object Request{
        const val REQUEST_CODE_MULTIPLE_PERMISSION = 100
        const val REQUEST_CODE_READ_PHONE_STATE = 101
        const val REQUEST_CODE_CALL_PHONE = 102
        const val REQUEST_CODE_CAMERA = 103
        const val REQUEST_CODE_RECORD_AUDIO = 104
        const val REQUEST_CODE_READ_EXTERNAL_STORAGE = 105
        const val REQUEST_CODE_WRITE_EXTERNAL_STORAGE = 106
        const val REQUEST_CODE_ACCESS_FINE_LOCATION = 107
        const val REQUEST_CODE_ACCESS_COARSE_LOCATION = 108
    }

    object Name{
        const val TELEFONO = "TELÉFONO"
        const val CAMARA = "CÁMARA"
        const val MICROFONO = "MICRÓFONO"
        const val ALMACENAMIENTO = "ALMACENAMIENTO"
        const val UBICACION = "UBICACIÓN"
    }

    object Reason{
        const val TELEFONO_NOTIFICACION = "recibir notificaciones."
        const val TELEFONO_LLAMADA = "realizar llamadas."
        const val CAMARA_VIDEOLLAMADA = "realizar la videollamada."
        const val CAMARA_PERFIL_FOTO = "cambiar tu foto de perfil."
        const val CAMARA_FOTO = "tomar un foto."
        const val MICROFONO_VIDEOLLAMADA = "realizar la videollamada."
        const val ALMACENAMIENTO_GALERIA_ACCESO = "acceder a tu galería de imágenes."
        const val ALMACENAMIENTO_GALERIA_GUARDAR = "almacenar la foto de perfil en tu galería."
        const val ALMACENAMIENTO_GALERIA_GUARDAR_FIRMA = "almacenar la firma en tu galería."
        const val UBICACION_ACTUAL = "obtener tu ubicación actual."
        const val UBICACION_CUPONES = "poder mostrar cupones cercanos a ti."
    }
}