package com.ecufroster.formulario.core

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

class Preferencias constructor(context: Context) {
    private var preferencias: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    private var editor: SharedPreferences.Editor? = null

    private val PREF_COLOR_PRIMARY = AppController.mInstance.packageName + ".color_primary"
    private val PREF_SESSION_USER = AppController.mInstance.packageName + ".session_usuario"
    private val PREF_PERFIL_ID = AppController.mInstance.packageName + ".perfil.id"
    private val PREF_PERFIL_NOMBRE = AppController.mInstance.packageName + ".perfil.nombre"
    private val PREF_PERFIL_IDENTIFICACION = AppController.mInstance.packageName + ".perfil.identificacion"
    private val PREF_PERFIL_TELEFONO = AppController.mInstance.packageName + ".perfil.telefono"
    private val PREF_PERFIL_CORREO = AppController.mInstance.packageName + ".perfil.correo"
    private val PREF_FIRMA_USUARIO = AppController.mInstance.packageName + ".firma.usuario"

    fun deleteAllPreferences() {
        preferencias.edit().clear().apply()
    }

    fun getPrefColorPrimary(): String {
        return preferencias.getString(PREF_COLOR_PRIMARY, "").toString()
    }
    fun setPrefColorPrimary(value: String) {
        editor = preferencias.edit()
        editor!!.putString(PREF_COLOR_PRIMARY, value)
        editor!!.apply()
    }

    fun getPrefSessionUser(): String {
        return preferencias.getString(PREF_SESSION_USER, "").toString()
    }
    fun setPrefSessionUser(session: String) {
        editor = preferencias.edit()
        editor!!.putString(PREF_SESSION_USER, session)
        editor!!.apply()
    }

    fun getPrefPerfilId(): String {
        return preferencias.getString(PREF_PERFIL_ID, "").toString()
    }
    fun setPrefPerfilId(param: String) {
        editor = preferencias.edit()
        editor!!.putString(PREF_PERFIL_ID, param)
        editor!!.apply()
    }

    fun getPrefPerfilNombre(): String {
        return preferencias.getString(PREF_PERFIL_NOMBRE, "").toString()
    }
    fun setPrefPerfilNombre(param: String) {
        editor = preferencias.edit()
        editor!!.putString(PREF_PERFIL_NOMBRE, param)
        editor!!.apply()
    }

    fun getPrefPerfilIdentificacion(): String {
        return preferencias.getString(PREF_PERFIL_IDENTIFICACION, "").toString()
    }
    fun setPrefPerfilIdentificacion(param: String) {
        editor = preferencias.edit()
        editor!!.putString(PREF_PERFIL_IDENTIFICACION, param)
        editor!!.apply()
    }

    fun getPrefPerfilTelefono(): String {
        return preferencias.getString(PREF_PERFIL_TELEFONO, "").toString()
    }
    fun setPrefPerfilTelefono(param: String) {
        editor = preferencias.edit()
        editor!!.putString(PREF_PERFIL_TELEFONO, param)
        editor!!.apply()
    }

    fun getPrefPerfilCorreo(): String {
        return preferencias.getString(PREF_PERFIL_CORREO, "").toString()
    }
    fun setPrefPerfilCorreo(param: String) {
        editor = preferencias.edit()
        editor!!.putString(PREF_PERFIL_CORREO, param)
        editor!!.apply()
    }

    fun getPrefFirmaUsuarioApp(): String {
        return preferencias.getString(PREF_FIRMA_USUARIO, "").toString()
    }
    fun setPrefFirmaUsuarioApp(param: String) {
        editor = preferencias.edit()
        editor!!.putString(PREF_FIRMA_USUARIO, param)
        editor!!.apply()
    }
}