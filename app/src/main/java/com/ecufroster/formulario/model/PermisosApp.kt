package com.ecufroster.formulario.model

data class PermisosApp (
    var permiso : String,
    var descripcion : String = "",
    var descripcionPermisoRechazo : String = ""
)