package com.ecufroster.formulario.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PopupListSelected (
    val uid : String = "",
    val descripcion : String = "",
    val imagenLink : String = "",
    val imagenDrawable : Int = -1,
    val colorHexadecimal : String = ""
) : Parcelable