package com.ecufroster.formulario.retrofit.model.response

import com.google.gson.annotations.SerializedName

class ApiResponseCabinet (
    @SerializedName("id") val id: String = "",
    @SerializedName("marca") val marca: String = "",
    @SerializedName("modelo") val modelo: String = "",
    @SerializedName("tipo") val tipo: String = "",
    @SerializedName("canastillas_parrillas") val canastillasParrillas: String = ""
)