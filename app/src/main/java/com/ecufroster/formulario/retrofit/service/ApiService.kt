package com.ecufroster.formulario.retrofit.service

import com.ecufroster.formulario.retrofit.model.request.RequestFirmaTecnico
import com.ecufroster.formulario.retrofit.model.request.RequestFormulario
import com.ecufroster.formulario.retrofit.model.response.*
import retrofit2.Call
import retrofit2.http.*

interface ApiService {
    @FormUrlEncoded
    @POST("v1/login")
    fun login(
        @Field("usuario") usuario: String,
        @Field("password") password: String,
    ): Call<Int>

    @GET("v1/perfil/{identificacion}")
    fun perfil(
        @Path("identificacion") identificacion: String
    ): Call<ApiResponsePerfilAuth>

    @GET("v1/clientes")
    fun getClientes(): Call<List<ApiResponseCliente>>

    @GET("v1/formularios")
    fun getTiposFormularios(): Call<List<ApiResponseTiposFormulario>>

    @GET("v1/cabinets")
    fun getCabinets(): Call<List<ApiResponseCabinet>>

    @GET("v1/placas")
    fun getPlacas(): Call<List<ApiResponsePlaca>>

    @Headers("Content-Type: application/json") //FALTA
    @POST("v1/formularios")
    fun enviarFormulario(
        @Body data: RequestFormulario
    ): Call<ResponseFormulario>

    @Headers("Content-Type: application/json") //FALTA
    @POST("v1/firma/tecnico")
    fun enviarFirmaTecnico(
        @Body data: RequestFirmaTecnico
    ): Call<ResponseFormulario>
}