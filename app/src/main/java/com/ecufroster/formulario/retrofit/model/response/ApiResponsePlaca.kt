package com.ecufroster.formulario.retrofit.model.response

import com.google.gson.annotations.SerializedName

data class ApiResponsePlaca (
    @SerializedName("placa") val placa: String = "",
    @SerializedName("anio_importacion") val anioImportacion: Int = -1
)