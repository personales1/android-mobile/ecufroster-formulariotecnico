package com.ecufroster.formulario.retrofit

import com.ecufroster.formulario.`interface`.OnApiResponse
import com.ecufroster.formulario.core.AppController
import com.ecufroster.formulario.helper.UtilidadesHelper
import com.ecufroster.formulario.retrofit.error.ErrorUtils
import com.ecufroster.formulario.retrofit.error.ServiceError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RecursoApi {
    companion object{
        fun <T> consumeApi(o: Call<T>, callback: OnApiResponse<T>) {
            try {
                if (UtilidadesHelper.isInternetAvailable(AppController.mInstance.applicationContext)) {
                    o.enqueue(object : Callback<T> {
                        override fun onResponse(call: Call<T>, response: Response<T>) {
                            if (response.isSuccessful) {
                                callback.onSuccess(response.body()!!)
                            } else {
                                val error: ServiceError? = ErrorUtils.parseError(response)
                                if (error == null) {
                                    callback.onUnknownError()
                                } else if (response.code() == 401) { //refresh token
                                    callback.onUnAuthenticated(error)
                                } else if (response.code() == 410) { //actualizar app
                                    callback.onUpdateApp(error)
                                } else {
                                    callback.onError(error)
                                }
                            }
                        }

                        override fun onFailure(
                            call: Call<T>,
                            t: Throwable
                        ) {
                            callback.onFailure(t)
                        }
                    })
                } else {
                    callback.onWithoutInternet()
                }
            } catch (e: Exception) {
                callback.onErrorException(e)
            }
        }
    }
}