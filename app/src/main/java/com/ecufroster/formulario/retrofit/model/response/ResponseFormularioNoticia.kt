package com.ecufroster.formulario.retrofit.model.response

import com.google.gson.annotations.SerializedName

class ResponseFormularioNoticia (
    @SerializedName("titulo") var titulo: String = "",
    @SerializedName("mensaje") var mensaje: String = "",
)