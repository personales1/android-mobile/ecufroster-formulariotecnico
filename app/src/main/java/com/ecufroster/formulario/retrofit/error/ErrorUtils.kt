package com.ecufroster.formulario.retrofit.error

import com.ecufroster.formulario.retrofit.service.Api
import retrofit2.Response
import java.io.IOException

object ErrorUtils {
    fun parseError(response: Response<*>): ServiceError {
        Api.settingService()
        val converter = Api.retrofit
            .responseBodyConverter<ServiceError>(
                ServiceError::class.java,
                arrayOfNulls(0)
            )
        val error: ServiceError
        error = try {
            converter.convert(response.errorBody())!!
        } catch (e: IOException) {
            return ServiceError()
        }
        return error
    }
}