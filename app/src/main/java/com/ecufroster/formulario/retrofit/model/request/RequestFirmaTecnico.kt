package com.ecufroster.formulario.retrofit.model.request

import com.google.gson.annotations.SerializedName

class RequestFirmaTecnico (
    @SerializedName("id_tecnico") var idTecnico: String,
    @SerializedName("firma") var firma: String,
)