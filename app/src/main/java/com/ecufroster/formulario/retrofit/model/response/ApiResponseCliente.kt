package com.ecufroster.formulario.retrofit.model.response

import com.google.gson.annotations.SerializedName

class ApiResponseCliente(
    @SerializedName("id") val id: String = "",
    @SerializedName("codigo") val codigo: String = "",
    @SerializedName("nombre") val nombre: String = "",
    @SerializedName("calle") val calle: String = "",
    @SerializedName("fecha_creacion") val fechaCreacion: String = "",
    @SerializedName("grupo") val grupo: String = "",
    @SerializedName("telefono") val telefono : String = "",
    @SerializedName("condicion_pago_descripcion") val condicionPagoDescripcion : String = "",
    @SerializedName("grupo_descripcion") val grupoDescripcion : String = "",
    @SerializedName("email") val email : String = "",
    @SerializedName("territorio") val territorio : String = "",
    @SerializedName("territorio_descripcion") val territorioDescripcion : String = "",
    @SerializedName("ejecutivo") val ejecutivo : String = "",
    @SerializedName("ejecutivo_telefono") val ejecutivoTelefono : String = "",
    @SerializedName("productividad") val productividad : String = "",
    @SerializedName("modelo_atencion") val modeloAtencion : String = ""
)