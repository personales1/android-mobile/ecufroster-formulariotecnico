package com.ecufroster.formulario.retrofit.model.response

import com.google.gson.annotations.SerializedName

class ResponseFormulario (
    @SerializedName("estado") var estado: Int = 0,
    @SerializedName("noticia") var noticia: ResponseFormularioNoticia? = null,
)