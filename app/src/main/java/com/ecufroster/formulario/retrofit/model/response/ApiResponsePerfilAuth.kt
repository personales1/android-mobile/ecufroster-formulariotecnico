package com.ecufroster.formulario.retrofit.model.response

import com.google.gson.annotations.SerializedName

class ApiResponsePerfilAuth (
    @SerializedName("id") val id: String = "",
    @SerializedName("nombre") val nombre: String = "",
    @SerializedName("identificacion") val identificacion: String = "",
    @SerializedName("telefono") val telefono: String = "",
    @SerializedName("correo") val correo: String = ""
)