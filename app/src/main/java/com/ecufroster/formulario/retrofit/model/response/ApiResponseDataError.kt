package com.ecufroster.formulario.retrofit.model.response

import com.google.gson.annotations.SerializedName

class ApiResponseDataError (
    @SerializedName("requiere_correo") val requiereCorreo : Int
)