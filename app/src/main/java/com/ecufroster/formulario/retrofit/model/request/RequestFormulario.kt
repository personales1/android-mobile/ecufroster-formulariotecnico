package com.ecufroster.formulario.retrofit.model.request

import com.ecufroster.formulario.db.model.*
import com.google.gson.annotations.SerializedName

class RequestFormulario (
    @SerializedName("id_formulario_app") var idFormularioApp: Int,
    @SerializedName("id_usuario_app") var idUsuarioApp: String,
    @SerializedName("formulario_app") var formularioApp: DBFormulario,
    @SerializedName("form_informacion_formulario") var formInformacionFormulario: DBFormularioInformacion,
    @SerializedName("form_informacion_cliente") var formInformacionCliente: DBFormularioInformacionCliente,
    @SerializedName("form_informacion_basica_cabinet") var formInformacionBasicaCabinet: DBFormularioInformacionCabinet,
    @SerializedName("form_informacion_tecnica_cabinet") var formInformacionTecnicaCabinet: DBFormularioInformacionCabinetTecnica,
    @SerializedName("form_observacion_tecnica") var formObservacionTecnica: DBFormularioObservacionTecnica,
    @SerializedName("form_firma_cliente") var formFirmaCliente: DBFormularioImagen,
    @SerializedName("form_fotos_reporte") var formFotosReporte: ArrayList<DBFormularioImagen>,
)