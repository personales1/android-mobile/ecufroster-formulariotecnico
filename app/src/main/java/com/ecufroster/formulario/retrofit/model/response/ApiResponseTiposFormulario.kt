package com.ecufroster.formulario.retrofit.model.response

import com.google.gson.annotations.SerializedName

class ApiResponseTiposFormulario (
    @SerializedName("id") val id: String = "",
    @SerializedName("nombre") val nombre: String = "",
    @SerializedName("codigo") val codigo: String = ""
)