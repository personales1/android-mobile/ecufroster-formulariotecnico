package com.ecufroster.formulario.retrofit.service

import com.ecufroster.formulario.BuildConfig
import com.ecufroster.formulario.core.AppController
import com.ecufroster.formulario.core.Preferencias
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class Api {
    companion object {
        private lateinit var apiInterfaceMethod: ApiService
        private lateinit var preferencias: Preferencias
        lateinit var retrofit: Retrofit
        private var api: Api? = null

        fun settingService(): ApiService {
            if (api == null) {
                api = Api()
            }
            return apiInterfaceMethod
        }

        fun cleanService() {
            if (api != null) {
                api = null
            }
        }

        private val headersDefault: MutableMap<String, String>
            get() {
                val map: MutableMap<String, String> = HashMap()
                /*
                map["Authorization"] = "Bearer " + preferencias.getAccessToken()
                map["Accept"] = "application/json"
                map["version-actualizacion"] = java.lang.String.valueOf(VERSION_ACTUALIZACION)
                map["version-app"] = BuildConfig.VERSION_CODE.toString()
                map["version-name"] = BuildConfig.VERSION_NAME
                map["device-width"] = widthDevice.toString()
                map["device-brand"] = Build.MANUFACTURER
                map["device-model"] = Build.MODEL
                map["device-version-api"] = Build.VERSION.SDK_INT.toString()
                map["device-platform"] = "android"
                map["id-dispositivo"] = preferencias.getIdDispositivo()
                 */

                return map
            }

        //Setea las cabeceras para el API
        val headers: Map<String, String>
            get() {
                val map: MutableMap<String, String> = headersDefault
                map["Content-Type"] = "application/x-www-form-urlencoded"

                return map
            }

        //Setea las cabeceras para el API
        val headersJsonWithAuthentication: Map<String, String>
            get() {
                val map: MutableMap<String, String> = headersDefault
                map["Content-Type"] = "application/json"

                return map
            }
    }

    init {
        val clientBuilder = OkHttpClient.Builder()
        val loggingInterceptor = HttpLoggingInterceptor()

        loggingInterceptor.level = HttpLoggingInterceptor.Level.HEADERS
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        if(BuildConfig.DEBUG){
            clientBuilder.addInterceptor(loggingInterceptor)
        }

        clientBuilder.connectTimeout(60, TimeUnit.SECONDS)
        clientBuilder.readTimeout(60, TimeUnit.SECONDS)

        preferencias = Preferencias(AppController.mInstance.applicationContext)

        retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.API_ENDPOINT)
            .addConverterFactory(GsonConverterFactory.create())
            .client(clientBuilder.build())
            .build()

        apiInterfaceMethod = retrofit.create(ApiService::class.java)
    }
}