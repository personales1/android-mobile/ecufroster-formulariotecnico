package com.ecufroster.formulario.retrofit.model.response

import com.google.gson.annotations.SerializedName

class ApiResponseNotice (
    @SerializedName("titulo") val titulo : String,
    @SerializedName("mensaje") val mensaje : String,
    @SerializedName("mensaje_catch") val mensajeCatch: String
)