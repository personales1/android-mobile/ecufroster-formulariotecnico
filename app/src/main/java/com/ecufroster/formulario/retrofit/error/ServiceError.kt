package com.ecufroster.formulario.retrofit.error

import com.ecufroster.formulario.retrofit.model.response.ApiResponseDataError
import com.ecufroster.formulario.retrofit.model.response.ApiResponseNotice
import com.fasterxml.jackson.annotation.JsonAnyGetter
import com.fasterxml.jackson.annotation.JsonAnySetter
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty

class ServiceError {
    @JsonProperty("estado")
    var estado: Int? = null

    @JsonProperty("noticias")
    lateinit var noticias: ApiResponseNotice

    @JsonProperty("mensaje_desarrollador")
    lateinit var mensajeDesarrollador: String

    @JsonProperty("data")
    var data: ApiResponseDataError? = null

    @JsonIgnore
    private val additionalProperties: MutableMap<String, Any> = HashMap()

    /**
     * No args constructor for use in serialization
     */
    constructor() {}

    /**
     * @param message
     * @param status
     * @param mensaje_desa
     */
    constructor(status: Int, message: ApiResponseNotice, mensaje_desa: String, dataError: ApiResponseDataError) {
        estado = status
        noticias = message
        mensajeDesarrollador = mensaje_desa
        data = dataError
    }

    @JsonAnyGetter
    fun getAdditionalProperties(): Map<String, Any> {
        return additionalProperties
    }

    @JsonAnySetter
    fun setAdditionalProperty(name: String, value: Any) {
        additionalProperties[name] = value
    }
}